//Copyright (c) 2004-2008 Anastasis Societa' Cooperativa,Bologna,Italy - www.anastasis.it
//	This copyright notice must stay intact for use.

//----------------FUNZIONI A DISPOSIZIONE DELL'OGGETTO ANASTASIS--------------//
/**
 * Apre un popup passandogli dei parametri e una funzione di callback.<br />
 * NB: si puo' aprire una sola finsetra di popup alla volta (le precedenti vengono chiuse). Se tentiamo di aprire un'altra finestra uguale alla preesistente,le diamo semplicemente il focus.
 * @param {String} url l'indirizzo della pagina da aprire nel popup
 * @param {function} action la funzione di callback chiamata alla chiusura della finestra; puo' prendere in input un Object con tutti i parametri ritornati
 * @param {Object} init un Object con tutti i parametri da passare alla finestra
 * @example  var param=new Object(); <br /> param["parametro"]="valore"; <br /> var action=function(returnParams){alert(returnParams["parametro_ritornato"]);}; <br /> anastasis.openDialog("http://url",action,param);
 */
Anastasis.prototype.openDialog=function(url, action, init)
{
	//Se tentiamo di aprire un'altra finestra diversa, chiudiamo la preestistente.
	//Se tentiamo di aprire un'altra finestra uguale alla preesistente,le diamo semplicemente il focus
	if(anastasis.current_dialog && anastasis.current_dialog.finestra && !anastasis.current_dialog.finestra.closed)
	{	
		if(url==anastasis.current_dialog.url)
		{
			anastasis.current_dialog.finestra.focus();
			return;
		}
		else
			anastasis.current_dialog.finestra.close();
	}
	anastasis.current_dialog=new Anastasis.Dialog(url, action, init);
	anastasis.current_dialog.show();
}

/**
 * Funzione che puo' usare la finestra di popup per ricevere i parametri con cui e' stata chiamata.
 * @private
 */
Anastasis.prototype.getDialogArguments=function()
{
	return window.opener.anastasis.current_dialog.argument;
}

/**
 * Funzione che deve usare il popup per ritornare i valori alla finestra chimante.
 * @private
 */
Anastasis.prototype.dialogReturn=function(param)
{
	return window.opener.anastasis.current_dialog.restitute(param);
}

//---------------------CLASS--DIALOG-------------------------------------------//

/**
 * @class La classe per lanciare popups.
 * @private
 */
Anastasis.Dialog=function(url, action, init)
{
	var me=this;
	
	this.finestra = null;
	
	//Viene passato a tutti i popup per avere le coordinate per caricare popup.js e popup_style.css
	//if(!init) init=new Object();
	//if(_editor_url) init["editor_url"]=_editor_url;		
		
	this.argument = init;
	this.action=action;		
	this.url=url;
}

/**
 * @private
 */
Anastasis.Dialog.prototype.show=function() 
{
	var menubar="no";
	if(this.argument && this.argument["menubar"]) menubar="yes";
	
	var scrollbars="no";
	if(this.argument && this.argument["scrollbars"]) scrollbars="yes";
	
	if(anastasis.is_ie) this.finestra=window.open("","_blank",
			      "toolbar=no,menubar="+menubar+",personalbar=no,width=10,height=10,location=no," +
			      "scrollbars="+scrollbars+",resizable=yes,modal=yes,dependable=yes");	
	else               this.finestra=window.open("","dialog "+this.url,
			      "toolbar=no,menubar="+menubar+",personalbar=no,width=10,height=10," +
			      "scrollbars="+scrollbars+",resizable=yes,modal=yes,dependable=yes");		
	
		
	
	var me=this;		
	
	//Sarebbe l'ideale, ma in IE non va...
	//anastasis.utils.addEvent(me.finestra,"load",function(){me.finestra.anastasis.syncLoadScript("Popup",true); me.finestra.popup.init();});			
	
	me.finestra.location=this.url;
}

/**
 * @private
 */
Anastasis.Dialog.prototype.restitute = function (param) 
{    			
	if (this.action) 
		return this.action(param);
	
	return null;	
};



