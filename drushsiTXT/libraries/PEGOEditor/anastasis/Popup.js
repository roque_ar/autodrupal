//Copyright (c) 2004-2008 Anastasis Societa' Cooperativa,Bologna,Italy - www.anastasis.it
//	This copyright notice must stay intact for use.


//---------------COSTRUTTORE-------------------------//

Anastasis.Popup=function()
{
	this.callParams=anastasis.getDialogArguments();
	if(!this.callParams) this.callParams=new Object();			
	this.returnParams=new Object();
	this.I18N=null;
	this.required=new Object();
	this.fields=new Array();
	this.attachFileds=new Array();


	var me=this;
	anastasis.utils.addEvent(window,"load",function(){me.init();});	
}

//--------------METODI PUBBLICI----------------------//

Anastasis.Popup.prototype.hasCallParam=function(param)
{
	if(this.callParams[param]) return true;
	else return fals;
}

Anastasis.Popup.prototype.getCallParam=function(param)
{	
	return this.callParams[param];
}

Anastasis.Popup.prototype.getReturnParam=function(param)
{	
	return this.callParams[param];
}

Anastasis.Popup.prototype.addReturnParam=function(param,value)
{
	this.returnParams[param]=value;
}

Anastasis.Popup.prototype.addRequired=function(field,message)
{
	this.required[field]=message;
}

Anastasis.Popup.prototype.addRequireds=function(requireds)
{
	for(var req in requireds)
	{
		this.required[req]=requireds[req];
	}
}

Anastasis.Popup.prototype.addField=function(field)
{
	this.fields.push(field);
}

Anastasis.Popup.prototype.addFields=function(fields)
{
	for(var i in fields)
		this.fields.push(fields[i]);
}

Anastasis.Popup.prototype.ok=function() 
{   
   	if(!this.checkFormValues(this.required)) return;      	
  	
  	if(!this.onOk()) return;  	
  	
  	this.getFormValues(this.fields);
  	
  	if(this.formSender) this.send(this.attachFileds);
  	else this.close();
  	 
	return false;
};

Anastasis.Popup.prototype.cancel=function() 
{
  if(!this.onCancel()) return; 
  this.close(true);
  return false;
};

Anastasis.Popup.prototype.unlink=function() 
{   
  if(!this.onUnlink()) return;   
  this.returnParams = new Object();
  this.returnParams["unlink"]="anastasis_unlink";      
  this.close();
  return false;
};

Anastasis.Popup.prototype.translate=function(string)
{
	return this.I18N.translate(string);
}

Anastasis.Popup.prototype.fillFormValues=function(params)
{	
	for(var i in params)
	{
		if(this.getCallParam(params[i]))
		{ 
			if(document.getElementById(params[i]))
				document.getElementById(params[i]).value=this.getCallParam(params[i]);
			else  
				throw ("Try to fill absent field: "+params[i]);
		}
	}
}

Anastasis.Popup.prototype.checkFormValues=function(required)
{
	for (var i in required) 
	{
    	var el = document.getElementById(i);
    	if(!el) throw ("Try to check absent field: "+i);
    	if (!el.value) 
    	{
      		if(required[i]) alert(this.translate(required[i]));
      		else alert("Missing: "+i);
      		el.focus();
      		return false;
    	}
  	}
  	return true;
}

Anastasis.Popup.prototype.getFormValues=function(fields)
{
	for (var i in fields) 
	{
	    var el = document.getElementById(fields[i]);
	    if(!el) {alert ("Try to get absent field: "+fields[i]); continue; }
	    var name=el.tagName ? el.tagName.toLowerCase() : "";
	    switch(name)
	    {
	    	case "input": 
			    switch(el.type)
			    {
				    	case "text":
				    	case "hidden":
				    	case "file":
			    	this.addReturnParam(fields[i],el.value);
			    	break;
			    		
			    		case "checkbox":
			    	this.addReturnParam(fields[i],el.checked);
			    	break;	
			    		
			    		default:
			    	alert("Try to get field with unknown type: "+fields[i]+" - "+el.type);
			    	continue;
			    }
			    break;
	    	
	    	case "select":
	    	case "textarea":
		    	this.addReturnParam(fields[i],el.value);
		    	break;
		    	
		    default:
		    	alert("Try to get field with unknown type: "+fields[i]+" - "+el.type);
		    	continue;
	    	
	    }	    
	 }
}

Anastasis.Popup.prototype.registerForm=function(idForm,fileFieldName,attachFieldId)
{
	this.loadFormSender();
	this.formSender.register(idForm,fileFieldName,attachFieldId); 
	this.attachFileds.push(attachFieldId);
}

Anastasis.Popup.prototype.unRegisterForm=function(idForm)
{
	try { this.formSender.unRegister(idForm); } catch(e){}
}

Anastasis.Popup.prototype.popupDialog = function(url, action, outparam) 
{
	if(!outparam) outparam=new Object();
	 
	if(!outparam["I18N"] && this.I18N)
	{
		outparam["I18N"]=this.I18N;
	}
	 
	return anastasis.openDialog(url,action, outparam); 
};

//-----------FUNZIONIPERSONALIZZABILI NEL POPUP-------------------------//

Anastasis.Popup.prototype.onInit=function(){}

/**
 * NB: Ritorna true se si pu� procedere, false altrimenti
 */
Anastasis.Popup.prototype.onOk=function() { return true;};

/**
 * NB: Ritorna true se si pu� procedere, false altrimenti
 */
Anastasis.Popup.prototype.onCancel=function() { return true; };

/**
 * NB: Ritorna true se si pu� procedere, false altrimenti
 */
Anastasis.Popup.prototype.onUnlink=function() {return true; };

//--------------------METODI-PRIVATI----------------------------------------------------//

Anastasis.Popup.prototype.init=function()
{
	
	var me=this;
	
	if (typeof _popup_onInit== "function")
		this.onInit=_popup_onInit;
	if (typeof _popup_onOk== "function")
		this.onOk=_popup_onOk;
	if (typeof _popup_onCancel== "function")
		this.onCancel=_popup_onCancel;
	if (typeof _popup_onUnlink== "function")
		this.onUnlink=_popup_onUnlink;
	
	//-----------------------------------------------------------------------------------//
	function onkeypress(ev) 
	{
		//Chiude se uno preme Esc
		if (ev.keyCode == 27) 
		{
			me.close(true);
			return false;
		}
		return true;
	};	
	
	//-----------------------------------------------------------------------------------//
			
	if(this.getCallParam("I18N"))
	{
		if(this.getCallParam("I18N").strings)
			this.I18N=this.getCallParam("I18N");
		else
			this.I18N=new Anastasis.I18N(this.getCallParam("I18N"));
	}
	Anastasis.Utils.translatePage(this.I18N);
	
	var me=this;
	anastasis.utils.addEvent(window,"keypress",onkeypress);
	//anastasis.utils.addEvent(window,"unload",function(){me.restitute(true);});
	
	//Blocca le finstre parent
	/*
	var win=window.opener;
	while(win)
	{
		this.capwin(win);
		win=win.opener;
	}
	*/
	
	this.onInit();
}

Anastasis.Popup.prototype.setSize=function(width,height)
{
	if(width && height)	   
	{	
		if (anastasis.is_ie) //IE
	  	{
			if(anastasis.is_ie7) //due to the unremovable top address bar...
			{
				width+=40;
				height+=40;
			}
			window.resizeTo((width < 770) ? width+30 : 800, (height < 560) ? height+40 : 600); //Resize to in Firefox non considera i margini e statusbar, in explorer s�'		
			// center on parent
			var x = (screen.availWidth - width) / 2;
			var y = (screen.availHeight - height) / 2;
			window.moveTo(x, y);			
		}  
	  else  //Mozilla
	  {
			window.innerWidth=width;
			window.innerWidth=width;	//Assurdo!! Va fatto 2 volte!	
			window.innerHeight=height;			
			// center on parent
			var x = opener.screenX + (opener.outerWidth - window.outerWidth) / 2;
			var y = opener.screenY + (opener.outerHeight - window.outerHeight) / 2;
			window.moveTo(x, y);
		}
	}
	
}



Anastasis.Popup.prototype.close=function(isCancel)
{
	this.restitute(isCancel);
	window.close();
}

Anastasis.Popup.prototype.restitute=function(isCancel)
{
	//Libera le finestre parent      
	/*
	var win=window.opener;
	while(win)
	{
		this.relwin(win);
		win=win.opener;
	}
	* */
	
	if(!isCancel) return anastasis.dialogReturn(this.returnParams);
}

Anastasis.Popup.prototype.capwin=function(w) 
{
	anastasis.utils.addEvent(w, "click", this.parentEvent);
	anastasis.utils.addEvent(w, "mousedown", this.parentEvent);
	anastasis.utils.addEvent(w, "focus", this.parentEvent);
};

Anastasis.Popup.prototype.relwin=function(w) 
{
	anastasis.utils.removeEvent(w, "click", this.parentEvent);
	anastasis.utils.removeEvent(w, "mousedown", this.parentEvent);
	anastasis.utils.removeEvent(w, "focus", this.parentEvent);
};

Anastasis.Popup.prototype.parentEvent = function(ev) 
{
	var me=this;
	setTimeout( function() { if (!window.closed) { window.focus() } }, 50);
	if (!window.closed) 
	{
		anastasis.utils.stopEvent(anastasis.is_ie ? window.event : ev);
	}
};

Anastasis.Popup.prototype.loadFormSender=function(idForm,fileFieldName,attachFieldId)
{
	if (this.formSender) return;
	anastasis.syncLoadScript("FormSender",true);
	this.formSender=new Anastasis.FormSender();	
}

Anastasis.Popup.prototype.send=function(fields)
{
	var me=this;
	try 
	{ 
			this.formSender.send(function()
								{
									me.getFormValues(fields);
									me.close();										
								}); 
	} catch(e) {alert("Upload error:\n"+e); me.close();}
}

//----------------------------------------------------------------------------------------//
window.popup=new Anastasis.Popup();
