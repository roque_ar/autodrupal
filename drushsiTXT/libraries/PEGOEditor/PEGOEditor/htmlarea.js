//Copyright (c) 2004-2007 Anastasis S.C.A.R.L., www.anastasis.it
//
//  PEGO was originally based on HTMLArea by Mihai Bazon which is:
//	Copyright (c) 2003-2004 dynarch.com.
//	Copyright (c) 2002-2003 interactivetools.com, inc.
//	This copyright notice MUST stay intact for use.



//TODO: move inside a class
function randomnum()
{
  return Math.round(Math.random()*2000000000);
}

//TODO: eliminate!!destroy!erase!
String.prototype.trim = function()
{
	var a = this.replace(/^\s+/, '');
	return a.replace(/\s+$/, '');
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                          CREAZIONE PEGOEditor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

Anastasis.prototype.getEditor=function (textarea,editorParams)
{
	var getBaseUrl=function()
	{

		var script=document.getElementById("PEGOEditor_script");
		if(!script)
		{
			var ss=document.getElementsByTagName("script");
			for(var index=0; index<ss.length; index++)
			{
				if(ss[index].src && ss[index].src.match(/htmlarea.js/i))
				{
					script=ss[index];
					break;
				}
			}

		}

		var url=null;

		try //Gecko
		{
			url="/"+script.src.match(/.*:\/\/[\w\d]*:?\d*\/(.*)htmlarea.js/i)[1];
		}catch(e) //IE
		{
			try
			{
				url=script.src.match(/(.*)htmlarea\.js/i)[1];
				//L'url potrebbe essere relativo, dobbiamo farlo assoluto
				if(!url || url.match(/^\.\./))
				{
					var dove=window.location.toString();
					dove=dove=dove.match(/(.*)\/.*/);
					if(dove &&  dove[1])
					{
						if(url==null) url="";
						url=dove[1]+"/"+url;
					}
				}
			}catch(e){alert("[ERROR] Cannot initialize editor_url"); return "";}
		}

		if(url==null) alert("[ERROR] Cannot initialize editor_url");

		return url;
	}

	_editor_url=getBaseUrl();

	return new PEGOEditor(textarea,_editor_url,editorParams);
}

// Creates a new PEGOEditor object.  Tries to replace the textarea with the given ID with it.
function PEGOEditor(textarea,_editor_url,params)
{
	PEGOEditor.is_ie=anastasis.is_ie;
	PEGOEditor.is_ie7=anastasis.is_ie7;
	PEGOEditor.is_ie8=anastasis.is_ie8;
	PEGOEditor.is_gecko=anastasis.is_gecko;

	if (!PEGOEditor.is_ie && !PEGOEditor.is_gecko)
	{
		alert("ATTENZIONE: questo editor e' compatibile solo con Internet Explorer 5.5 o superiore e Mozilla Firefox 1.0 o superiore\n------------\n" +
				"WARNING: this editor is compatible only with Internet Explorer 5.5 or later and Mozilla Firefox 1.0 or later");
		return;
	}

	this.system_params=params;

	this._editor_url=_editor_url;

	anastasis.syncLoadScript(this.system_params.config_file);

	this.config = new PEGOEditor.Config();
	if(PEGOEditor._config_options)
    {
      for (var option in PEGOEditor._config_options) this.config[option]=PEGOEditor._config_options[option];
    }

    anastasis.syncLoadScript(this._editor_url + "Plugin.js");

	this._htmlArea = null;
	this._editor= null;
	this._textArea = textarea;
	this._editMode = "wysiwyg";
	this.plugins = {};
	this._timerToolbar = null;
	this._timerUndo = null;
	this._undoQueue = new Array(this.config.undoSteps);
	this._undoPos = -1;
	this._form=null;
	this.mem=null;

	this.document_id=-1;
	this.element_id=-1;
	this.anchor_id_index=0;
	this.word_pasted=false;

	this.searched = false;
    this.lastSearched="" ;
    this.found = false;
    this.ie_search_base='';
    this.noComb=false;

	this._xmlHttp=null;

    this.loadStyle("htmlarea.css");

    this.loaded=false;

    var editor=this;

    if(this.config.enableNotify && this.system_params.ping_url)
    	setInterval(function(){editor.notify()},this.config.notifyTime);

};

PEGOEditor.prototype.is_ie7_or_later=function()
{
	return (PEGOEditor.is_ie7 || PEGOEditor.is_ie8);
}

PEGOEditor.prototype.is_ie8_or_later=function()
{
	return (PEGOEditor.is_ie8);
}

/**
 * Notify to the server that the user is still editing the resource with the PEGOEditor.
 * Useful if you have timed sessions.
 */
PEGOEditor.prototype.notify = function()
{
  try
  {
  	this.ajaxSend(new Object(),this.system_params.ping_url,true);
  }
  catch(e){}

}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                          CONFIG CLASS
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

PEGOEditor.Config = function ()
{
	this.lang="it";

	this.width = "auto";
	this.height = "auto";

	// enable creation of a status bar?
	this.statusBar = true;

	//maximum number of links in the stausbar (for blocks path)
	this.max_statusbar_links=35;

	// maximum size of the undo queue
	this.undoSteps = 20;

	// the time interval at which undo samples are taken
	this.undoTimeout = 500;	// 1/2 sec.

	//whether the toolbar should be included in the size or not
	this.sizeIncludesToolbar = true;

	//enable toolabar ackground image?
	this.toolbarBackroundImage= true;

	//Enable notify every notifyTime milliseconds?
	this.enableNotify=true;
	this.notifyTime=60000;

	// BaseURL included in the iframe document
	this.baseURL = document.baseURI || document.URL;
	if (this.baseURL && this.baseURL.match(/(.*)\/([^\/]+)/))
		this.baseURL = RegExp.$1 + "/";


	// URL-s
	this.imgURL = "images/";
	this.popupURL = "popups/";

	// Stop on acccessibility warnings
	this.stop_on_accessibility_warning=true;

	//libert� di assegnare stili (se false si possono usare solo le classi - se vengono contenmporaneamente disabilitati gli opportuni bottoni)
	this.freestyle= true;

	/**
	 * Altri elementi disponibili:
	 * preview - lefttoright - righttoleft
	 */
	this.toolbar = [
		//Prima fila
    [ "anchor","save", "cancel","separator","space",
      "undo", "redo","findreplace","pasteunformatted","separator",
		  "bold", "italic", "underline", "strikethrough", "separator",
		  "subscript", "superscript", "separator",
		  "justifyleft", "justifycenter", "justifyright", "justifyfull","separator",
		  "insertorderedlist", "insertunorderedlist", "outdent", "indent", "separator",
		  "forecolor", "hilitecolor"
    ],
		//Seconda fila
		  [
		  "HTML: ","space","anchor",
		  "formatblock","space","separator",
		  "inserthorizontalrule", "separator",
      "createlink","insertanchor","nodelink","separator",
      "inserttable","separator",
      "insertimage",
      "space","separator",
		  "changelang","space","separator",
		  "deleteall","clearstyle","separator",
      "space","htmlmode","space","separator",
		  "space","showhelp", "about" ]
	];

	//"ID: [ ToolTip, Enabled in text mode?, options={option:value},ACTION, context, update]"
	this.selectList =
	{
		formatblock : ["formatblock",
					false,
					options={
							"None":   "-",
						    "Paragraph": "p",
							"Heading1": "h1",
							"Heading2": "h2",
							"Heading3": "h3",
							"Heading4": "h4",
							"Heading5": "h5",
							"Heading6": "h6"
							},
					function(editor,myself)
					{
						var value = myself.getValue();
				  		if(value=="-") value="div";
						(PEGOEditor.is_ie) && (value = "<" + value + ">");
						editor.execCommand("formatblock", false, value);
					},
					"*",
					function(editor,myself,ancestors)
					{
						var format_tag_found=false;
						for(var i=0;i<ancestors.length;i++)
						{
							var el = ancestors[i];
							if (!el) continue;
							var tagName=el.tagName.toLowerCase();
							if(!format_tag_found) format_tag_found=myself.selectOptionWithValue(tagName);
							if(format_tag_found) break;
						}
						if(!format_tag_found) myself.selectOptionWithValue("-");
					}
					]
	};

	// ADDING CUSTOM BUTTONS: please read below!
	// format of the btnList elements is "ID: [ ToolTip, Icon, Enabled in text mode?, ACTION, context,activation]"
	//    - ID: unique ID for the button.  If the button calls document.execCommand
	//	    it's wise to give it the same name as the called command.
	//    - ACTION: function that gets called when the button is clicked.
	//              it has the following prototype:
	//                 function(editor, buttonName)
	//              - editor is the PEGOEditor object that triggered the call
	//              - buttonName is the ID of the clicked button
	//              These 2 parameters makes it possible for you to use the same
	//              handler for more PEGOEditor objects or for more different buttons.
	//    - ToolTip: default tooltip, for cases when it is not defined in the -lang- file (PEGOEditor.I18N)
	//    - Icon: path to an icon image file for the button
	//    - Enabled in text mode: if false the button gets disabled for text-only mode; otherwise enabled all the time.
	//	  - Context: the context (tag[attribute=value,attribute=value]) in which the button is to be enabled; leave blank or "*" means everywhere
	//				 tag and attributes must be specified lower-case
	//	  - activation: a function that takes as input the context and return true if the button must be activated, false otherwise
	this.btnList = {
		bold: [ "Bold", "format_bold.gif", false, function(e) {e.execCommand("bold");},"*",function(editor){return editor.standardCheckButtonActivation("bold");} ],
		italic: [ "Italic", "format_italic.gif", false, function(e) {e.execCommand("italic");},"*",function(editor){return editor.standardCheckButtonActivation("italic");} ],
		underline: [ "Underline", "format_underline.gif", false, function(e) {e.execCommand("underline");},"*",function(editor){return editor.standardCheckButtonActivation("underline");} ],
		strikethrough: [ "Strikethrough", "format_strike.gif", false, function(e) {e.execCommand("strikethrough");},"*",function(editor){return editor.standardCheckButtonActivation("strikethrough");} ],
		subscript: [ "Subscript", "format_sub.gif", false, function(e) {e.execCommand("subscript");},"*",function(editor){return editor.standardCheckButtonActivation("subscript");} ],
		superscript: [ "Superscript", "format_sup.gif", false, function(e) {e.execCommand("superscript");},"*",function(editor){return editor.standardCheckButtonActivation("superscript");} ],
		justifyleft: [ "Justify Left", "align_left.gif", false, function(e) {e.execCommand("justifyleft");},"*",function(editor){return editor.standardCheckButtonActivation("justifyleft");} ],
		justifycenter: [ "Justify Center", "align_center.gif", false, function(e) {e.execCommand("justifycenter");},"*",function(editor){return editor.standardCheckButtonActivation("justifycenter");} ],
		justifyright: [ "Justify Right", "align_right.gif", false, function(e) {e.execCommand("justifyright");},"*",function(editor){return editor.standardCheckButtonActivation("justifyright");} ],
		justifyfull: [ "Justify Full", "align_justify.gif", false, function(e) {e.execCommand("justifyfull");},"*",function(editor){return editor.standardCheckButtonActivation("justifyfull");} ],
		insertorderedlist: [ "Ordered List", "list_num.gif", false, function(e) {e.execCommand("insertorderedlist");},"*",function(editor){return editor.standardCheckButtonActivation("insertorderedlist");} ],
		insertunorderedlist: [ "Bulleted List", "list_bullet.gif", false, function(e) {e.execCommand("insertunorderedlist");},"*",function(editor){return editor.standardCheckButtonActivation("insertunorderedlist");} ],
		outdent: [ "Decrease Indent", "indent_less.gif", false, function(e) {e.execCommand("outdent");} ],
		indent: [ "Increase Indent", "indent_more.gif", false, function(e) {e.execCommand("indent");} ],
		forecolor: [ "Font Color", "color_fg.gif", false, function(e) {e.execCommand("forecolor");} ],
		hilitecolor: [ "Background Color", "color_bg.gif", false, function(e) {e.execCommand("hilitecolor");} ],
		inserthorizontalrule: [ "Horizontal Rule", "hr.gif", false, function(e) {e.execCommand("inserthorizontalrule");} ],
		createlink: [ "Insert Web Link", "link.gif", false, function(e) {e.execCommand("createlink", true);},"*",function(editor,ancestors){return editor.searchIdInAncestors(ancestors,"anastasis_link");} ],
		insertimage: [ "Insert/Modify Image", "image.gif", false, function(e) {e.execCommand("insertimage");},"*",function(editor,ancestors){return (ancestors[0] && ancestors[0].id && ancestors[0].id.match(/anastasis_uploaded/));} ],
		inserttable: [ "Insert Table", "insert_table.gif", false, function(e) {e.execCommand("inserttable");} ],
		htmlmode: [ "Toggle HTML Source", "html.gif", true, function(e) {e.execCommand("htmlmode");},"*",function(editor,ancestors){return (editor._editMode == "textmode");} ],
		about: [ "About this editor", "about.gif", true, function(e) {e.execCommand("about");} ],
		showhelp: [ "Help using editor", "help.gif", true, function(e) {e.execCommand("showhelp");} ],
		undo: [ "Undoes your last action", "undo.gif", false, function(e) {e.execCommand("undo");} ],
		redo: [ "Redoes your last action", "redo.gif", false, function(e) {e.execCommand("redo");} ],
		lefttoright: [ "Direction left to right", "left_to_right.gif", false, function(e) {e.execCommand("lefttoright");},"*",function(editor,ancestors){var el = ancestors[0]; for(var i in ancestors) if(ancestors[i] && editor.isBlockElement(ancestors[i])){el=ancestors[i]; break;} if (el) return (el.style.direction == "ltr"); else return false;} ],
		righttoleft: [ "Direction right to left", "right_to_left.gif", false, function(e) {e.execCommand("righttoleft");},"*",function(editor,ancestors){var el = ancestors[0]; for(var i in ancestors) if(ancestors[i] && editor.isBlockElement(ancestors[i])){el=ancestors[i]; break;} if (el) return (el.style.direction == "rtl"); else return false;} ],
		preview: [ "Preview", "preview.gif", false, function(e) {e.execCommand("preview");} ],
		changelang: [ "Specify language", "changelang.gif", false, function(e) {e.execCommand("changelang");} ],
		insertanchor: [ "Insert anchor", "insert_anchor.gif", false, function(e) {e.execCommand("insertanchor");},"*",function(editor,ancestors){return (editor.searchIdInAncestors(ancestors,"anchor_link") || editor.searchIdInAncestors(ancestors,"anastasis_anchor_")); } ],
		save: [ "Save", "save.gif", false, function(e) {e.execCommand("save");} ],
		cancel: [ "Cancel", "cancel.gif", true, function(e) {e.execCommand("cancel");} ],
	    nodelink: [ "Insert internal node link", "node_link.gif", false, function(e) {e.execCommand("nodelink");},"*",function(editor,ancestors){return editor.searchIdInAncestors(ancestors,"anastasis_nodelink");} ],
	    findreplace: [ "Find and replace", "replace.gif", false, function(e) {e.execCommand("findreplace");} ],
	    clearstyle: [ "Clear style properties", "clear_tags.gif", false, function(e) {e.execCommand("clearstyle");} ],
	    deleteall: [ "Delete all", "delete_all.gif", false, function(e) {e.execCommand("deleteall");} ],
	    pasteunformatted: [ "Paste unformatted", "paste_unformatted.gif", false, function(e) {e.execCommand("pasteunformatted");} ],
	    firstButton: [ " ", " ", false, function() {} ]
	};

	/**
	 * Unicode symbols to be searched and converted with the conversion code
	 */
	this._unicode_symbols=
	{
	  0x20AC: "&#x20AC;",
	  0x00B4: "'",
	  0x01BC: "'",
	  0x2018: "'",
	  0x2019: "'",
	  0x201A: "'",
	  0x201B: "'",
	  0x201C: "&quot;",
	  0x201D: "&quot;",
	  0x201E: "&quot;",
	  0x201F: "&quot;",
	  0x2032: "'",
	  0x2033: "&quot;",
	  0x2035: "'",
	  0x2036: "&quot;"
	};

	/**
	 * Useless tag inserted by word - to be deleted
	 */
	this._word_trash=
	[
	  "<!--[if supportFields]>",
	  "mso-element",
	  "mso-spacerun",
	  "mso-bookmark",
	  "shapetype",
	  "v:f eqn",
	  "extrusionok"
	];
};

PEGOEditor.Config.prototype.init=function(editor_url)
{
	// initialize tooltips from the I18N module and generate correct image path
	for (var i in this.btnList)
	{
		var btn = this.btnList[i];
		if(!btn[1].match(/^[[\/]|[\?]|http:|file:/)) btn[1] = editor_url + this.imgURL + btn[1];
		if (typeof PEGOEditor.I18N.tooltips[i] != "undefined")
		{
			btn[0] = PEGOEditor.I18N.tooltips[i];
		}
	}
}

/**
 * DEPRECATED
 *
 *Helper function: register a new button with the configuration.  It can be
 * called with all 5 arguments, or with only one (first one).  When called with
 * only one argument it must be an object with the following properties: id,
 * tooltip, image, textMode, action.  Examples:
 *
 * 1. config.registerButton("my-hilite", "Hilite text", "my-hilite.gif", false, function(editor) {...});
 * 2. config.registerButton({
 *      id       : "my-hilite",      // the ID of your button
 *      tooltip  : "Hilite text",    // the tooltip
 *      image    : "my-hilite.gif",  // image to be displayed in the toolbar
 *      textMode : false,            // disabled in text mode
 *      action   : function(editor) { // called when the button is clicked
 *                   editor.surroundHTML('<span class="hilite">', '</span>');
 *                 },
 *      context  : "p"               // will be disabled if outside a <p> element
 *    });
 */
PEGOEditor.Config.prototype.registerButton = function(id, tooltip, image, textMode, action, context) {
	var the_id;
	if (typeof id == "string") {
		the_id = id;
	} else if (typeof id == "object") {
		the_id = id.id;
	} else {
		alert("ERROR [PEGOEditor.Config::registerButton]:\ninvalid arguments");
		return false;
	}
	// check for existing id
	if (typeof this.btnList[the_id] != "undefined") {
		// alert("WARNING [PEGOEditor.Config::registerDropdown]:\nA button with the same ID already exists.");
	}
	switch (typeof id) {
	    case "string": this.btnList[id] = [ tooltip, image, textMode, action, context ]; break;
	    case "object": this.btnList[id.id] = [ id.tooltip, id.image, id.textMode, id.action, id.context ]; break;
	}
};

/**
 * Register new buttons
 * @param buttons; buttons Array; in the with the syntax of the btnList
 */
PEGOEditor.Config.prototype.registerButtons = function(buttons)
{
	for(var i in buttons)
	{
		if(this.btnList[i]) {alert("Duplicate button entry, skipped: "+i); continue;}
		this.btnList[i]=buttons[i];
	}
}

/**
 * Register new selects
 * @param selects; selects Array; in the with the syntax of the selectList
 */
PEGOEditor.Config.prototype.registerSelects = function(selects)
{
	for(var i in selects)
	{
		if(this.selectList[i]) {alert("Duplicate button entry, skipped: "+i); return;}
		this.selectList[i]=selects[i];
	}
}

/**
 * Adds the objects to the current toolbar configuration
 * @param objects String Array; with the syntax ["id","id",...]
 */
PEGOEditor.Config.prototype.addToolbarObjects = function(objects)
{
	this.toolbar.push(objects);
}



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                          TOOLBAR CLASS
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

// Creates the toolbar and appends it to the _htmlarea
PEGOEditor.Toolbar = function (editor,config)
{
	this.editor=editor;
	this.contatore_ancore=1;
	this.tabindex=0;
	this.lines=new Array();
	this.lines.push(new Object());//this.lines[0] is a dummy object

	var toolbar = document.createElement("div");
	editor._toolbar = toolbar;
	this.mainDiv=toolbar;
	toolbar.className = "toolbar";
	toolbar.unselectable = "1";
  	if(config.toolbarBackroundImage)
  	{
    	toolbar.style.backgroundImage='url("'+_editor_url+'/images/toolbar-back.jpg")';
    	toolbar.style.paddingTop = "6px";
 	}

	this.objects = new Object();

	// init first line
	this.newLine();
	var newel=null;
	toolbar.firstElement=null;

	var objList=config.toolbar;

	//if(PEGOEditor.is_ie) objList[0].splice(0, 0, "firstButton");

	for (var i in objList)
	{
		var group = objList[i];
		for (var j in group)
    	{
			var code = group[j];
			newel=this.createObject(code,config);
			if(!toolbar.firstElement) toolbar.firstElement=newel;
		}
	}
};

PEGOEditor.Toolbar.prototype.getHeight=function()
{
	return this.mainDiv.offsetHeight;
}

PEGOEditor.Toolbar.prototype.newLine=function(id)
{
	var line= document.createElement("div");
	if(id) line.id=id;
	line.className="line";
	this.lines.push(line);
	this.mainDiv.appendChild(line);
};

PEGOEditor.Toolbar.prototype.focusOnLine=function(line_number)
{
	var line=this.lines[line_number];
	if(line)
	{
		var child=line.firstChild;
		while(child)
		{
			if(child.firstChild.tagName.toLowerCase()=="select" || child.firstChild.tagName.toLowerCase()=="button")
			{
				try { child.firstChild.focus(); } catch(e){}
				break;
			}
			child=child.nextSibling;
		}
	}
}

	//~~~~~~~~~~~~~~~~~TOOLBAR OBJECTS~~~~~~~~~~~~~~~~~~~~~~~//

PEGOEditor.Toolbar.Object=function(){}

/**
 * Enable/disable the object
 * @param newval Boolean; true=enable, false=disable
 */
PEGOEditor.Toolbar.Object.prototype.enable=function(newval)
{
	if (newval)
	{
		if(!this.enabled)
		{
			this.editor.removeClass(this.element, "buttonDisabled");
			this.element.disabled = false;
		}
	}
	else if(this.enabled)
	{
		this.editor.addClass(this.element, "buttonDisabled");
		this.element.disabled = true;
	}
	this.enabled=newval;
}

PEGOEditor.Toolbar.Object.prototype.setTabindex=function(index)
{
	this.element.tabindex=index;
}

/**
 * Enable/disable the button looking the current context (the element where the cursor is in and its ancestors)
 */
PEGOEditor.Toolbar.Object.prototype.updateState=function(ancestors,textmode)
{
	var inContext=true;
	if (this.context && !textmode)
	{
		var match = (this.context == "*");
		if(!match || this.contextAttrs.length==0)
		{
			inContext = false;
			for (var k in ancestors)
			{
				if (!ancestors[k])continue;// the impossible really happens.
				if (match || (ancestors[k].tagName.toLowerCase() == this.context))
				{
					inContext = true;
					for (var ka in this.contextAttrs)
						if (!eval("ancestors[k]." + this.contextAttrs[ka]))
						{
							inContext = false;
							break;
						}
					if (inContext) break;
				}
			}
		}
	}

	this.enable((!textmode || this.textmode) && inContext);
}

/**
 * Toolbar button object.
 * @param name String; the button name (i.e. 'bold')
 * @param element HTMLElement; the UI element
 * @param text boolean; enabled in text mode?
 * @param cmd String; the command ID
 * @param context String; enabled in a certain context?
 * @param activationa Function; function to determine if the button must be activated
 */
PEGOEditor.Toolbar.Button=function(editor,name,title,text,cmd,context,imgsrc,imgalt,activation)
{
	this.type="button";

	this.editor=editor;
	this.name=name;
	this.enabled=true;
	this.active=false;
	this.textmode=text;
	this.cmd=cmd;
	this.activation=activation;

	this.element=document.createElement("button");
	var element=this.element;

	element.title=title;

	var img = document.createElement("img");
	img.src = imgsrc;
	img.alt = imgalt;
	img.style.width = "18px";
	img.style.height = "18px";
	element.appendChild(img);

	this.context=context;
	this.contextAttrs=[];
	if (/(.*)\[(.*?)\]/.test(this.context))
	{
		this.context = RegExp.$1;
		this.contextAttrs = RegExp.$2.split(",");
	}

	var obj=this;
	editor.addEvent(element, "mouseover", function () {
		if (obj.enabled) {
			editor.addClass(element, "buttonHover");
		}
	});
	editor.addEvent(element, "mouseout", function () {
		if (obj.enabled) {
			editor.removeClass(element, "buttonHover");
			editor.removeClass(element, "buttonActive");
			(obj.active) && editor.addClass(element, "buttonPressed");
		}
	});
	editor.addEvent(element, "mousedown", function (ev) {
		if (obj.enabled) {
			editor.addClass(element, "buttonActive");
			editor.removeClass(element, "buttonPressed");
			editor.stopEvent(PEGOEditor.is_ie ? window.event : ev);
		}
	});
	// when clicked, do the following:
	editor.addEvent(element, "click", function (ev) {
		if (obj.enabled) {
			editor.removeClass(element, "buttonActive");
			editor.removeClass(element, "buttonHover");
			obj.cmd(editor, obj.name, obj);
			editor.stopEvent(PEGOEditor.is_ie ? window.event : ev);
		}
	});
}

PEGOEditor.Toolbar.Button.prototype=new PEGOEditor.Toolbar.Object();

/**
 * Activate/disactivate the button
 * @param newval Boolean; true=activate, false=disactivate
 */
PEGOEditor.Toolbar.Button.prototype.activate=function(newval)
{
	if (newval)
	{
		if(!this.active)
		{
			this.editor.addClass(this.element, "buttonPressed");
		}
	}
	else if(this.active)
	{
		this.editor.removeClass(this.element, "buttonPressed");
	}
	this.active=newval;
}

PEGOEditor.Toolbar.Select=function(editor,name,text,action,context,options,update)
{
	this.type="select";

	this.editor=editor;
	this.name=name;
	this.enabled=true;
	this.active=false;
	this.textmode=text;
	this.action=action;

	this.element = document.createElement("select");
	for (var i in options)
	{
		var op = document.createElement("option");
		op.appendChild(document.createTextNode(i));
		op.value = options[i];
		this.element.appendChild(op);
	}

	this.context=context;
	this.contextAttrs=[];
	if (/(.*)\[(.*?)\]/.test(this.context))
	{
		this.context = RegExp.$1;
		this.contextAttrs = RegExp.$2.split(",");
	}

	this.updateValue=update;

	var obj=this;
	editor.addEvent(this.element, "change", function () { obj.action(editor,obj); });
}

PEGOEditor.Toolbar.Select.prototype=new PEGOEditor.Toolbar.Object();

PEGOEditor.Toolbar.Select.prototype.getValue=function()
{
	return this.element.options[this.element.selectedIndex].value;
}

/**
 * Makes selected the option with the passed value.
 * @param val String;the value
 * @return true if the value is found; false otherwise
 */
PEGOEditor.Toolbar.Select.prototype.selectOptionWithValue=function(val)
{
	for(var el=this.element.firstChild;el;el=el.nextSibling)
	{
		if(el.tagName && el.tagName.toLowerCase()=="option" && el.value==val)
		{
			el.selected="selected";
			return true;
		}
	}
	return false;
}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

// appends a new button to toolbar
PEGOEditor.Toolbar.prototype.createObject=function(txt,config)
{
	// the element that will be created
	var el = null;

	var id=txt.match(/linebreak\|(.*)/);
	if(id) { id=id[1]; txt="linebreak"; }

	//~~~~~~~~~~~~~~~~SPECIAL~ELEMENTS~~~~~~~~~~~~~~~~~~//

	switch (txt)
	{
    case "separator":
		el = document.createElement("div");
		el.className = "separator";
		break;

	/*case "anchor":
	case "ancora":
		el = document.createElement("div");
		el.className = "anchor";
		var a = document.createElement("a");
		a.name="riga_"+this.contatore_ancore;
		a.id="riga_"+this.contatore_ancore;
		el.appendChild(a);
		a = document.createElement("a");
		a.href="#riga_"+this.contatore_ancore;
		a.title=this.editor.translate("Link to the","msg")+" "+this.contatore_ancore+this.editor.translate("nth line of buttons","msg");
		document.body.insertBefore(a,document.body.firstChild);
		this.contatore_ancore++;
		break;*/

	case "space":
		el = document.createElement("div");
		el.className = "space";
		break;

	case "linebreak":
		this.newLine(id);
		return false;
	}

	//~~~~~~~~~~~~~~~~~~BUTTON~~~~~~~~~~~~~//
	if (!el && config.btnList[txt])
	{
		var btn = config.btnList[txt];
		var obj=new PEGOEditor.Toolbar.Button(this.editor,txt,btn[0],btn[2],btn[3],btn[4] || null,btn[1],btn[0],btn[5] || null);
		obj.setTabindex(++this.tabindex);
		this.objects[txt] = obj;

		el = obj.element;
		//Bottone invisibile per accessibilita' in explorer
		if(txt=="firstButton") el.style.width=0;
	}
	//~~~~~~~~~~~~~~~~~~SELECT~~~~~~~~~~~~~//
	else if (!el && config.selectList[txt])
	{
		var sel=config.selectList[txt];
		el = this.createSelect(txt,sel);
	}
	//~~~~~~~~~~~~~~~~~~LABEL~~~~~~~~~~~~//

	if(!el) //anything else is just a label
	{
		el = document.createElement("div");
		el.appendChild(document.createTextNode(this.editor.translate(txt,"labels")));
		el.className = "label";
	}

	var cell = document.createElement("div");
	cell.className="cell";
	this.lines[this.lines.length-1].appendChild(cell);
	cell.appendChild(el);

	return el;
};

// this function will handle creation of combo boxes.Receives as
	// parameter the name of a button as defined in the toolBar config.
	// This function is called from createObject, above, if the given "txt"
	// doesn't match a button.
PEGOEditor.Toolbar.prototype.createSelect=function(id,sel)
{
	var options = null;
	var el = null;
	var cmd = null;
	var context = null;

	//[ ToolTip, Enabled in text mode?, options={option:value},ACTION, context, update]

	options = {};
	for(var i in sel[2])
	{
		options[this.editor.translate(i,id)]=sel[2][i];
	}

	var obj=new PEGOEditor.Toolbar.Select(this.editor,id,sel[1],sel[3],sel[4] || null,options,sel[5]);
	obj.setTabindex(++this.tabindex);
	this.objects[id] = obj;
	el=obj.element;

	return el;
};


// updates enabled/disable/active state of the toolbar elements
PEGOEditor.Toolbar.prototype.update = function(ancestors)
{
	var editor=this.editor;
	var doc = this.editor._doc;
	var textmode = (this.editor._editMode == "textmode");
	var lang_found=false;

	for (var i in this.objects)
	{
		var btn = this.objects[i];
		var cmd = i;

		btn.updateState(ancestors,textmode);
		if(btn.type=="button" && typeof btn.activation == "function") btn.activate(btn.activation(editor,ancestors));
		if(btn.type=="select" && typeof btn.updateValue == "function") btn.updateValue(editor,btn,ancestors);
	}
};

// Returns an array with all the ancestor nodes of the selection.
PEGOEditor.prototype.getAllAncestors = function()
{
	var element=this.getParentElement();
	var a = [];
	while (element && (element.nodeType == 1) && (element.tagName.toLowerCase() != 'body')) {
		a.push(element);
		element = element.parentNode;
	}
	a.push(this._doc.body);
	return a;
};

PEGOEditor.prototype.updateToolbar = function()
{
	var ancestors = this.getAllAncestors();
	this.toolbar.update(ancestors);
	this.statusBar.update(ancestors);

	for (var i in this.plugins)
	{
		var plugin = this.plugins[i].instance;
		if (typeof plugin.onUpdateToolbar == "function")
			try{ plugin.onUpdateToolbar(ancestors);} catch(e){if(e.message) alert(e.message)}
	}

	// take undo snapshots
	if (!this._timerUndo)
	{
		this.undoTakeSnapshot();
		var editor=this;
		this._timerUndo = setTimeout(function(){editor._timerUndo = null;}, this.config.undoTimeout);
	}
}

PEGOEditor.prototype.standardCheckButtonActivation=function(command)
{
	try
	{
		return (this._editMode != "textmode" && this._doc.queryCommandState(command));
	}
	catch(e)
	{
		return false;
	}
}

/**
 * Search in the passed ancestors list the first element matching the given id and returns its id
 */
PEGOEditor.prototype.searchIdInAncestors=function(ancestors,id)
{
	var el = ancestors[0];
	for(var i in ancestors)
		if(ancestors[i] && ancestors[i].id && ancestors[i].id.match(id)) return ancestors[i].id;

	return null;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                          STATUSBAR CLASS
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

PEGOEditor.StatusBar = function(editor,max_statusbar_links)
{
  this.editor=editor;
  this.maxLinks=max_statusbar_links;

  var statusbar = document.createElement("div");
  statusbar.className = "statusBar";
  statusbar.style.border="1px solid buttonShadow";

  //TODO: non ci credo!
  //Per l'ancora della statusbar in IE che se non ha href non va
	function vuoto(){};

	var a;
	if(PEGOEditor.is_ie) { a=document.createElement("a"); a.title="Ancora alla statusbar"; a.href="javascript:vuoto();"; a.appendChild(document.createTextNode(" "));}
	else a=document.createElement("a");
	a.name="statusbar_anchor";
	a.id="statusbar_anchor";
	statusbar.appendChild(a);


	var checkComb=document.createElement("input");
	if(PEGOEditor.is_ie)checkComb.style.styleFloat="right";
	else checkComb.style.cssFloat="right";
	checkComb.type="checkbox";
	checkComb.id="noComb";
	checkComb.title=PEGOEditor.I18N.tooltips["disable_key_shortcuts"];
	checkComb.onclick=function(){editor.noComb=checkComb.checked;};
	statusbar.appendChild(checkComb);

  var perc=document.createElement("span");
  perc.appendChild(document.createTextNode(PEGOEditor.I18N.msg["Path"] + ": "));
  perc.style.fontVariant="small-caps";
  perc.style.fontWeight="bold";
  statusbar.appendChild(perc);
  // creates a holder for the path view
  var div = document.createElement("span");
  div.className = "statusBarTree";
  div.innerHTML = "";
  var statusBarTree = div;


  var linkArray=new Array(max_statusbar_links);
  for (var i=0;i<max_statusbar_links;i++)
  {
	  if(i%2==1)
	  {
		  linkArray[i]=document.createElement("span");
		  linkArray[i].appendChild(document.createTextNode(" "));
	  }
	  else
	  {
		  var a=document.createElement("span");
		  a.className="statusbar_link";
		  //a.editor = this;
		  //a.href = "#";
		  //a.el=null;
		  //a.appendChild(document.createTextNode(""));
		  linkArray[i]=a;
	  }
	  statusBarTree.appendChild(linkArray[i]);
  }
  this.linkArray=linkArray;

  statusbar.appendChild(statusBarTree);

  if(editor.config.toolbarBackroundImage)
  {
    statusbar.style.backgroundImage='url("'+_editor_url+'/images/statusbar-back.jpg")';
    statusbar.style.backgroundPosition="bottom";
  }

  this.mainDiv=statusbar;

//this._htmlArea.appendChild(statusbar);

};

PEGOEditor.StatusBar.prototype.clear=function()
{
	var currentLink=0;
	for(currentLink=0;currentLink<this.maxLinks;currentLink++)
	{
		if(currentLink%2==0)	//pari: link
		{
			if(this.linkArray[currentLink].firstChild)
				this.linkArray[currentLink].removeChild(this.linkArray[currentLink].firstChild);
		}
		else	//dispari: spaziatori e frecce
		{
			this.linkArray[currentLink].firstChild.data="";
		}
	}
}

PEGOEditor.StatusBar.prototype.update=function(ancestors)
{
	this.clear();
	var editor=this.editor;

	var currentLink=0;
	for (var i = ancestors.length; --i >= 0;)
	{
		var el = ancestors[i];
		if (!el) 				// hell knows why we get here; this could be a classic example of why
		 continue; 				// it's good to check for conditions that are impossible to happen ;-)

		var tagName=el.tagName.toLowerCase();

		if(currentLink>=this.maxLinks) break;

		if(tagName=="html" || tagName=="body" || tagName=="tbody" || tagName=="tfoot" || tagName=="thead") continue; //sono inutili: non sono gestiti dall'editor

		var a=document.createElement("a");
		a.editor = editor;
		a.href = "#";
		a.appendChild(document.createTextNode(""));
		this.linkArray[currentLink].appendChild(a);
		a.el = el;

		a.title="";
		if (el.className) a.title += editor.translate("Class","statusbar_tags")+": "+ el.className+" ";
		if (el.id)a.title += " ID: " + el.id+" ";

		var txt = editor.translate(el.tagName.toLowerCase(),"statusbar_tags");
		if(el.lang) txt+="["+el.lang+"]";
		a.firstChild.data=txt;
		currentLink++;

		if(currentLink>=this.maxLinks) break;

		//eventualmente aggiunge le frecce
		if (i != 0) this.linkArray[currentLink].firstChild.data=String.fromCharCode(0xbb);

		currentLink++; //salta al link successivo
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//								PLUGINS
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

PEGOEditor.prototype.registerPlugin = function(plugin)
{
	if (typeof plugin == "string")
		plugin = eval("PEGOEditor."+plugin);
	var obj = new plugin(this);
	try
	{
		obj.init();
		var clone = {};
		clone.instance = obj;
		this.plugins[obj.name] = clone;
	}
	catch(e)
	{
		alert("Can't register plugin " + plugin.toString() + ": "+e);
	}
};


PEGOEditor.prototype.loadPlugin = function(pluginName)
{

	var editor=this;

	var dir = _editor_url + "plugins/" + pluginName;
	var plugin = pluginName.replace(/([a-z])([A-Z])([a-z])/g,
					function (str, l1, l2, l3) {
						return l1 + "-" + l2.toLowerCase() + l3;
					}).toLowerCase() + ".js";
	var plugin_file = dir + "/" + plugin;
	var plugin_lang = dir + "/lang/" + PEGOEditor.I18N.lang + ".js";

	anastasis.syncLoadScript(plugin_file);
	anastasis.syncLoadScript(plugin_lang);
	editor.registerPlugin(pluginName);

};

//----------------------------STYLE-----------------------------------------//

PEGOEditor.prototype.loadStyle = function(style, plugin) {
	var url = _editor_url;
	if (typeof plugin != "undefined") {
		url += "plugins/" + plugin + "/";
	}
	url += style;
	anastasis.loadStyle(url);
}

//--------------------------------------------------------------------------//

/**
 * Creates the PEGOEditor object and replaces the textarea with it. Loads the plugins.
 * @param text the text to be insrted in the editor
 */
PEGOEditor.prototype.generate = function (text)
{
	var editor = this;

	anastasis.syncLoadScript(this._editor_url + "lang/" + this.config.lang + ".js");
    this.I18NCollection=new Object();
    for(var i in PEGOEditor.I18N)
    {
    	this.I18NCollection[i]=this.getTranslator(PEGOEditor.I18N[i]);
    }

	if(this.system_params.plugins)
		for(var i in this.system_params.plugins)
		{
			this.loadPlugin(this.system_params.plugins[i]);
		}

	this.config.init(this._editor_url);

	// get the textarea
	var textarea = this._textArea;

	// it's not element but ID
	if (typeof textarea == "string")
		this._textArea = textarea = document.getElementById(textarea);

	this._ta_size = {
		w: textarea.offsetWidth,
		h: textarea.offsetHeight
	};
	textarea.style.display = "none";

	if(text)
	{
	  //Pulizia di sicurezza - per i filmati
    text=this.replaceHTMLOnGenerate(text);
    this._textArea.value=text;
  }

	// create the editor framework
	var divPEGOEditor = document.createElement("div");
	divPEGOEditor.className = "htmlarea";
	this._htmlArea = divPEGOEditor;

	// insert the editor before the textarea.
	textarea.parentNode.insertBefore(divPEGOEditor, textarea);

	// add a handler for the "back/forward" case -- on body.unload we save
	// the HTML content into the original textarea.
	window.onunload = function() { editor._textArea.value = editor.obtainMyHTML(); };

	// creates & appends the toolbar
	this.toolbar=new PEGOEditor.Toolbar(this,this.config);
	this._htmlArea.appendChild(this.toolbar.mainDiv);

  	// creates & appends the status bar, if the case
	if (this.config.statusBar)
	{
		this.statusBar=new PEGOEditor.StatusBar(this,this.config.max_statusbar_links);
		this.toolbar.mainDiv.appendChild(this.statusBar.mainDiv);
	}

	// create the IFRAME
	var iframe = document.createElement("iframe");
	divPEGOEditor.appendChild(iframe);

	this._iframe = iframe;

	// size the IFRAME according to user's prefs or initial textarea
	var height = 0;
	var width = 0;

  	height = (this.config.height == "auto" ? (this._ta_size.h + "px") : this.config.height);
	height = parseInt(height);
	width = (this.config.width == "auto" ? (this._ta_size.w + "px") : this.config.width);
	width = parseInt(width);


	if (PEGOEditor.is_ie) { height -= 5; width -= 5;}
	else { height -= 5; width -= 8;}

	iframe.style.width = width + "px";
	if (this.config.sizeIncludesToolbar) {
		// substract toolbar height
		height -= this.toolbar.getHeight();
	}
	if (height < 0) {
		height = 0;
	}
	iframe.style.height = height + "px";
	divPEGOEditor.original_height=height;

	// the editor including the toolbar now have the same size as the
	// original textarea.. which means that we need to reduce that a bit.
	textarea.style.width = iframe.style.width;
 	textarea.style.height = iframe.style.height;

 	//~~~ SOSTITUZIONI NEL TESTO ~~~//
 	text=this._textArea.value;
  if (PEGOEditor.is_gecko)  //Mozilla non usa strong ed em, ma <span style=...
  {
    text=text.replace(/<strong>/ig,'<span style="font-weight: bold;">');
    text=text.replace(/<em>/ig,'<span style="font-style: italic;">');
    text=text.replace(/<u>/ig,'<span style="text-decoration: underline;">');
    text=text.replace(/<strike>/ig,'<span style="text-decoration: line-through;">');
    text=text.replace(/<\/strong>/ig,'</span>');
    text=text.replace(/<\/em>/ig,'</span>');
    text=text.replace(/<\/u>/ig,'</span>');
    text=text.replace(/<\/strike>/ig,'</span>');
  }

  this._textArea.value=text;

		var doc = editor._iframe.contentWindow.document;
		if (!doc) { alert("ERROR: IFRAME can't be initialized."); return; }

		// enable editable mode for Mozilla
    if (PEGOEditor.is_gecko) { try {doc.designMode = "on"; }catch(e){alert(e);}}

    this._doc = doc;

		doc.open();
		var html = "<html>\n";
		html += "<head>\n";
		html += '<meta http-equiv="Content-type" content="text/xhtml; charset=ISO-8859-1" />\n';
		html +=	"<style></style>\n";

		if (editor.config.baseURL) html += '<base href="' + editor.config.baseURL + '" />';

		html+='\n<link rel="stylesheet" type="text/css" href="'+_editor_url+'internal.css">';

    for(var i in this.system_params.private_css)
    	html+='\n<link rel="stylesheet" type="text/css" href="'+this.system_params.private_css[i]+'">';
    for(var i in this.system_params.public_css)
    	html+='\n<link rel="stylesheet" type="text/css" href="'+this.system_params.public_css[i]+'">';

		//Carica dai plugin eventuali css interni alla PEGOEditor
		for (var i in editor.plugins) {
			var plugin = editor.plugins[i].instance;
			if (plugin.innerCSS)
			{
			 var url = _editor_url+"plugins/" + plugin.innerCSS;
			 html+='\n<link rel="stylesheet" type="text/css" href="'+url+'">';
			}
		}

		html += "</head>\n";
		html += '<body id="wysiwyg" style="background-color: #aaaaaa;">\n';
		// html += '<body style="background-color: #ffffff; background-image: none;">\n'; modifica fallita di pego
		html += editor._textArea.value;
		html += "</body>\n";
		html += "</html>";
		doc.write(html);
		doc.close();

		if (PEGOEditor.is_ie) {
			// enable editable mode for IE.	 For some reason this
			// doesn't work if done in the same place as for Gecko
			// (above).
			doc.body.contentEditable = true;
		}


		this.document_id=window.document.getElementById('ID_NODO') ? window.document.getElementById('ID_NODO').value : "";
		this.element_id=window.document.getElementById('ID_ELEMENTO') ? window.document.getElementById('ID_ELEMENTO').value : "";
		//~~~~~~~~~~~~~~~~~~VARIABILI~"GLOBALI"~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		PEGOEditor.document_id=window.document.getElementById('ID_NODO') ? window.document.getElementById('ID_NODO').value : "";
		PEGOEditor.element_id=window.document.getElementById('ID_ELEMENTO') ? window.document.getElementById('ID_ELEMENTO').value : "";

    //TODO: eliminare! provando con IE...
    PEGOEditor.searched = false;
    PEGOEditor.lastSearched="" ;
    PEGOEditor.found = false;
    PEGOEditor.ie_search_base='';

    PEGOEditor._editor=editor;

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



	// intercept some events; for updating the toolbar & keyboard handlers
	this.addEvents
		(doc, ["keydown", "keypress", "mousedown", "mouseup", "drag","dblclick"],
		 function (event) {
			 return editor.editorEvent(PEGOEditor.is_ie ? editor._iframe.contentWindow.event : event);
		 });

	this.addEvents
		(window, ["resize"],
		 function (event) {
			 return editor.windowEvent(PEGOEditor.is_ie ? window.event : event);
		 });

	this.addEvents
		(document, ["keydown", "keypress"],
		 function (event) {
			 return editor.documentEvent(PEGOEditor.is_ie ? window.event : event);
		 });

  //Aggiunge i link per l'accessibilita'
  /*
  var a=document.createElement("a");
  if(PEGOEditor.is_ie) a.accessKey="1";
  a.href="javascript: editor.focusEditor();"
  a.title="Vai al contenuto dell'editor";
  document.body.insertBefore(a,document.body.firstChild);

  a=document.createElement("a");
  if(PEGOEditor.is_ie) a.accessKey="3";
  a.href="#statusbar_anchor";
  a.title="Vai alla statusbar";
  document.body.insertBefore(a,document.body.firstChild);
  */
  this.initDocument(0);


};  //fine generate

PEGOEditor.prototype.initDocument=function(initCounter)
{
	var editor=this;

	setTimeout(function()
  	{
		try
  		{
			for (var i in editor.plugins)
			{
				var plugin = editor.plugins[i].instance;
				if (typeof plugin.onGenerate == "function") plugin.onGenerate(editor);
			}
			var body_clone=editor._doc.createElement("div");
		    editor.replaceOnGenerate(body_clone,editor._doc.body);
		    editor._doc.body.innerHTML=body_clone.innerHTML;
		    editor.resizeArea();
		    editor._doc.body.style.backgroundColor="";
		    editor.focusEditor();
		    editor.updateToolbar();
		    editor.loaded=true;
  		} catch(e)
  			{
  				if(initCounter<4)
  					editor.initDocument(initCounter+1);
  				else
  				{
  					editor.loaded=true;
  					alert(editor.translate("Errors during Editor loading","msg")+"...\n"+e);
  				editor._doc.body.style.backgroundColor="";
				    editor.focusEditor();
				    editor.updateToolbar();
  				}
  			}
	 },500);
}

PEGOEditor.prototype.replaceOnGenerate=function(parent,root)
{
	var editor=this;
	switch (root.nodeType)
	{
	case 1: // Node.ELEMENT_NODE
	case 11: // Node.DOCUMENT_FRAGMENT_NODE

		var newEl=root.cloneNode(false);

		for (var i in editor.plugins)
		{
			var plugin = editor.plugins[i].instance;
			var newElement;
			if (typeof plugin.onReplaceOnGenerate == "function") newElement=plugin.onReplaceOnGenerate(root);
			if(newElement) newEl=newElement;
		}

  	var el_tag = (newEl.nodeType == 1) ? newEl.tagName.toLowerCase() : '';

  	switch(el_tag)
	  {
        case "a":
      if(root.id.match(/anastasis_anchor/) && root.name && root.name.match(/anastasis_anchor_n_/))
  	  {
        var img=this._doc.createElement("img");

        var number=parseInt(root.name.match(/anastasis_anchor_n_(.*)/)[1]);
        if(!number && number!=0) break;
        if(this.anchor_id_index<number+1) this.anchor_id_index=number+1;

        root.removeChild(root.firstChild);

        img.id=root.name;
        img.name=root.name;
        img.alt="anchor # "+number;

        var link=this._doc.getElementById("anchor_link_"+img.id);
        var title="";
        if(link && link.firstChild) img.title=link.firstChild.data;

        img.className=root.className;
        img.src=this.imgURL("insert_anchor.gif");
        newEl=img;
      }
      break;

        case "span":    //IE
      if(!PEGOEditor.is_ie) break;
      var newtag=null;
      if(newEl.style.textDecoration && newEl.style.textDecoration.match(/underline/i)) newtag="u";
      else if(newEl.style.textDecoration && newEl.style.textDecoration.match(/line-through/i)) newtag="strike";
      else break;
      var tmp=this._doc.createElement(newtag);
      for(var i=newEl.firstChild;i;i=i.nextSibling) tmp.appendChild(i);
      newEl=tmp;
      break;
    }

    parent.appendChild(newEl);

		for (var i = root.firstChild; i; i = i.nextSibling)
     {
			this.replaceOnGenerate(newEl,i);
		 }

		break;

      case 3: // Node.TEXT_NODE
    parent.appendChild(root.cloneNode(true));
		break;
    //	    case 8: // Node.COMMENT_NODE
    //		break;
	}

}

PEGOEditor.prototype.replaceHTMLOnGenerate=function(html)
{
	for (var i in editor.plugins)
	{
		var plugin = editor.plugins[i].instance;
		if (typeof plugin.onReplaceHTMLOnGenerate == "function") html=plugin.onReplaceHTMLOnGenerate(html);
	}

	return html;
}



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                          EDITOR UTILITIES
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

 // Switches editor mode; parameter can be "textmode" or "wysiwyg".  If no
// parameter was passed this function toggles between modes.
PEGOEditor.prototype.setMode = function(mode)
{
	if (typeof mode == "undefined")
	{
		mode = ((this._editMode == "textmode") ? "wysiwyg" : "textmode");
	}
	else if(mode==this._editMode) return;

	switch (mode)
	{
	case "textmode":
	  this.wordClean();
	  var text=this.obtainMyHTML();
	  if(text)
	  {
	  	//text=text.replace(/>/g,">\n");
	  	//text=text.replace(/(.+)<\//g,"$1\n</");
	  }
		this._textArea.value = text;

		this._iframe.style.display = "none";
		this._textArea.style.display = "block";
		if (this.config.statusBar)
      {
        this.statusBar.clear();
      }

		break;
	    case "wysiwyg":
		if (PEGOEditor.is_gecko) // disable design mode before changing innerHTML
      {
			try
        {
				this._doc.designMode = "off";
			  } catch(e) {};
		  }
		var txt=this.obtainMyHTML();
		txt=txt.replace(/\t/g,"");
		txt=txt.replace(/\n/g,"");
		this._doc.body.innerHTML = txt;

		this._iframe.style.display = "block";
		this._textArea.style.display = "none";
		this._textArea.value = txt;		//replace the html without \n and \t

		if (PEGOEditor.is_gecko) // enable again design mode
      {
			try
        {
				this._doc.designMode = "on";
			  } catch(e) {};
		  }
		break;

	    default:
		alert("Mode <" + mode + "> not defined!");
		return false;
	}

	this._editMode = mode;
	this.focusEditor();
};



// The following function is a slight variation of the word cleaner code posted
// by Weeezl (user @ InteractiveTools forums).
PEGOEditor.prototype.wordClean = function() {
	var editor = this;
	var stats = {
			empty_tags : 0,
			mso_class  : 0,
			mso_style  : 0,
			mso_xmlel  : 0,
			orig_len   : this._doc.body.innerHTML.length,
			T          : (new Date()).getTime()
		};
	var stats_txt = {
			empty_tags : "Empty tags removed: ",
			mso_class  : "MSO class names removed: ",
			mso_style  : "MSO inline style removed: ",
			mso_xmlel  : "MSO XML elements stripped: "
		};
	function showStats() {
		var txt = "PEGOEditor word cleaner stats: \n\n";
		for (var i in stats)
			if (stats_txt[i])
				txt += stats_txt[i] + stats[i] + "\n";
		txt += "\nInitial document length: " + stats.orig_len + "\n";
		txt += "Final document length: " + editor._doc.body.innerHTML.length + "\n";
		txt += "Clean-up took " + (((new Date()).getTime() - stats.T) / 1000) + " seconds";
		alert(txt);
	};
	function clearClass(node) {
		var newc = node.className.replace(/(^|\s)mso.*?(\s|$)/ig, ' ');
		if (newc != node.className) {
			node.className = newc;
			if (!/\S/.test(node.className)) {
				node.removeAttribute("className");
				editor.word_pasted=true;
				++stats.mso_class;
			}
		}
	};
	function clearStyle(node) {
 		var declarations = node.style.cssText.split(/\s*;\s*/);
		for (var i = declarations.length; --i >= 0;)
			if (/^mso|^tab-stops/i.test(declarations[i]) ||
			    /^margin\s*:\s*0..\s+0..\s+0../i.test(declarations[i])) {
				++stats.mso_style;
				editor.word_pasted=true;
				declarations.splice(i, 1);
			}
		node.style.cssText = declarations.join("; ");
	};

	function getInnerText(el) {
		var txt = '', i;
		for (i = el.firstChild; i; i = i.nextSibling) {
			if (i.nodeType == 3)
				txt += i.data;
			else if (i.nodeType == 1)
				txt += getInnerText(i);
		}
		return txt;
	};

	function stripTag(el) {
		if (PEGOEditor.is_ie)
			el.outerHTML = editor.htmlEncode(el.innerText);
		else {
			var txt = document.createTextNode(getInnerText(el));
			el.parentNode.insertBefore(txt, el);
			el.parentNode.removeChild(el);
		}
		++stats.mso_xmlel;
	};
	function checkEmpty(el) {
		if (/^(a|span|b|strong|i|em|font)$/i.test(el.tagName) &&
		    !el.firstChild) {
			el.parentNode.removeChild(el);
			++stats.empty_tags;
		}
	};
	function parseTree(root) {
		var tag = root.tagName.toLowerCase(), i, next;
		if ((PEGOEditor.is_ie && root.scopeName != 'HTML') || (!PEGOEditor.is_ie && /:/.test(tag))) {
			stripTag(root);
			return false;
		} else {
			clearClass(root);
			clearStyle(root);
			for (i = root.firstChild; i; i = next) {
				next = i.nextSibling;
				if (i.nodeType == 1 && parseTree(i))
					checkEmpty(i);
			}
		}
		return true;
	};
	parseTree(this._doc.body);
	this.updateToolbar();
};

// focuses the iframe window.  returns a reference to the editor document.
PEGOEditor.prototype.focusEditor = function() {
	switch (this._editMode) {
	    case "wysiwyg" : this._iframe.contentWindow.focus(); break;
	    case "textmode": this._textArea.focus(); break;
	    default	   : alert("ERROR: mode " + this._editMode + " is not defined");
	}
};

// takes a snapshot of the current text (for undo)
PEGOEditor.prototype.undoTakeSnapshot = function() {
	++this._undoPos;
	if (this._undoPos >= this.config.undoSteps) {
		// remove the first element
		this._undoQueue.shift();
		--this._undoPos;
	}
	// use the fasted method (getInnerHTML);
	var take = true;
	var txt = this.getInnerHTML();
	if (this._undoPos > 0)
		take = (this._undoQueue[this._undoPos - 1] != txt);
	if (take) {
		this._undoQueue[this._undoPos] = txt;
	} else {
		this._undoPos--;
	}
};

//Svuota la cosa di undo: non si possono pi� annullare le ultime operazioni
PEGOEditor.prototype.resetUndo = function()
{
  var editor=this;
  for(var i=0;i<=this._undoPos;i++) this._undoQueue.shift();
  this._undoPos=0;
  this._timerUndo = setTimeout(function() {
			editor._timerUndo = null;
		}, this.config.undoTimeout);
}

PEGOEditor.prototype.undo = function() {
	if (this._undoPos > 0) {
		var txt = this._undoQueue[--this._undoPos];
		if (txt) this.setHTML(txt);
		else ++this._undoPos;
	}
};

PEGOEditor.prototype.redo = function() {
	if (this._undoPos < this._undoQueue.length - 1) {
		var txt = this._undoQueue[++this._undoPos];
		if (txt) this.setHTML(txt);
		else --this._undoPos;
	}
};

/** Returns a node after which we can insert other nodes, in the current
 * selection.  The selection is removed.  It splits a text node, if needed.
 */
PEGOEditor.prototype.insertNodeAtSelection = function(toBeInserted) {
	if (!PEGOEditor.is_ie) {
		var sel = this.getSelection();
		var range = this.createRange(sel);
		range.deleteContents();

		range.insertNode(toBeInserted);
		this._iframe.contentWindow.getSelection().collapseToEnd();
		range.detach();
    		/*		                          //Non sapevano di range.insertNode!?
    		// remove the current selection
    		sel.removeAllRanges();
    		range.deleteContents();
    		var node = range.startContainer;
    		var pos = range.startOffset;
    		switch (node.nodeType) {
    		    case 3: // Node.TEXT_NODE
    			// we have to split it at the caret position.
    			if (toBeInserted.nodeType == 3) {
    				// do optimized insertion
    				node.insertData(pos, toBeInserted.data);
    				range = this.createRange();
    				range.setEnd(node, pos + toBeInserted.length);
    				range.setStart(node, pos + toBeInserted.length);
    				sel.addRange(range);
    			} else {
    				node = node.splitText(pos);
    				var selnode = toBeInserted;
    				if (toBeInserted.nodeType == 11 ) {// Node.DOCUMENT_FRAGMENT_NODE
    					selnode = selnode.firstChild;
    				}
    				node.parentNode.insertBefore(toBeInserted, node);
    				this.selectNodeContents(selnode);
    				this.updateToolbar();
    			}
    			break;
    		    case 1: // Node.ELEMENT_NODE
    			var selnode = toBeInserted;
    			if (toBeInserted.nodeType == 11) { // Node.DOCUMENT_FRAGMENT_NODE
    				selnode = selnode.firstChild;
    			}
    			node.insertBefore(toBeInserted, node.childNodes[pos]);
    		  this.selectNodeContents(selnode);
    		  this._iframe.contentWindow.getSelection().collapseToEnd();
    			this.updateToolbar();
    			break;
    		}*/
	} else {
		return null;	// this function not used for IE
	}
};

// Returns the deepest node that contains both endpoints of the selection.
PEGOEditor.prototype.getParentElement = function() {
	var sel = this.getSelection();
	var range = this.createRange(sel);
	if (PEGOEditor.is_ie) {
		switch (sel.type) {
		    case "Text":
		    case "None":
			// It seems that even for selection of type "None",
			// there _is_ a parent element and it's value is not
			// only correct, but very important to us.  MSIE is
			// certainly the buggiest browser in the world and I
			// wonder, God, how can Earth stand it?
			return range.parentElement();
		    case "Control":
			return range.item(0);
		    default:
			return this._doc.body;
		}
	} else try {
		var p = range.commonAncestorContainer;
		if (!range.collapsed && range.startContainer == range.endContainer &&
		    range.startOffset - range.endOffset <= 1 && range.startContainer.hasChildNodes())
			p = range.startContainer.childNodes[range.startOffset];

		while (p.nodeType == 3) {
			p = p.parentNode;
		}
		return p;
	} catch (e) {
		return null;
	}
};



// Selects the contents inside the given node
PEGOEditor.prototype.selectNodeContents = function(node, pos) {
	this.focusEditor();
	var range;
	var collapsed = (typeof pos != "undefined");
	if (PEGOEditor.is_ie) {
		range = this._doc.body.createTextRange();
		range.moveToElementText(node);
		(collapsed) && range.collapse(pos);
		range.select();
	} else {
		var sel = this.getSelection();
		range = this._doc.createRange();
		range.selectNodeContents(node);
		(collapsed) && range.collapse(pos);
		sel.removeAllRanges();
		sel.addRange(range);
	}
};

/** Call this function to insert HTML code at the current position.  It deletes
 * the selection, if any.
 */
PEGOEditor.prototype.insertHTML = function(html) {
	var sel = this.getSelection();
	var range = this.createRange(sel);
	var check=false;
  if(PEGOEditor.is_ie)
  {
    if(!range.htmlText && sel.type.toLowerCase()!="none")
    {
      var tmpEl=this._doc.createElement("<div>");
      for (i = 0; i < range.length; i++) tmpEl.appendChild(range(i).cloneNode());
      return this.check_del(tmpEl);
    }
  	if(range.htmlText=="" && !this.is_ie8_or_later()) range.moveStart("character", -1);
    var tmpEl=this._doc.createElement("<div>");
    tmpEl.innerHTML=range.htmlText;
    check= this.check_del(tmpEl);
  }
	else
  {
    if(!range.collapsed)
    {
      var contents=range.cloneContents();
      check=this.check_del(contents);
    }
  }
	if(check) return false;
	if (PEGOEditor.is_ie)
  {
		range.pasteHTML(html);
	}
  else
  {
		// construct a new document fragment with the given HTML
		var fragment = this._doc.createDocumentFragment();
		var div = this._doc.createElement("div");
		div.innerHTML = html;
		while (div.firstChild) {
			// the following call also removes the node from div
			fragment.appendChild(div.firstChild);
		}
		// this also removes the selection
		var par=this.getParentElement();
		if(par.tagName.toLowerCase()=="html")
		  this._doc.body.appendChild(fragment);
		else
		  this.insertNodeAtSelection(fragment);
	}
	return true;
};

/// Retrieve the selected block
PEGOEditor.prototype.getSelectedHTML = function() {
	var sel = this.getSelection();
	var range = this.createRange(sel);
	var existing = null;
	if (PEGOEditor.is_ie) {
		existing = range.htmlText;
	} else {
		existing = this.getHTML(range.cloneContents(), false);
	}
	return existing;
};

/// Retrieve the selected text if it is all contained in a single TextNode, else return ""
PEGOEditor.prototype.getSelectedText = function()
{
	var text="";
	if(PEGOEditor.is_ie)
	{
		var sel = this.getSelection();
		var range = this.createRange(sel);
		if(!range.htmlText) return "";
		var tmpEl=this._doc.createElement("<div>");
		tmpEl.innerHTML=range.htmlText;
		if(tmpEl.innerText)
		{
			text=tmpEl.innerText;
		}
		else
		{
			if(tmpEl.firstChild.nodeType==3) text=tmpEl.firstChild.data;
			else if(tmpEl.firstChild.firstChild && tmpEl.firstChild.firstChild.nodeType==3) text=tmpEl.firstChild.firstChild.data;
		}
	}
	else
	{
		var sel = this.getSelection();
		var range = this.createRange(sel);
		var contents=range.cloneContents();
		if(contents && contents.textContent)
		{
			text=contents.textContent;
	   	}
		else if(contents.firstChild && contents.firstChild.nodeType==3)
		{
			text=contents.firstChild.data;
		}
	}
	return text;
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//                          EDITOR ACTIONS
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

PEGOEditor.prototype.sendPage= function(testo)
{

  for (var i in this.plugins)
  {
		var plugin = this.plugins[i].instance;
		if (typeof plugin.onSend == "function") plugin.onSend();
  }

  try
  {
    switch(this.system_params.editor_context)
	{
	case "embedded": break; //nothing

	case "popup":
		popup.addReturnParam("text",testo);
		popup.close();
		break;

	case "custom":
		if(typeof(this.system_params.send_function)=="function") this.system_params.send_function(testo);
	}
  } catch(e){alert(this.translate("An error occured during saving:","msg")+"\n"+e)}

  /*
   * var param=new Object();
        param["ID_NODO"]=PEGOEditor.document_id;
        param["ID_ELEMENTO"]=PEGOEditor.element_id;
        param["CONTENT"]=testo;
        //'L'invio e' sincrono: aspetta che sia arrivato prima di chiudere la finestra!
        var res=anastasis.ajax.send(param,_save_template_element_page,"POST",false,false);
        //alert(res);
        popup.addReturnParam("text",testo);
		popup.close();
   */

}

/**
 * Gets the HTML code of the page, puts it in the textarea and sends it
 */
PEGOEditor.prototype.save=function()
{
	var testo=this.getPageHTML();
	if(!this.checkPage(testo)) return;


	//Sostituisce tutti i simboli da  a  con la sequenza &#codice (non si sa mai)
  	for(var i=96;i<97;i++)
    {
      var reg=new RegExp('('+String.fromCharCode(i)+')');
      reg.global=true;
      while(testo.match(reg)) { testo=testo.replace(reg,'&#'+i+';');}
    }
    for(var i=127;i<256;i++)
    {
      var reg=new RegExp('('+String.fromCharCode(i)+')');
      reg.global=true;
      while(testo.match(reg)) { testo=testo.replace(reg,'&#'+i+';');}
    }

    //Sostituisce i simboli unicode
    for (var i in this.config._unicode_symbols)
    {
      var reg=new RegExp('('+String.fromCharCode(i)+')');
      reg.global=true;
      while(testo.match(reg)) {testo=testo.replace(reg,this.config._unicode_symbols[i]);}
    }

  //Elimino eventuali tag <body>
	testo=testo.replace(/^\s*<body id="wysiwyg">/,'');
	testo=testo.replace(/<\/body>\s*$/,'');

	for (var i in this.plugins)
	{
		var plugin = this.plugins[i].instance;
		if (typeof plugin.onSave == "function") testo=plugin.onSave(testo);
	}

  	this._textArea.value=testo;

	this.sendPage(testo);
}

PEGOEditor.prototype.cancel=function()
{
	if(!confirm(this.translate("Exit without saving","msg")+"?")) return false;

	switch(this.system_params.editor_context)
	{
	case "popup":
		popup.cancel();
		break;

	case "embedded": break; //nothing

	case "custom":
		if(typeof(this.system_params.cancel_function)=="function") 	this.system_params.cancel_function();
	}

	return true;
}

PEGOEditor.prototype.recClearColor=function(first,root,color,back)
{
if (root.nodeType==1 || root.nodeType==11)
  {
		var i;

		var root_tag = (root.nodeType == 1) ? root.tagName.toLowerCase() : '';

    var color=null;
    if(back && root.style) color=root.style.backgroundColor; else if(root.style) color=root.style.color;

		if(root_tag=="span" && color)
		{
        while(i=root.firstChild)
        {
  			 root.parentNode.insertBefore(i,root);
  		  }
        root.parentNode.removeChild(root);
        this.recClearColor(first,first,color,back);
        return;
    }

    for (i = root.firstChild; i; i = i.nextSibling)
    {
			this.recClearColor(first,i,color,back);
		}

	}
}

//Set the foreground or background color
PEGOEditor.prototype.setColor = function(command,color)
{
  var text="";

  if(PEGOEditor.is_ie)
  {
    var sel = this.getSelection();
    var range = this.createRange(sel);
    text=range.htmlText;
	}
	else
	{
    var sel = this.getSelection();
	  var range = this.createRange(sel);
	  var contents=range.extractContents();
	  var div=this._doc.createElement("div");
	  div.appendChild(contents);
	  this.recClearColor(div,div,color,(command=="forecolor") ? false : true);
	  text=div.innerHTML;
	}

	if(command=="forecolor")
    this.insertHTML('<span style="color: '+color+'">'+text+'</span>');
  else
    this.insertHTML('<span style="background-color: '+color+'">'+text+'</span>');
}

PEGOEditor.prototype._editColor = function(command)
{
	var editor=this;
	if(command=="hilitecolor" && PEGOEditor.is_ie) command = "backcolor";

	var outparam =
	{
		color : this.colorToRgb(this._doc.queryCommandValue(command))
	};

	var action=function(param)
	{
		if (param["color"])
        {
			editor.setColor(command,"#"+param["color"]);
		}
	}
	this.popupDialog("select_color.html", action , outparam);
}

PEGOEditor.prototype.isBlockElement = function(el)
{
	var blockTags = " body form textarea fieldset ul ol dl li div " +
		"p h1 h2 h3 h4 h5 h6 quote pre table thead " +
		"tbody tfoot tr td iframe address object";
	return (el.tagName && (blockTags.indexOf(" " + el.tagName.toLowerCase() + " ") != -1));
};

PEGOEditor.prototype._setDirection = function(command)
{
	var dir = (command == "righttoleft") ? "rtl" : "ltr";
	var el = this.getParentElement();
	while (el && !this.isBlockElement(el))
		el = el.parentNode;
	if (el)
	{
		if (el.style.direction == dir)
			el.style.direction = "";
		else
			el.style.direction = dir;
	}
}

// Called when the user clicks on "InsertImage" button.  If an image is already
// there, it will just modify it's properties.
PEGOEditor.prototype._insertImage = function(image)
{
	var editor = this;	// for nested functions
	var outparam = null

	if (typeof image == "undefined") {
		image = this.getParentElement();
		if (image && !/^img$/i.test(image.tagName))
			image = null;
	}

	if(image && (!image.id || !image.id.match(/^anastasis_uploaded/)))
	{
    	this.suggestButton(image.id,image.className);
    	return false;
	}

	if (image) outparam = {
		f_href     : PEGOEditor.is_ie ? editor.stripBaseURL(image.src) : image.getAttribute("src"),
		f_alt    : image.alt,
		f_border : parseInt(image.style.border),
		f_vert   : image.style.marginTop,          //tanto setto sempre sopra e sotto uguali!
		f_horiz  : image.style.marginLeft,        //tanto setto sempre destro e sinistro uguali!
		f_float  : PEGOEditor.is_ie ? image.style.styleFloat : image.style.cssFloat,
		modify:	true,
		I18N		: PEGOEditor.I18N.dialogs
	};
	else outparam = {
		create:	true,
		I18N		: PEGOEditor.I18N.dialogs
	};

	var action=function(param)
  {
	if (!param) {return false;}

    if(param["unlink"]=="anastasis_unlink") { image.parentNode.removeChild(image); return; }

    var img = image;
	if (!img)
    {
      var src="";
      var style="";

		var image_params=
		{
			src: param["f_href"],
			alt: param["f_alt"],
			border: param["f_border"]+'px solid; ',
			marginTop: param["f_vert"],
			marginBottom: param["f_vert"],
			marginLeft: param["f_horiz"],
			marginRight: param["f_horiz"],
			cssFloat: param["f_float"]
		}
		var html=editor.createImage(image_params);

    	editor.insertHTML(html);
	}
    else
    {
		for (field in param)
		{
			var value = param[field];
			switch (field)
			{
				case "f_href"   : if(value) {img.src = value;} break;
				case "f_alt"    : if(value) {img.alt = value;} else img.alt=" "; break;
				case "f_border" : img.style.border = parseInt(value || "0")+"px solid"; break;
				case "f_vert"   : img.style.marginTop = parseInt(value || "0");  img.style.marginBottom = parseInt(value || "0"); break;
				case "f_horiz"  : img.style.marginLeft = parseInt(value || "0"); img.style.marginRight = parseInt(value || "0"); break;
				case "f_float"  : if(PEGOEditor.is_ie) img.style.styleFloat=value; else img.style.cssFloat=value; break;
			}
		}
	}


	};

  this.popupDialog("insert_image.html", action, outparam);
};

/**
 * Creates the html code for an image.
 * Should always be used when inserting an image,
 * because it gives the image the ID anastasis_uploaded that is checked during saving.
 * @param params the attributes of the image(src,alt,border,marginTop,marginBottom,marginLeft,marginRight,cssFloat)
 * @return string; the html code of the image
 */
PEGOEditor.prototype.createImage=function(params)
{
	var html='<img id="anastasis_uploaded_'+randomnum()+'"';
	var style="";
	for (field in params)
	{
		var value = params[field];
		switch (field)
		{
			case "src"				: if(value) {html+= 'src="'+value+'" '; } else html+= 'src=" "'; break;
			case "alt"				: if(value) {html+= 'alt="'+value+'" '; html+= 'title="'+value+'" '; } else html+= 'alt=" "'; break;
			case "border" 			: if(value) {style+= 'border: '+value;} break;
			case "marginTop"		: if(value) {style+= 'margin-top: '+parseInt(value || "0")+'; '} break;
			case "marginBottom"		: if(value) {style+= 'margin-bottom: '+parseInt(value || "0")+'; '} break;
			case "marginLeft"		: if(value) {style+= 'margin-left: '+parseInt(value || "0")+'; '} break;
			case "marginRight"		: if(value) {style+= 'margin-right: '+parseInt(value || "0")+'; '} break;
			case "cssFloat"			: if(value) {style+= 'float: '+ value+"; "; } break;
	 	}
	}
	if(style) html+='style="'+style+'"';
	html+=' />';

	return html;
}

PEGOEditor.prototype._insertLink = function(res,type)
{
  var editor = this;	// for nested functions
	var outparam = null;
	var text="";
	var sel_img=null;
	var creating=false;

 if (!res)
  {
    res = editor.getParentElement();
		if (res) {
			if (/^img$/i.test(res.tagName))
				{ sel_img=res; res = res.parentNode; }
			if (!/^a$/i.test(res.tagName))
				res = null;
		}
	}

	if(!res)
	{
    	text=this.getSelectedText();
    	creating=true;
  	}
  	else
  	{
  		if( res.hasChildNodes() && res.firstChild) text=res.firstChild.data;
  	}

	if(res && res.id)
  {
  if(res.id.match("anastasis_nodelink"))
    {
    if (type!="nodelink") {alert(editor.translate("Use the button","dialogs")+" "+editor.translate("Insert/Modify Internal Node Link","dialogs")); return; }
    }
  else if(res.id.match(/anchor_link/))
    {
    alert(editor.translate("Use the button","dialogs")+" "+editor.translate("Insert/Modify Anchor","dialogs")); return;
    }
  else
    {
    if (type!="createlink") {alert(editor.translate("Use the button","dialogs")+" "+editor.translate("Insert/Modify Link","dialogs")); return; }
    }
  }

  var f_href ="";
  if(res) f_href = PEGOEditor.is_ie ? editor.stripBaseURL(res.href.replace(/\/$/,"")) : res.getAttribute("href");
  if(f_href && type=="nodelink")
  {
    var r=new RegExp(this.system_params.node_link.replace("?","\\\?")+"(\\\d*)");
    if(f_href.match(r)) f_href=f_href.match(r)[1];
  }

	if(!creating) outparam =
    {
	  f_text   : text,
	  f_href   : f_href,
		f_title  : res.title,
		type     : type,
		is_img   : (sel_img!=null),
		res_id: Math.round(Math.random()*2000000000),
		modify		:	true,
		I18N		: PEGOEditor.I18N.dialogs
		}
	else outparam =
    {
      		f_text   : text,
      		f_href   : "",
			f_title  : "",
			type     : type,
			is_img   : (sel_img!=null),
			res_id: Math.round(Math.random()*2000000000),
			create: true,
			I18N		: PEGOEditor.I18N.dialogs
    }

  var action=function(param)
  {
	if (!param) return false;

	 if(param["unlink"]=="anastasis_unlink")
	 {
	 	editor._doc.execCommand("unlink", false, null);
		editor.updateToolbar();
		return false;
	 }

	var img_text="";

	var href="";


	href=param["f_href"];

   	if(sel_img)
	{
    	img_text=editor.getHTML(sel_img,true);
    }

	if (creating)
    {
		if(type=="nodelink") href=editor.system_params.node_link+href;
  		var res_text="";
      	if (type=="nodelink") res_text='id="anastasis_nodelink_'+randomnum()+'"';
      	else res_text='id="anastasis_link_'+randomnum()+'"';
  		var title_text = param.f_title.trim();

  		if(sel_img && PEGOEditor.is_ie)
  		{
        	res=sel_img.parentNode;

	        var newel=editor._doc.createElement("a");
	        newel.href=href;
	        newel.title=title_text;
	        if(res_text) newel.id=res_text.substr(4,res_text.length-1);
	        newel.appendChild(editor._doc.createTextNode(param.f_text));
	         res.replaceChild(newel,sel_img);
	        newel.appendChild(sel_img);
	        return false;
      	}

  		editor.insertHTML('<a href="'+href+
                           '" title="'+ title_text+
                           '"'+res_text+' >' +
                           param.f_text+img_text+'</a>&nbsp;');
	}
	else
	{
		href = href.trim();
		if(type=="nodelink") href=editor.system_params.node_link+href;
		res.href = href;

		editor.selectNodeContents(res);
		if(res.hasChildNodes()) res.firstChild.data=param.f_text;
		else res.appendChild(editor.createTextNode(param.f_text));  //comunque non deve mai finire qua!
		res.title=param.f_title.trim();
	}

  }

  this.popupDialog("insert_link.html",action, outparam);
};

//TODO: remove reference to global variable PEGOEditor._editor...
PEGOEditor.prototype.doInsertAnchor = function()
{
   var editor=PEGOEditor._editor;
   //Questa cosa del timeout � un po' pericolosa, ma in IE non c'� altro modo!
   var timeout= PEGOEditor.is_ie ? 600 : 1;
   setTimeout(function(){
        if(editor.getParentElement()==PEGOEditor.mem) return;

        editor.removeEvent(editor._doc, "click",editor.doInsertAnchor);

        var link=editor._doc.getElementById("anchor_link_anastasis_anchor_n_"+(editor.anchor_id_index-1));
        var title="";
        if(link.firstChild) title=link.firstChild.data;
      	editor.insertHTML(' <img id="anastasis_anchor_n_'+(editor.anchor_id_index-1)+'" title="'+editor.translate("Anchor: ","msg")+title+'" name="anastasis_anchor_n_'+(editor.anchor_id_index-1)+'" alt="anchor #'+ (editor.anchor_id_index-1)+'" src="'+_editor_url +'images/insert_anchor.gif" />');
      	},timeout);
}


PEGOEditor.prototype._insertAnchor = function()
{
	var editor = this;	// for nested functions
	var outparam = null;
	var text="";
	var sel_img=null;

	anc = editor.getParentElement();

	if (anc)
    {
		if (/^img$/i.test(anc.tagName)) {sel_img=anc; anc = anc.parentNode; }
		if (!/^a$/i.test(anc.tagName))  {anc = null; }
	}

	if(!anc)
	{
		text=this.getSelectedText();
    }
	else
    {
		if( anc.hasChildNodes() && anc.firstChild) text=anc.firstChild.data;
    }

	if(anc && anc.id)
	{
		if(anc.id.match("anastasis_nodelink")){alert(editor.translate("Use the button","dialogs")+" "+editor.translate("Insert/Modify Internal Node Link","dialogs")); return; }
		else if(!anc.id.match(/anchor_link/)){alert(editor.translate("Use the button","dialogs")+" "+editor.translate("Insert/Modify Link","dialogs")); return; }
	}

	if(anc) outparam =
    {
		f_text   : text,
		f_href   : PEGOEditor.is_ie ? (editor.stripBaseURL(anc.href)).substr(1) : (anc.getAttribute("href")).substr(1),
		f_title  : anc.title,
		is_img   : (sel_img!=null),
		is_anc   : true,
		modify: true,
		I18N		: PEGOEditor.I18N.dialogs
	}
	else outparam =
    {
		f_text   : text ,
		f_href   : "anastasis_anchor_n_"+this.anchor_id_index,
		f_title  : "",
		is_img   : (sel_img!=null),
		is_anc   : (sel_img!=null && sel_img.id.match(/anastasis_anchor/)),
		create: true,
		I18N		: PEGOEditor.I18N.dialogs
    }

	var action=function(param)
	{
		if (!param) return false;
		if (!anc && !(sel_img!=null && sel_img.id.match(/anastasis_anchor/)))
		try
		{
			var title_text= param.f_title.trim();

			if(sel_img && PEGOEditor.is_ie)
			{
				anc=sel_img.parentNode;

				var newel=editor._doc.createElement("a");
				newel.href=param.f_href;
				newel.title=title_text;
				newel.id="anchor_link_"+param.f_href;
				newel.appendChild(editor._doc.createTextNode(param.f_text));

				anc.replaceChild(newel,sel_img);
				newel.appendChild(sel_img);
				return false;
			}

			var img_text="";
			if(sel_img) img_text=editor.getHTML(sel_img,true);

			editor.insertHTML('<a href="#'+param.f_href+
			 '" title="'+ title_text+
			 '" id="anchor_link_'+param.f_href+'"" >' +
			 param.f_text+img_text+'</a>&nbsp;');

 			editor.anchor_id_index++;
	 		PEGOEditor.mem=editor.getParentElement();

	 		editor.addEvent(editor._doc, "click",editor.doInsertAnchor);
		} catch(e) {}
		else
		{
	 		if (param["unlink"]=="anastasis_unlink")
			{
				if (sel_img && sel_img.id.match(/anastasis_anchor/))
				{
					var link=editor._doc.getElementById("anchor_link_"+sel_img.attributes["name"].value);
					if(link)
					{
					if(link.firstChild.data)
					{
						newel=editor._doc.createTextNode(link.firstChild.data);
						link.parentNode.replaceChild(newel,link);
					}
					else if (link.firstChild)	//e' un'immagine
					{
						newel=link.firstChild;
						link.parentNode.replaceChild(newel,link);
					}
					}
					sel_img.parentNode.removeChild(sel_img);
					return false;
				}
				editor.selectNodeContents(anc);
				editor._doc.execCommand("unlink", false, null);
				var img=editor._doc.getElementById(param["f_href"]);
				if (img) img.parentNode.removeChild(img);	//else c'� qualcosa che non va!
				editor.updateToolbar();
				return false;
			}
			else
			{
				var text = param.f_text.trim();
				if(anc.hasChildNodes()) anc.firstChild.data=param.f_text;
				else anc.appendChild(editor.createTextNode(param.f_text));//comunque non deve mai finire qua!
				anc.href = "#"+href;
				anc.target=param.f_target.trim();
				anc.title=param.f_title.trim();
			}
		}
	}
	this.popupDialog("insert_anchor.html",action, outparam);
};

// Called when the user clicks the Insert Table button
PEGOEditor.prototype._insertTable = function() {
	var editor = this;	// for nested functions
	var action = function(param)
  {
		if (!param) {	// user must have pressed Cancel
			return false;
		}
		editor.focusEditor();
		var doc = editor._doc;
		//L'unico modo per allineare la tabella in IE � metterla in un div con text-align...cosa che per� non funziona in Mozilla...
		//--> Ora la tabella sta SEMPRE dentro un div
		var div=doc.createElement("div");
		// create the table element
		var table = doc.createElement("table");

    //table.id="anastasis_table_"+randomnum();
		//div.id="div_"+table.id;

    // assign the given arguments
		for (var field in param) {
      var value = param[field];
      if (!value) { continue; }
      switch (field)
      {
          case "f_summary" : table.summary= value; break;
          case "f_width"   : table.style.width = value + "%"; break;
          case "f_align"   :
        switch(value)
        {
          case "left": table.style.marginLeft="0pt"; table.style.marginRight="auto"; break;
          case "center": table.style.marginLeft="auto"; table.style.marginRight="auto"; break;
          case "right": table.style.marginLeft="auto"; table.style.marginRight="0pt"; break;
        }
        div.style.textAlign=value;
        break;
          case "f_border"  : table.border	 = parseInt(value); break;
          case "f_spacing" : table.cellSpacing = parseInt(value); break;
          case "f_padding" : table.cellPadding = parseInt(value); break;
      }
    }

		var tbody = doc.createElement("tbody");
		table.appendChild(tbody);
		for (var i = 0; i < param["f_rows"]; ++i) {
			var tr = doc.createElement("tr");
			tbody.appendChild(tr);
			for (var j = 0; j < param["f_cols"]; ++j) {
				var td = doc.createElement("td");
				tr.appendChild(td);
				// Mozilla likes to see something inside the cell.
				(PEGOEditor.is_gecko) && td.appendChild(doc.createElement("br"));
				(PEGOEditor.is_ie) && td.appendChild(doc.createElement("p"));
				/*if(PEGOEditor.is_gecko)
				{
          var p=doc.createElement("p");
          p.appendChild(doc.createTextNode(" "));
          td.appendChild(p);
        }*/
			}
		}
		div.appendChild(table);
		if (PEGOEditor.is_ie) {
		  var sel = editor.getSelection();
	    var range = editor.createRange(sel);
			range.pasteHTML(div.outerHTML);
		} else {
			// insert the table
			editor.insertNodeAtSelection(div);
		}
		return true;
	}
	this.popupDialog("insert_table.html",action,null);
};

/**
 * Takes in input the id and the className of the selected element (if any).
 * Alerts a message with the suggestion of the correct button to be used in order to edit the element.
 * @param id String; the id of the selected element (or null)
 * @param className String; the class of the selected element (or null)
 */
PEGOEditor.prototype.suggestButton = function(id,className)
{
	var editor=this;

	for (var i in this.plugins)
	{
		var plugin = this.plugins[i].instance;
		var reply=null;
		if (typeof plugin.onSuggestButton == "function")
		{
			reply = plugin.onSuggestButton(id,className);
			if(reply)
			{
				alert(reply);
				return;
			}
		//se procede non ha trovato alcuna corrispondenza
		}
	}

	if(!id)
	{
		alert(editor.translate("Wrong button. Use the proper one to edit the element."));
	}
	else
	{
		if(id.match(/^anastasis_uploaded/))
			alert(editor.translate("Use the button","dialogs")+" '"+editor.translate("Insert/Modify Image","dialogs")+"' "+editor.translate("to edit this element.","dialogs"));
		else if(id.match(/^anastasis_nodelink/))
			alert(editor.translate("Use the button","dialogs")+" '"+editor.translate("Insert/Modify Internal Node Link","dialogs")+"' "+editor.translate("to edit this element.","dialogs"));
		else if(id.match(/^anchor_link/))
			alert(editor.translate("Use the button","dialogs")+" '"+editor.translate("Insert/Modify Anchor","dialogs")+"' "+editor.translate("to edit this element.","dialogs"));
		else if(id.match(/^anastasis_link/))
			alert(editor.translate("Use the button","dialogs")+" '"+editor.translate("Insert/Modify Link","dialogs")+"' "+editor.translate("to edit this element.","dialogs"));

		else alert(editor.translate("Wrong button. Use the proper one to edit the element."));
	}
}

PEGOEditor.prototype._changeLang = function()
{
  var editor = this;	// for nested functions
	var outparam = null;

	this.popupDialog("change_language.html", function(param) {
		if (!param) {	return false; }       // user must have pressed Cancel
    var text=editor.getSelectedHTML();
    text='<span lang="'+param["lang"]+'">'+text+'</span>';
    editor.insertHTML(text);
     },outparam);
};



PEGOEditor.prototype._pasteunformatted = function()
{
  	var editor = this;	// for nested functions
	var outparam = null;
	var action=function(param)
	{
		editor.insertHTML(param["f_text"]);
	}
	editor.popupDialog("paste_unformatted.html",action, outparam);
}



                  //~~~~~~~~~FIND~AND~REPLACE~~~~~~~~~~~~~~//

PEGOEditor.prototype._findreplace = function()
{
  var editor = this;	// for nested functions
	var outparam = null;
	var text="";

  text=editor.getSelectedText();
  if(!text && PEGOEditor.lastSearched)text=PEGOEditor.lastSearched;

  var action=function(param)
	{
		var text=param["f_text"];
		var repl=param["f_replace"];
		var caseMatch=param["f_case"];

		if(param["action"]=="find")
			editor.search(text,null,false,caseMatch);
		else if(param["action"]=="replace")
			editor.search(text,repl ? repl : "anastasis_vuoto",false,caseMatch);
		else if(param["action"]=="replace_all")
			editor.search(text,repl ? repl : "anastasis_vuoto",true,caseMatch);
	}

	outparam =
  	{
		f_text   : text
	}
	editor.popupDialog("find_replace.html",action, outparam);
}


PEGOEditor.prototype.ie_search=function(text,repl,all,caseMatch,all2)
{
  if(PEGOEditor.lastSearched!=text) PEGOEditor.searched=false;
  if(repl && !all)
    {
      if(this.getSelectedText()==text) { this.insertHTML(repl);	return; }
    }
  if (!PEGOEditor.searched) {PEGOEditor.ie_search_base =this._doc.selection.createRange(); PEGOEditor.lastSearched=text;}
  PEGOEditor.found=PEGOEditor.ie_search_base.findText(text,1000000000,caseMatch ? 4 : 0);
  if (PEGOEditor.found)
  {
    PEGOEditor.ie_search_base.findText(text,1000000000,caseMatch ? 4 : 0);
    PEGOEditor.ie_search_base.select();
    PEGOEditor.ie_search_base.scrollIntoView();
    PEGOEditor.searched=true;
    if(repl) this.insertHTML((repl!="anastasis_vuoto") ? repl : "");
    PEGOEditor.ie_search_base.moveStart("character", 1);
    PEGOEditor.ie_search_base.moveEnd("textedit");
    if(all) this.ie_search(text,repl,all,caseMatch,all2);
  }
  else
  {
    if (!PEGOEditor.searched) alert('"' + text +'" '+this.translate("was not found in this page","msg"));
    else
    {
      if(PEGOEditor.searched && all)
      {
      if(all2)return;
      if(confirm(this.translate("Reached end of document, start from the top of the page?","msg")))
        { PEGOEditor.ie_search_base =this._doc.body.createTextRange(); this.ie_search(text,repl,all,caseMatch,true); }
      return;
      }
      PEGOEditor.lastSearched="" ;
      PEGOEditor.searched=false;
      this._doc.selection.empty();
      PEGOEditor.ie_search_base =this._doc.body.createTextRange();
      alert(this.translate("Reached end of document without findig any more occurrencies of ","msg")+' "'+text+'"');

    }
    PEGOEditor.searched=false;
  }
}

PEGOEditor.prototype.moz_search=function(text,repl,all,caseMatch,all2)
{
  if(PEGOEditor.lastSearched!=text) PEGOEditor.searched=false;

  if(repl && !all)
    {
      if(this.getSelectedText()==text) { this.insertHTML(repl);	return; }
    }

  if (!PEGOEditor.searched) {PEGOEditor.lastSearched=text;}

  PEGOEditor.found=this._iframe.contentWindow.find(text,caseMatch);

  if (PEGOEditor.found)
  {
    PEGOEditor.searched=true;
    if(repl) this.insertHTML((repl!="anastasis_vuoto") ? repl : "");
    if(all) this.moz_search(text,repl,all,caseMatch,all2);
  }
  else
  {
    if (!PEGOEditor.searched) alert('"' + text +'" '+this.translate("was not found in this page","msg"));
    else
    {
      if(PEGOEditor.searched && all)
      {
      if(all2)return;
      if(confirm(this.translate("Reached end of document, start from the top of the page?","msg")))
        {
          var r=this._doc.createRange();
          r.selectNode(this._doc.body.firstChild);
          sel=this._iframe.contentWindow.getSelection();
          sel.removeAllRanges();
			    sel.addRange(r);
          this.moz_search(text,repl,all,caseMatch,true);
        }
      return;
      }
      PEGOEditor.lastSearched="" ;
      PEGOEditor.searched=false;
      var r=this._doc.createRange();
      r.selectNode(this._doc.body.firstChild);
      sel=this._iframe.contentWindow.getSelection();
      sel.removeAllRanges();
		sel.addRange(r);
      alert(this.translate("Reached end of document without findig any more occurrencies of","msg")+' "'+text+'"');
      anastasis.current_dialog.finestra.focus();
    }
    PEGOEditor.searched=false;
  }

}

PEGOEditor.prototype.search=function(text,repl,all,caseMatch)
{
  var reg=new RegExp(text);
  PEGOEditor.found=false;

  if(PEGOEditor.is_ie)this.ie_search(text,repl,all,caseMatch);
	else this.moz_search(text,repl,all,caseMatch);
}

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

PEGOEditor.prototype.recClearStyle=function(root)
{
if (root.nodeType==1 || root.nodeType==11)
  {
		var i;
		var root_tag = (root.nodeType == 1) ? root.tagName.toLowerCase() : '';
    for (var i in root.style) try { root.style[i]=null; } catch(e){}
		if(root.className) root.className="";

		if(root_tag=="font")
		{
  		  for (i = root.firstChild; i; i = i.nextSibling)
        {
  			 root.parentNode.insertBefore(i,root);
  		  }
        root.parentNode.removeChild(root);
    }

		for (i = root.firstChild; i; i = i.nextSibling)
    {
			this.recClearStyle(i);
		}
	}
}

PEGOEditor.prototype._clearStyle=function()
{
  this.recClearStyle(this._doc.body);
}

//IE usa l'attributo align, che � decprecato, quindi va fatto tutto a mano
//L'allineamento viene applicato all'elemento p o li pi� vicino
//se non ce n'�, ne crea uno
PEGOEditor.prototype._ie_justify=function(jus)
{
  var editor=this;
  var el = editor.getParentElement();
  if(!el || !el.tagName) return;
  var tagName=el.tagName.toLowerCase();
  if(tagName=="p" || tagName=="li" || tagName=="td" || tagName=="th") { el.style.textAlign=jus; return; }
  else
  {
    var p=el.parentNode;
    while(p && p.tagName)
    {
      var tag=p.tagName.toLowerCase();
      if(tag=="p" || tag=="td" || tag=="th" || tag=="li") break;
      p=p.parentNode;
    }
  }
  if(p && p.tagName)
    {
      tagName=p.tagName.toLowerCase();
      //non mi piace molto assegnarlo anche a td e th, ma Mozilla fa cos�...
      if(tagName=="p" || tagName=="li" || tagName=="td" || tagName=="th") p.style.textAlign=jus;
      else p=null;
    }
  else p=null;

  if(!p)
  {
    p=this._doc.createElement("p");
    p.style.textAlign=jus;

    if(el.tagName.toLowerCase()=="body")//   Qua si finisce SE E SOLO SE c'� solo del testo nel body e nient'altro!
    {
      this.execCommand("formatblock", false,"<p>");
      p = editor.getParentElement();
      p.style.textAlign=jus;
      return;
    }

    p.appendChild(el.cloneNode(true));
    el.parentNode.replaceChild(p,el);
  }
}

//Mozilla non usa strong e em! Quindi glileo facciamo usare noi!
/*
PEGOEditor.prototype._moz_setWeigth = function(command)
{
  var text="";

  var sel = this.getSelection();
  var range = this.createRange(sel);
  var contents=range.extractContents();
  var div=this._doc.createElement("div");
  div.appendChild(contents);
  text=div.innerHTML;

  this.insertHTML('<'+command+'>'+text+'</'+command+'>');
}*/

//IE usa il tag blockquote, che fa casino, quindi va fatto tutto a mano
//L'allineamento viene applicato all'elemento p o li pi� vicino
//se non ce n'�, ne crea uno
//Invece per le lista lascia fare ad IE
PEGOEditor.prototype._ie_indent=function(cmd)
{
  var editor=this;
  var el = editor.getParentElement();
  if(!el || !el.tagName) return;
  var tagName=el.tagName.toLowerCase();
  var p=el;
  var tagName=null;

  while(p && p.tagName)
  {
    tagName=p.tagName.toLowerCase();
    if(tagName=="p" || tagName=="li" || tagName=="td" || tagName=="th") break;
    p=p.parentNode;
  }

  if(!p) p==el;

  if(tagName=="p")
  {
    if(!p.style.marginLeft) { if (cmd=="indent") p.style.marginLeft="40px"; }
    else
      if(cmd=="indent") p.style.marginLeft=(parseInt(p.style.marginLeft)+40)+"px";
      else { p.style.marginLeft=(parseInt(p.style.marginLeft)-40)+"px"; if(p.style.marginLeft=="0px") p.style.marginLeft=""; }
    return;
  }
  else if(tagName=="li")  this._doc.execCommand(cmd);
  else if((tagName=="td" || tagName=="th") && cmd=="indent")
      {
        if(!p.firstChild) return;
        var newp=this._doc.createElement("p");
        newp.style.marginLeft="40px";
        for (i = p.firstChild; i; i = i.nextSibling) newp.appendChild(i);
        p.appendChild(newp);
        return;
      }
  else if(cmd=="indent")  //se non � n� un p n� una cella n� un li...ficca el dentro un paragrafo
  {
    p=this._doc.createElement("p");
    p.style.marginLeft="40px";
    p.appendChild(el.cloneNode(true));
    el.parentNode.replaceChild(p,el);
  }
}

/***************************************************
 *  Category: EVENT HANDLERS
 ***************************************************/


// the execCommand function (intercepts some commands and replaces them with
// our own implementation)
PEGOEditor.prototype.execCommand = function(cmdID, UI, param)
{
	if(!this.loaded) return;

	try {

	var editor = this;	// for nested functions
	this.focusEditor();

	//PEGOEditor.nothing_to_do();
	cmdID = cmdID.toLowerCase();
	switch (cmdID)
  	{
	case "htmlmode" :
    	this.setMode();
    	break;

	case "hilitecolor":
	case "forecolor":
		this._editColor(cmdID);
		break;

	case "createlink": this._insertLink(null,cmdID); break;

	case "insertanchor": this._insertAnchor(); break;

	case "undo": this.undo(); break;

	case "redo": this.redo(); break;

	case "inserttable": this._insertTable(); break;

	case "insertimage": this._insertImage(); break;

	case "about"    : this.popupDialog("about.html", null, this); break;

	case "showhelp" : window.open(editor.system_params.help_url, "help"); break;

	case "lefttoright":
	case "righttoleft":
		this._setDirection(cmdID);
		break;

	case "save":
    	this.save();
		break;

	case "preview":
		alert("Preview function not yet implemented");
		break;

	case "cancel":
		this.cancel();
	    break;

	case "changelang": this._changeLang(); break;

	case "nodelink": this._insertLink(null,cmdID); break;

	case "findreplace": this._findreplace(); break;

	case "clearstyle": this._clearStyle(); break;

	case "justifyleft":
		if(!PEGOEditor.is_ie) this._doc.execCommand(cmdID, UI, param);
		else this._ie_justify("left");
		break;

	case "justifycenter":
    	if(!PEGOEditor.is_ie) this._doc.execCommand(cmdID, UI, param);
    	else this._ie_justify("center");
    	break;

	case "justifyright":
    	if(!PEGOEditor.is_ie) this._doc.execCommand(cmdID, UI, param);
    	else this._ie_justify("right");
    	break;

	case "justifyfull":
    	if(!PEGOEditor.is_ie)	this._doc.execCommand(cmdID, UI, param);
		else this._ie_justify("justify");
    	break;

	case "outdent":
    	if(!PEGOEditor.is_ie) this._doc.execCommand(cmdID, UI, param);
    	else this._ie_indent("outdent");
    	break;

	case "indent":
    	if(!PEGOEditor.is_ie)	this._doc.execCommand(cmdID, UI, param);
    	else this._ie_indent("indent");
    	break;

	case "deleteall":
    	if(confirm(this.translate("Delete all?","msg"))) { if(!this.check_del(this._doc.body)) this._doc.body.innerHTML="<p> </p>"; }
    	this.selectNodeContents(this._doc.body.firstChild);
    	break;

	case "pasteunformatted": this._pasteunformatted(); break;

	default: this._doc.execCommand(cmdID, UI, param);
	}

	this.updateToolbar();
	return false;

} catch(e) {alert("JavaScript Error: execCommand("+cmdID+")\n"+e);}

};

PEGOEditor.prototype.resizeArea=function()
{
  if(this.config.height != "auto" && this.config.width != "auto") return;

  var iframe=this._iframe;
  var textarea=this._textArea;

  //alert(textarea.offsetHeight);

  textarea.style.width="100%";
  this._ta_size = {
		w: textarea.offsetWidth,
		h: textarea.offsetHeight
	};

	if(this.config.width== "auto")
	{
    iframe.style.width = "100%";
    textarea.style.width = iframe.style.width;
  }
	if(this.config.height == "auto")
	{
    var height=0;
    var mult=1;
    if(this.system_params.editor_context=="custom") mult=this.system_params.height_percentage;
  	if(!PEGOEditor.is_ie) height=window.innerHeight-parseInt(window.innerHeight/100*mult);
  	else height=document.documentElement.clientHeight-parseInt(document.documentElement.clientHeight/100*mult);

  	if (this.config.sizeIncludesToolbar)
    {
  		height -= this.toolbar.getHeight();   // substract toolbar height
  	}

  	if(this.is_ie7_or_later()) height-=40;

  	if (height < 0) height = 0;

    iframe.style.height = height + "px";
   	textarea.style.height = iframe.style.height;

  }
}

    //~~~~~~~~~~~~~~WINDOW_EVENT~~~~~~~~~~~~~~//
//Combinazioni di tasti intercettate:
//  Alt+1 : focus sull'area di lavoro
//  Alt+2 : focus sulla toolbar
//  Alt+3 : focus sulla statusbar

PEGOEditor.prototype.windowEvent = function(ev)
{
	var editor = this;

	if(ev.type=="resize")
	{
    	this.resizeArea();
    	return;
	}
}

PEGOEditor.prototype.documentEvent = function(ev)
{
	var keyEvent = (PEGOEditor.is_ie && ev.type == "keydown") || (ev.type == "keypress");
	if (keyEvent && ev.altKey)
	{
		var key = String.fromCharCode(PEGOEditor.is_ie ? ev.keyCode : ev.charCode).toLowerCase();

		if(key=="0")
		{
			this.focusEditor();
		}
		else if(key.match(/\d/))
		{
			this.toolbar.focusOnLine(key);
		}
	}
}

// A generic event handler for things that happen in the IFRAME's document.
// This function also handles key bindings.
PEGOEditor.prototype.editorEvent = function(ev)
{
	var editor = this;

	if(ev.type=="dblclick")
	{
		this.doubleClick();
		return;
  	}


	var keyEvent = (PEGOEditor.is_ie && ev.type == "keydown") || (ev.type == "keypress");
	if (keyEvent)
	{
		if(!editor.loaded) editor.stopEvent(ev);
		for (var i in editor.plugins) {
			var plugin = editor.plugins[i].instance;
			if (typeof plugin.onKeyPress == "function") plugin.onKeyPress(ev);
		}
	}

	if (keyEvent && ev.ctrlKey)
    {
    var key = String.fromCharCode(PEGOEditor.is_ie ? ev.keyCode : ev.charCode).toLowerCase();
    switch (key)
      {
          case 'z': this.execCommand("undo", false, null); editor.stopEvent(ev); break;
          case 'y': this.execCommand("redo", false, null); editor.stopEvent(ev); break;
          case 'x': if(PEGOEditor.is_ie) this.ie_checkBackspace(); else this.dom_checkBackspace(); break;
      }
     if(!editor.noComb)
      switch(key)
       {
           case 'b': this.execCommand("bold", false, null); editor.stopEvent(ev); break;
           case 'i': this.execCommand("italic", false, null); editor.stopEvent(ev); break;
           case 'u': this.execCommand("underline", false, null); editor.stopEvent(ev); break;
           case 's': this.execCommand("strikethrough", false, null); editor.stopEvent(ev); break;
           case 'l': this.execCommand("createlink", false, null); editor.stopEvent(ev); break;
           case 'a': this.execCommand("insertimage", false, null); editor.stopEvent(ev); break;
           case 't': this.execCommand("inserttable", false, null); editor.stopEvent(ev); break;
           case 'f': this.execCommand("findreplace", false, null); editor.stopEvent(ev); break;
       }

    }
    else if (keyEvent && ev.altKey)
  {
  var key = String.fromCharCode(PEGOEditor.is_ie ? ev.keyCode : ev.charCode).toLowerCase();
  if(key=="0")
  {
  	this.focusEditor();
  }
  else if(key.match(/\d/))
    {
    	this.toolbar.focusOnLine(key);
    }
  }
  else

	if (keyEvent)
  {
		// other keys here
		switch (ev.keyCode)
    {
		    case 8: // KEY backspace
		    case 46: // KEY delete
			if (PEGOEditor.is_gecko && !ev.shiftKey)
        { if (this.dom_checkBackspace()) editor.stopEvent(ev); }
      else if (PEGOEditor.is_ie)
        { if (this.ie_checkBackspace()) editor.stopEvent(ev);}
			break;
			   case 9: 	//KEY tab
			if(!PEGOEditor.is_ie) break;
      this.insertHTML("&nbsp;&nbsp;");
      editor.stopEvent(ev);
      break;
		}

	}

	// update the toolbar state after some time
	if (editor._timerToolbar) {
		clearTimeout(editor._timerToolbar);
	}
	editor._timerToolbar = setTimeout(function() {
		editor.updateToolbar();
		editor._timerToolbar = null;
	}, 50);
};

PEGOEditor.prototype.doubleClick=function()
{
	var editor=this;

	var root=editor.getParentElement();

	for (var i in this.plugins)
	{
		var plugin = this.plugins[i].instance;
		var reply=null;
		if (typeof plugin.onDoubleClick == "function")
		{
			if(plugin.onDoubleClick(root)) return;
			//se procede non ha trovato alcuna corrispondenza
		}
	}

	if(!root.id)
		return;
    else if (root.id.match(/^anastasis_uploaded/))
		  editor.execCommand("insertimage");
    else if (root.id.match(/^anastasis_nodelink/))
		  editor.execCommand("nodelink");
    else if (root.id.match(/^anastasis_link/))
		  editor.execCommand("createlink");
    else if (root.id.match(/^anastasis_anchor/))
		  editor.execCommand("insertanchor");

}

// retrieve the HTML
PEGOEditor.prototype.obtainMyHTML = function()
{
	switch (this._editMode)
	{
	    case "wysiwyg"  : return this.getHTML(this._doc.body, false,true,-1);
	    case "textmode" : return this._textArea.value;
	    default	    : alert("Mode <" + mode + "> not defined!");
	}
	return false;
};

// retrieve the HTML (fastest version, but uses innerHTML)
PEGOEditor.prototype.getInnerHTML = function() {
	switch (this._editMode) {
	    case "wysiwyg"  :	return this._doc.body.innerHTML;
	    case "textmode" : return this._textArea.value;
	    default	    : alert("Mode <" + mode + "> not defined!");
	}
	return false;
};

// completely change the HTML inside
PEGOEditor.prototype.setHTML = function(html) {
	switch (this._editMode) {
	    case "wysiwyg"  :	this._doc.body.innerHTML = html; break;
	    case "textmode" : this._textArea.value = html; break;
	    default	    : alert("Mode <" + mode + "> not defined!");
	}
	return false;
};


/***************************************************
 *  Category: UTILITY FUNCTIONS
 ***************************************************/

// selection & ranges

// returns the current selection object
PEGOEditor.prototype.getSelection = function() {
	if (PEGOEditor.is_ie) {
		return this._doc.selection;
	} else {
		return this._iframe.contentWindow.getSelection();
	}
};

// returns a range for the current selection
PEGOEditor.prototype.createRange = function(sel) {
	if (PEGOEditor.is_ie)
	{
		return sel.createRange();
	}
	else
	{
		if (typeof sel != "undefined")
		{
			try
			{
				return sel.getRangeAt(0);
			} catch(e)
			{
				return this._doc.createRange();
			}
		}
		else
		{
			return this._doc.createRange();
		}
	}
};

PEGOEditor.prototype.getPageHTML= function()
{
  this.wordClean();

  var body_clone=this._doc.createElement("div");

  this.replaceElements(body_clone,this._doc.body);

  var testo=this.getHTML(body_clone, true);
  testo=testo.replace(/^\s*/,'');
  testo=testo.replace(/^\s*<div>/,'');
  testo=testo.replace(/<\/div>\s*$/,'');
  testo=testo.replace(/\s*$/,'');

  return testo;
}

PEGOEditor.prototype.replaceElements= function(parent,root)
{
var editor=this;
switch (root.nodeType)
{
	    case 1: // Node.ELEMENT_NODE
	    case 11: // Node.DOCUMENT_FRAGMENT_NODE

	  var replaced=null;
	  var newEl=root.cloneNode(false);

	  		    //In IE si mangia il contenuto di <script> e da' quintali di errori stronzi!
    if(root.tagName.toLowerCase()=="script")
    {
		if(PEGOEditor.is_ie) alert(editor.translate("Warning: the page contains scripts. The Editor, due to some bugs of Internet Explorer, cannot save them. Please use Mozilla (download for free at mozilla.org).","msg"));
		else alert(editor.translate("Warning: the page contains scripts. The Editor can not manage script comments (//), if any delete them from the script.","msg"));
    }

    //passa l'elemento ai plugin che eventualmente devono sostituirlo
	for (var k in editor.plugins)
    {
      var plugin = editor.plugins[k].instance;
      var newElement;
	    if (typeof plugin.onReplace == "function") newElement=plugin.onReplace(root);
	    if(newElement) newEl=newElement;
    }

  	var el_tag = (newEl.nodeType == 1) ? newEl.tagName.toLowerCase() : '';
  	var attrs = newEl.attributes;

  	switch(el_tag)
	  {
        case "img":
      if(attrs && attrs["alt"] && attrs["alt"].value && attrs["alt"].value.match(/anchor #/))
      {
        var name=newEl.id;
        newEl=this._doc.createElement("a");
        newEl.id="anastasis_anchor_"+randomnum();
        newEl.name=name;
        newEl.style.visibility="hidden";

        var span=this._doc.createElement("span");
        span.style.display="none";
        span.appendChild(this._doc.createTextNode(name));
        newEl.appendChild(span);

      }
      break;
        case "u":     //IE
      var tmp=this._doc.createElement("span");
      tmp.style.textDecoration="underline";
      for(var i=newEl.firstChild;i;i=i.nextSibling) tmp.appendChild(i);
      newEl=tmp;
      break;
        case "strike":    //IE
      var tmp=this._doc.createElement("span");
      tmp.style.textDecoration="line-through";
      for(var i=newEl.firstChild;i;i=i.nextSibling) tmp.appendChild(i);
      newEl=tmp;
      break;
        case "span":    //Mozilla
      var newtag=null;
      if(this.getHTML(newEl,true).match(/<span style="font-weight: bold;?">/i)) newtag="strong";
      else if(this.getHTML(newEl,true).match(/<span style="font-style: italic;?">/i)) newtag="em";
      else break;
      var tmp=this._doc.createElement(newtag);
      for(var i=newEl.firstChild;i;i=i.nextSibling) tmp.appendChild(i);
      newEl=tmp;
      break;
    }


    if(attrs["align"] && attrs["align"].value && el_tag!="img" && el_tag!="table")
    {
      newEl.style.textAlign=attrs["align"].value;
      attrs["align"].value="";
    }

    parent.appendChild(newEl);

		for (var i = root.firstChild; i; i = i.nextSibling)
     {
			this.replaceElements(newEl,i);
		 }

		break;

      case 3: // Node.TEXT_NODE
    parent.appendChild(root.cloneNode(true));
		break;
//	    case 8: // Node.COMMENT_NODE
//		break;
}

}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~CONTROLLI~PAGINA~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

PEGOEditor.prototype.checkPage= function(text)
{
	var wrong_images=new Array();
	var check;
	try
	{
	  var body_clone=this._doc.createElement("div");
	  body_clone.innerHTML=text;
	  check=this.checkTree(body_clone,wrong_images);
	  check=check && this.checkHTML(text);

	  for (var i in this.plugins)
		{
		  var plugin = this.plugins[i].instance;
		  if (typeof plugin.onCheckTree == "function") check=check && plugin.onCheckTree(body_clone);
		  if (typeof plugin.onCheckHTML == "function") check=check && plugin.onCheckHTML(text);
		}

	} catch(e){alert(e);}

	if (!check)
	  {
	  if (wrong_images.length!=0)
	    {
	    w=window.open("","_blank","toolbar=no,menubar=no,personalbar=no,width=640,height=480,resizable=yes,modal=yes,dependable=no");
	    w.document.write("<p>"+this.translate("Each image must be inserted with the proper button. Please check the following images:","img")+"</p>");
	    var imm=wrong_images.pop();
	    while(imm!=null)
	    {
	      if(imm=="no_src") { w.document.write('<p>Immagine senza indirizzo</p>'); imm=wrong_images.pop(); continue; }
	      newEl=w.document.createElement("img");
	      newEl.src=imm;
	      newEl.alt=imm;
	      w.document.body.appendChild(newEl);
	      w.document.body.appendChild(w.document.createElement("br"));
	      imm=wrong_images.pop();
	    }
	    w.document.write('<input type="button" value="OK" onclick="window.close()" />');
	    w.document.close();
	    w.focus();
	    }
	  return true;
	  }
	else return true;
}

PEGOEditor.prototype.checkHTML= function(text)
{
	var editor=this;
	var trovato=false;

	var double_link=/<a.*href="\w*">.*<\/a><a.*href="\w*">.*<\/a>/;
	trovato=trovato || text.match(double_link);
	if(trovato!=null) alert(this.translate("Accessibility warnig: there are two adiacent links in the page","msg"));

	var word=editor.word_pasted;
	editor.word_pasted=false;
	for (var field in this.config._word_trash)
	{
	var reg=new RegExp(this.config._word_trash[field]);
	word=word || text.match(reg);
	}
	if (word) alert(this.translate("Warning: some text is probably pasted form Microsoft Word and can be displayed with differences or errors.\nYou should paste the text from Word using the 'Paste unformatted' button in order to erase Word HTML tags.","msg"));

	var corretto=!trovato;

	if(!this.config.stop_on_accessibility_warning) corretto=true;

	return corretto;
}


/**
 * Makes some accessibility check and cheks if every image has been inserted with an editor funcion and not pasted from the system.
 * Has to be called after replaceElements.
 * @param root HTMLElement; the root of the tree to be checked
 * @param wrong_images Array; an array that will be filled with the wrong images srcs
 */
PEGOEditor.prototype.checkTree= function(root,wrong_images)
{
	var html = "";
	var is_ok=true;

	if(root==null) return this.checkTree(this._doc.body,wrong_images);

	switch (root.nodeType)
	{
	case 1: // Node.ELEMENT_NODE
	case 11: // Node.DOCUMENT_FRAGMENT_NODE
		var i;
		var root_tag = (root.nodeType == 1) ? root.tagName.toLowerCase() : '';

		switch(root_tag)
		{
		case "img":
    		/*if( (!root.attributes.id) //non ha id
        		||                    // o ce l'ha e e' diversa da anastasis_uploaded
        		(root.attributes.id && root.attributes["id"].value.match(/anastasis_uploaded/) )// && !root.attributes["id"].value.match(/anastasis_anchor/) && !root.attributes["id"].value.match(/anastasis_editable_/) && !root.attributes["id"].value.match(/anastasis_movie_/))
        		)
				if (
          			(!root.attributes.src)  //non ha indirizzo
          			||                      //l'indirizzo non e' sulla rete (http)
          			(root.attributes.src && !root.attributes["src"].value.match(/^http:/))
          			)
          	*/if (
          			(!root.attributes.src)  //non ha indirizzo
          			||                      //l'indirizzo  e' locale (file:)
          			(root.attributes.src && root.attributes["src"].value.match(/^file:/))
          			)
        		{
        			wrong_images.push(root.attributes["src"] ? root.attributes["src"].value : "no_src");
        			is_ok=false;
        		}
			if((!root.attributes["alt"])||(root.attributes["alt"].value==null))
			{
				alert (this.translate("Accessibility warnig: image without alternative text","msg"));
				if(this.config.stop_on_accessibility_warning) is_ok=false;
			}
    		break;
		case "table":
			if((!root.attributes["summary"])||(root.attributes["summary"].value==null))
			{
				alert (this.translate('Accessibility warnig: Table without summary \nAn automatic empty summary will be added ("")',"msg"));
				root.attributes["summary"]="";
			}
    //Controllo unita' di misura
		if(       //width sbagliata
        (root.attributes["width"] && root.attributes["width"].value && !(root.attributes["width"].value.match(/em/) || root.attributes["width"].value.match(/%/)))
        ||    //height sbagliata
        (root.attributes["height"] && root.attributes["height"].value && !(root.attributes["height"].value.match(/em/) || root.attributes["height"].value.match(/%/)))
        ||
        (root.style && root.style.width && !(root.style.width.match(/em/) || root.style.width.match(/%/)) )
        ||
        (root.style && root.style.height && !(root.style.height.match(/em/) || root.style.height.match(/%/)) )
      )
      {
      alert (this.translate('Accessibility warnig: Table width and height must be specified in percentual value.',"msg"));
      if(this.config.stop_on_accessibility_warning) is_ok=false;
      }
    break;
      /*case "object":
    var attrs = root.attributes;
    if((!root.attributes["alt"])||(root.attributes["alt"].value==null))
      {
      alert (this.translate("Accessibility warnig: object without alternative text","msg"));
      if(this.config.stop_on_accessibility_warning) is_ok=false;
      }
    break;
      case "embed":
    var attrs = root.attributes;
    if((!root.attributes["alt"])||(root.attributes["alt"].value==null))
      {
      alert (this.translate("Accessibility warnig: object without alternative text","msg"));
      if(this.config.stop_on_accessibility_warning) is_ok=false;
      }
    break;*/
      case "form":
    alert (this.translate("Accessibility warnig: forms are not handled by this editor.Accessibility is not assured.","msg"));
    break;

    }
		for (i = root.firstChild; i; i = i.nextSibling) {
			is_ok &= this.checkTree(i,wrong_images);
		}
		break;
//	    case 3: // Node.TEXT_NODE
//		break;
//	    case 8: // Node.COMMENT_NODE
//		break;		// skip comments, for now.
	}

	return is_ok;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

PEGOEditor.prototype.stripBaseURL = function(string) {
	var baseurl = this.config.baseURL;

	// strip to last directory in case baseurl points to a file
	baseurl = baseurl.replace(/[^\/]+$/, '');
	var basere = new RegExp(baseurl);
	string = string.replace(basere, "");

	// strip host-part of URL which is added by MSIE to links relative to server root
	baseurl = baseurl.replace(/^(https?:\/\/[^\/]+)(.*)$/, '$1');
	basere = new RegExp(baseurl);
	return string.replace(basere, "");
};

/**
 * Returns hexadecimal color representation from a number or a rgb-style color.
 */
PEGOEditor.prototype.colorToRgb = function(v)
{
	if (!v) return '';

	// returns the hex representation of one byte (2 digits)
	function hex(d) {
		return (d < 16) ? ("0" + d.toString(16)) : d.toString(16);
	};

	if (typeof v == "number") {
		// we're talking to IE here
		var r = v & 0xFF;
		var g = (v >> 8) & 0xFF;
		var b = (v >> 16) & 0xFF;
		return "#" + hex(r) + hex(g) + hex(b);
	}

	if (v.substr(0, 3) == "rgb") {
		// in rgb(...) form -- Mozilla
		var re = /rgb\s*\(\s*([0-9]+)\s*,\s*([0-9]+)\s*,\s*([0-9]+)\s*\)/;
		if (v.match(re)) {
			var r = parseInt(RegExp.$1);
			var g = parseInt(RegExp.$2);
			var b = parseInt(RegExp.$3);
			return "#" + hex(r) + hex(g) + hex(b);
		}
		// doesn't match RE?!  maybe uses percentages or float numbers
		//TODO: not yet implemented.
		return null;
	}

	if (v.substr(0, 1) == "#") {
		// already hex rgb (hopefully :D )
		return v;
	}

	// if everything else fails ;)
	return null;
};

//Se ritorna true la cancellazione NON avviene, altrimenti si
PEGOEditor.prototype.check_del= function(root)
{

var html = "";
var result=false;
switch (root.nodeType) {
	    case 1: // Node.ELEMENT_NODE
	    case 11: // Node.DOCUMENT_FRAGMENT_NODE

    for (var i in this.plugins)
    {
      var plugin = this.plugins[i].instance;
      if (typeof plugin.onCheckDel == "function") result=result || plugin.onCheckDel(root);
    }
    if(result) { return result;}  //se gia' il plugin dice che non si puo' cancellare, stop qua

    var closed;
		var i;
		var root_tag = (root.nodeType == 1) ? root.tagName.toLowerCase() : '';

	  switch(root_tag)
	  {
        case "img":
      if(root.attributes["alt"] && root.attributes["alt"].value.match(/anchor #/))
      {
        var link=this._doc.getElementById("anchor_link_"+root.attributes["name"].value);
        if(link)
          {
            newel=this._doc.createTextNode(link.firstChild.data);
            link.parentNode.replaceChild(newel,link);
          }
      }
      break;
        case "a":
      if(root.attributes["id"] && root.attributes["id"].value.match(/anchor_link_/))
      {
        var img=this._doc.getElementById(root.attributes["href"].value.substring(1,root.attributes["href"].value.length));
        if (img) img.parentNode.removeChild(img);
      }
      break;
    }

		for (i = root.firstChild; i; i = i.nextSibling)
    {
			result=result || this.check_del(i);
		}
		break;
	}
return result;
}

PEGOEditor.prototype.ie_checkBackspace = function() {
	var sel = this.getSelection();
	var range = this.createRange(sel);
  var result=false;

  if(!range.htmlText && sel.type.toLowerCase()!="none")
  {
  var tmpEl=this._doc.createElement("<div>");
  for (i = 0; i < range.length; i++) tmpEl.appendChild(range(i).cloneNode());
  return this.check_del(tmpEl);
  }

	var rng = range.duplicate();
	if(rng.htmlText=="") rng.moveStart("character", -1);
  var tmpEl=this._doc.createElement("<div>");
  tmpEl.innerHTML=rng.htmlText;
  result=result || this.check_del(tmpEl);

	var r2 = range.duplicate();
	r2.moveStart("character", -1);
	var a = r2.parentElement();
	if (a != range.parentElement() && /^a$/i.test(a.tagName))
  {
		r2.collapse(true);
		r2.moveEnd("character", 1);
		r2.pasteHTML('');
		r2.select();
		return true;
	}

	return result;
};

PEGOEditor.prototype.dom_checkBackspace = function() {
	var self = this;
  var sel = self.getSelection();
	var range = self.createRange(sel);

	var result=false;
  //var rng = range.cloneRange();
  //rng.contents=range.cloneContents();
  if(!range.collapsed)
  {
    var contents=range.cloneContents();
    result=result || self.check_del(contents);
  }
  /*
  range.extractContents();

  if(rng.collapsed)
  {
    rng.selectNode(self._doc.body);
		var i=0;
		while(rng.compareBoundaryPoints(Range.START_TO_START,range)==-1)
		{
      rng.setStart(self._doc.body,++i);
    }
    alert(i);
    rng.setEnd(self._doc.body,++i);
  }

		//rng.setStartBefore(SC);
		//rng.setEndAfter(EC);
    //alert(SO);
    //if(rng.collapsed) { rng.setEnd(SC,EO); rng.setStart(SC,SO); }*/


	setTimeout(function() {
		var sel = self.getSelection();

		var range = self.createRange(sel);
		var SC = range.startContainer;
		var SO = range.startOffset;
		var EC = range.endContainer;
		var EO = range.endOffset;
		var newr = SC.nextSibling;
		if (SC.nodeType == 3) SC = SC.parentNode;

  	if (!/\S/.test(SC.tagName)) {
			var p = document.createElement("p");
			while (SC.firstChild) p.appendChild(SC.firstChild);
			SC.parentNode.insertBefore(p, SC);
			SC.parentNode.removeChild(SC);
			var r = range.cloneRange();
			r.setStartBefore(newr);
			r.setEndAfter(newr);
			r.extractContents();
			sel.removeAllRanges();
			sel.addRange(r);
		}
	}, 10);
	return result;
};



/**
 * Se assenti, aggiunge tutta una serie di parametri di default
 */
PEGOEditor.prototype.popupDialog = function(url, action, outparam)
{
	if(!outparam) outparam=new Object();
	if(!outparam["I18N"]) outparam["I18N"] =this.I18NCollection["dialogs"];

	return anastasis.openDialog(this.popupURL(url),action, outparam);
};

PEGOEditor.prototype.imgURL = function(file, plugin)
{
	if (typeof plugin == "undefined")
		return _editor_url + this.config.imgURL + file;
	else
		return _editor_url + "plugins/" + plugin + "/img/" + file;
};

PEGOEditor.prototype.popupURL = function(file)
{
	var url = "";
	if (file.match(/^plugin:\/\/(.*?)\/(.*)/))
	{
		var plugin = RegExp.$1;
		var popup = RegExp.$2;
		if (!/\.html$/.test(popup)) popup += ".html";
		url = _editor_url + "plugins/" + plugin + "/popups/" + popup;
	}
  	else
  		if(file.match(/^\//)) url=file;
		else url = _editor_url + this.config.popupURL + file;

	return url;
};

PEGOEditor.translate=function(msg,i18n)
{
  if(i18n) return (i18n[msg]) ? i18n[msg] : msg;
  else return (PEGOEditor.I18N.msg[msg]) ? PEGOEditor.I18N.msg[msg] : msg;
}

/**
 * Translation function
 * @param term the term (or phrase) to translate
 * @param field the field where to search for the term; if it is not specifed, the default field is 'msg'
 */
PEGOEditor.prototype.translate=function(term,field)
{
	if(!field) field="msg";
	if(!this.I18NCollection[field]) return term;

	return this.I18NCollection[field].translate(term);
}


/**
 *  Performs HTML encoding of the given string (replace only the 'critical' symbols)
 */
PEGOEditor.prototype.htmlEncode = function(str) {
	str = str.replace(/&/ig, "&amp;");
	str = str.replace(/</ig, "&lt;");
	str = str.replace(/>/ig, "&gt;");
	str = str.replace(/\x22/ig, "&quot;");
  	//Sostituzione spazi: fa troppo casino!
	//	str = str.replace(/\s\s/g, " &nbsp;");
	//	str = str.replace(/&nbsp;\s/g, "&nbsp;&nbsp;");

	return str;
};

/**
 * Used inside getHTML
 */
PEGOEditor.prototype.needsClosingTag = function(el) {
	var closingTags = " head script style div span tr td tbody table em strong font a title textarea button form fieldset legend label select option optgroup object";
	return (closingTags.indexOf(" " + el.tagName.toLowerCase() + " ") != -1);
};

/**
 * Returns the HTML text of the given node with all its children nodes.
 * @param root HTMLElement; the node
 * @param outputRoot boolean; whether or not to output the passed node (if not, it outputs only its children nodes)
 * @param formatted boolean; whether or not to format the HTML code; formatted code should only be used when showing to the user, not when saving.
 * @param level integer; used for putting tabs in front of tags; should be 0 if outputRoot=true, -1 otherwise
 */
PEGOEditor.prototype.getHTML = function(root, outputRoot,formatted,level)
{
	try {
	var html = "";
	switch (root.nodeType) {
	case 1: // Node.ELEMENT_NODE
	case 11: // Node.DOCUMENT_FRAGMENT_NODE
		var closed;
		var i;
		var root_tag = (root.nodeType == 1) ? root.tagName.toLowerCase() : '';
		if (PEGOEditor.is_ie && root_tag == "head") {	//TODO: eliminare!!?
			if (outputRoot)
				html += "\n<head>";
			// lowercasize
			var save_multiline = RegExp.multiline;
			RegExp.multiline = true;
			var RE_tagName = /(<\/|<)\s*([^ \t\n>]+)/ig;
			var txt = root.innerHTML.replace(RE_tagName, function(str, p1, p2) {
				return p1 + p2.toLowerCase();
			});
			RegExp.multiline = save_multiline;
			html += txt;
			if (outputRoot)
				html += "\n</head>";
			break;
		} else

		if (outputRoot)
		{
			closed = (!(root.hasChildNodes() || this.needsClosingTag(root)));
			html="";
			if(formatted) for(var i=0;i<level;i++) html+="\t";
			html += "<" + root.tagName.toLowerCase();
			var attrs = root.attributes;
			for (i = 0; i < attrs.length; ++i)
			{
				var a = attrs.item(i);
				//Per qualche motivo in IE la proprieta' specified di value e type e coords e name(quest'ulitmo SOLO nei tag a) e' sempre a false!
				if(PEGOEditor.is_ie) { if (!a.specified && a.nodeName!="value" && a.nodeName!="type" && a.nodeName!="coords" && a.nodeName!="name") continue;}
				else if (!a.specified) continue;

				var name = a.nodeName.toLowerCase();

				// avoid certain attributes: mozilla and explorer custom
				if (/_moz|contenteditable|_msh/.test(name))
					continue;

				var value;
				//event funcionts...
				if(name.match(/^on/))
				{
					value=a.nodeValue;
				}
				else if (name != "style")
				{
					if (typeof root[a.nodeName] != "undefined" && name != "href" && name != "src")
					{
						if(root_tag=="li" && name=="value") continue; //IE da' un value=0 a tutti i li, che fa casino in Mozilla, ergo lo elimino!
						else value = root[a.nodeName];
					}
					else
					{
						value = a.nodeValue;
						// IE seems not willing to return the original values - it converts to absolute
						// links using a.nodeValue, a.value, a.stringValue, root.getAttribute("href")
						// So we have to strip the baseurl manually -/
						if (name == "href" || name=="src")
						{
							if (PEGOEditor.is_ie)
							{
								value = this.stripBaseURL(value);
								value=value.replace(/^about:blank/,"");	//links
								value=value.replace(/\/$/,"");			//links
								value=value.replace(/^\/#/,"#");		//anchors
							}

							//Firefox replaces asbolute paths with relative paths when resources are moved with drag&drop: we must recover the absolute path
							if(value.match(/^\.\.\//))
							{
								var indietri=(value.match(/^([\.\.\/]*)/)[1].length)/3;
								var path="/"+value.match(/^[\.\.\/]*(.*)/)[1]
								var dove=window.location.href.match(/http:\/\/[\w\.\d-]*:?\d*\/(.*)/)[1];
								var pezzi=dove.split("/");
								var lunghezza_path=pezzi.length-1;
								for(var kappa=0;kappa<lunghezza_path-indietri;kappa++)
								{
									path="/"+pezzi[kappa]+path;
								}
								value=path;
							}
							//If the Firefox reltive path does not begin with ../ (nor /) but is relative to the current drectory (empty, just the file or dir name)...
							//...except when such an url is inserted by the user: namely when it begins with: ?, http, file, mailto, #, javascript, @
							//else if(!value.match(/^\?/) && !value.match(/^http/) && !value.match(/^file/) && !value.match(/^mailto/) && !value.match(/^\//) && !value.match(/^#/) && !value.match(/^javascript:/))
							else
							{
								var toReplace=true;
                      			toReplace=toReplace && !value.match(/^[[\/]|[\?]|http:|file:|mailto:|#|javascript:|@|]/);
								if(toReplace && this.system_params.attachment_prefixes)
								{
									for(var h in this.system_params.attachment_prefixes)
									{
										if(value.match("^"+this.system_params.attachment_prefixes[h]))
										toReplace=false;
									}
								}
								if(toReplace) try
								{
									var dove=window.location.href.match(/https?:\/\/[\w\.\d-]*:?\d*\/(.*)\/.*/)[1];
									value="/"+dove+"/"+value;
								} catch(e){}
							}
						}
					}
				}
				else
				{
					// IE fails to put style in attributes list
					// TODO: cssText reported by IE is UPPERCASE
					value = root.style.cssText;
				}
				// Mozilla reports some special tags here; we don't need them.

				if (/(_moz|^$)/.test(value)) { continue;}

				//Replace & with &amp;
				if(value && value.replace)
				{
					value=value.replace(/&amp;/g,"&");//Altrimenti si moltiplicano!
					value=value.replace(/&/g,"&amp;");
				}

				html += " " + name + '="' + value + '"';
			}
			html += closed ? " />" : ">";
			if(formatted) html+="\n";

			//object is a special case...
			if(PEGOEditor.is_ie && root_tag=="object") html+=root.innerHTML;
		}

		for (i = root.firstChild; i; i = i.nextSibling)
		{
			html += this.getHTML(i, true,formatted,level+1);
		}

		if (outputRoot && !closed)
		{
			if(formatted) for(var i=0;i<level;i++) html+="\t";
			html += "</" + root.tagName.toLowerCase() + ">";
			if(formatted) html+="\n";
		}
		break;

	case 3: // Node.TEXT_NODE
		html="";
		if(!PEGOEditor.is_ie) //Reaplce #10 spaces (?), ignored by Firefox
		{
			var reg=new RegExp('('+String.fromCharCode(10)+')');
			reg.global=true;
			while(root.data.match(reg)) { root.data=root.data.replace(reg,' ');}
		}

		if(formatted) for(var i=0;i<level;i++) html+="\t";
		html+=root.data;
		if(formatted) html+="\n";

		if(root.parentNode.tagName && root.parentNode.tagName.toLowerCase()!="script") html = this.htmlEncode(html);
		break;

	case 8: // Node.COMMENT_NODE
		// TODO: when coming back in wysiwyg mode it cuts out comments...
		html = "<!--" + root.data + "-->";
		break;
	}
	return html;

	}catch(e)
	{
		alert(this.translate("Warning: there is an error in some HTML code, that must have been pasted or inserted by hand"));
		return "[HTML ERROR]";
	}
};


//----------------------UTILITIES------------------------------------------------------//
//All the following utilities functions are wrappers for functions from the Anstasis framework.


//Invio sincrono o asincrono di dati via XMLHttp
//Parametri:
// - array param: param[id]=contenuto, dove id � l'id che avr� nella queryString
// - receiver: l'indirizzo a cui mandarlo
// - async: asincrono?
//Restituisce l'output della pagina receiver sotto forma di testo (se la richiesta � sincrona)

PEGOEditor.prototype.ajaxSend = function(param,receiver,async)
{
  return anastasis.ajax.send(param,receiver,"POST",false,async,null);
}

/**
 * Registers the function as handler for the event evname on the element el
 * If this function is called more than one time for the same element and the same event,
 * the function are executed one after the other in the registering order.
 * @param el the element
 * @param evname the name of the event (without the "on" prefix)
 * @param func the function
 */
PEGOEditor.prototype.addEvent = function(el, evname, func)
{
	anastasis.utils.addEvent(el,evname,func);
};

/**
 * Adds the same function on a list of events on the same element
 * @param el the element
 * @param evs an array with the names of the events (without the "on" prefix)
 * @param func the function
 *
 */
PEGOEditor.prototype.addEvents = function(el, evs, func)
{
	for (var i in evs)
	{
		this.addEvent(el, evs[i], func);
	}
};

/**
 * Removes a specific function as handler for the event evname on the element el
 * @param el the element
 * @param evname the name of the event (without the "on" prefix)
 * @param func the function (must be the same function added, no anonimous functions allowed)
 */
PEGOEditor.prototype.removeEvent = function(el, evname, func)
{
	anastasis.utils.removeEvent(el,evname,func);
};

/**
 * Stop the propagation of the event ev
 */
PEGOEditor.prototype.stopEvent = function(ev)
{
	anastasis.utils.stopEvent(ev);
};

/**
 * Adds the class className to the element el
 */
PEGOEditor.prototype.removeClass = function(el, className)
{
	anastasis.utils.removeClass(el,className);
};

/**
 * Removes the class className from the element el
 */
PEGOEditor.prototype.addClass = function(el, className)
{
	anastasis.utils.addClass(el,className);
};

PEGOEditor.prototype.getTranslator=function(strings)
{
	return new Anastasis.I18N(strings);
}

// EOF
