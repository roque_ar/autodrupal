PEGOEditor.CSSHandler.I18N=
{
	lang: "it",
	
	tooltips:
	{
		newblock: "Nuovo blocco",
		hiblocks: "Evidenzia blocchi"
	},
	dialogs:
	{
		"Insert/Edit Block Properties"     : "Inserisci/Modifica proprieta' del blocco",
	    "Class"                     : "Classe",
	    "none"                      : "Nessuna",
	    "Style"                     : "Stili",
	    "Text color"                : "Colore del testo",
	    "Text alignment"            : "Allineamento del testo",
	    "left"                      : "Sinistra",
	    "right"                     : "Destra",
	    "center"                    : "Centrato",
	    "justify"                   : "Giustificato",
	    "Remove"					: "Rimuovi",
	    "Delete block"             	: "Elimina blocco",
	    "Select block content"      : "Seleziona il blocco",
	    "Delete block keeps intact the block contents"  : "NB: 'Elimina blocco' mantiene intatto il contenuto del blocco"
	},
	msg:
	{
		"It is not allowed to remove this element"                : "La rimozione di questo elemento non e' permessa.",
		"You must select a portion of text"                       : "Devi selezionare una porzione di testo!"
	}
}