//Copyright (c) 2004-2008 Anastasis Societa' Cooperativa, www.anastasis.it

PEGOEditor.CSSHandler=function (editor) 
{
	var me=this;
	this.name="CSSHandler";
	
	this.editor = editor;	  	

	this.buttons = 
	{
		hiblocks: [ "hiblocks", editor.imgURL("hiblocks.gif", this.name), false, function(editor,id) { me.buttonPress(editor,id); }],
		newblock: [ "newblock", editor.imgURL("newblock.gif", this.name), false, function(editor,id) { me.buttonPress(editor,id); }]
	}

	this.buttonList=["separator","hiblocks","newblock"];
	this.I18N=PEGOEditor.CSSHandler.I18N;			
};

PEGOEditor.CSSHandler.prototype=new PEGOEditor.Plugin();

PEGOEditor.CSSHandler.prototype.onInit=function()
{
	this.parseCount=1;
	this.cssArray = new Array();
	this.groups=new Object();
	this.highlighted=false;
	this._blockElements=["p","div","span","sup","sub","strong","em","h1","h2","h3","h4","h5","h6"];
  
	var me=this;  
  	//Lo fa dopo un po', per sicurezza
  	setTimeout(function(){ me.parseStyleSheet(me.editor); }, 2000);
}

//--------------------------------------------------------//
      
PEGOEditor.CSSHandler.prototype.buttonPress = function(editor, button_id) 
{
  	editor.focusEditor();
  	try
  	{
	  	switch (button_id) 
	  	{	
	  			case "hiblocks": 
	  		this.highlight(); break;
      
      			case "newblock": 
      		this.newBlock(); break;
	  		  
	  	    	default:
	  		alert("Button [" + button_id + "] not yet implemented");
	  	}
  	} catch(e) {alert("JavaScript Error: execCommand("+button_id+")\n"+e);}
}

PEGOEditor.CSSHandler.prototype.onUpdateToolbar=function(ancestors)
{
	if(!this.editor.config.statusBar) return;
	
	var me=this;
	var len=this.editor.statusBar.linkArray.length;
	for(var i=0;i<len;i++)
	{
		var a=this.editor.statusBar.linkArray[i].firstChild;
		if(a && a.tagName)
		{
			//N.B: this si riferisce all'elemento link stesso!
			a.onclick = function() {
		          this.blur();
		          me.editBlock(this.el);          
		          return false;
		        };
		}
	}	
}
//-----------------------------------------------------------------------------//

PEGOEditor.CSSHandler.prototype.newBlock = function(el)
{
  var me=this;
  var editor = this.editor;	// for nested functions
  var outparam = null;
  var text="";
  
  outparam =
  {
    tag         : "div",
    freestyle	: editor.config.freestyle,
    classList   : this.createFullList(),
    css         : editor.system_params.public_css,
    create    : true       
  }
  
  var action = function(param)
  {
    if(!param) return;
    
    var tag=param["tag"];
    if(!tag) tag="div";
    
    var sel = editor.getSelection();
  	var range = editor.createRange(sel);
    var newel=editor._doc.createElement(tag);
    newel.className=param["className"];
    
    var ok=true;   
       
    if(PEGOEditor.is_ie)
    {
      if(range.htmlText)
        newel.innerHTML=range.htmlText;
       else
        ok=false;      
    }
    else
    {
      if(!range.collapsed)
        newel.appendChild(range.cloneContents());
      else
        ok=false;	        
    }
    
    if(ok)
    {
      if(PEGOEditor.is_ie) range.pasteHTML(newel.outerHTML);
      else editor.insertNodeAtSelection(newel);           
    }
    else alert(me.translate("You must select a portion of text","msg"));
    
    editor.updateToolbar(true);
  }
  
  editor.popupDialog("plugin://CSSHandler/edit_block.html",action, outparam);
  
}

PEGOEditor.CSSHandler.prototype.editBlock = function(el)
{
  var me=this;
  var editor = this.editor;	// for nested functions
  var outparam = null;
  var text="";
  
  outparam =
  {
    tag         : el.tagName,
    textAlign   : el.style.textAlign,
    color       : editor.colorToRgb(el.style.color),
    backgroundColor: editor.colorToRgb(el.style.backgroundColor),
    className   : el.className,
    classList   : this.createList(el.tagName.toLowerCase()),
    css         : editor.system_params.public_css,
    freestyle   : editor.config.freestyle,
    create    : false
  }
  
  var action = function(param)
  {
    if(!param) return;
    
    
    switch (param["action"])
    {            
        case "set":
      el.className=param["className"];
      el.style.textAlign=param["textAlign"];
      el.style.color=param["color"];
      el.style.backgroundColor=param["backgroundColor"];
      break;
        
        case "select":
      editor.selectNodeContents(el);
      break;
      
        case "delete":  
      if(el.tagName.toLowerCase()=="table" || el.tagName.toLowerCase()=="tr" || el.tagName.toLowerCase()=="td") 
      {
      	alert(me.translate("It is not allowed to remove this element","msg")); 
      	break; 
      }          
      //Al posto del nodo ci mette il suo primo figlio e, a seguire, tutti gli altri 
      var parent=el.parentNode;
      var son=el.firstChild;
      var uncle=el.nextSibling;      
      parent.replaceChild(son,el);
      while(el.firstChild) parent.insertBefore(el.firstChild,uncle);         
      break;            
    }         
    editor.updateToolbar(true);
  }
  
  editor.popupDialog("plugin://CSSHandler/edit_block.html",action, outparam);  
}

//----------------------------------------------------------------------------//

PEGOEditor.CSSHandler.prototype.parseStyleSheet=function(editor)
{
        var iframe = editor._iframe.contentWindow.document;
        
        var cssArray=this.cssArray;
         
        if(editor.system_params.private_css) i=editor.system_params.private_css.length;
        
        var non_pubblico=true;                
        
        for(var i=0;i<iframe.styleSheets.length;i++)
        {             
	        //Cerca se l'indirizzo del css e' tra quelli pubblici, altrimenti salta
	        non_pubblico=true;    
	        for(var h in editor.system_params.public_css) 
	          if(iframe.styleSheets[i].href && iframe.styleSheets[i].href.match(editor.system_params.public_css[h])) non_pubblico=false;
	        if(non_pubblico) continue;
	        
	        try
	        {
	        	if(!PEGOEditor.is_ie)
	           		cssArray=this.collectCSSRule(iframe.styleSheets[i].cssRules,cssArray); 
	        	else if(iframe.styleSheets[0].rules) 
	              		cssArray=this.collectCSSRule(iframe.styleSheets[i].rules,cssArray);
	              
	        } catch(e){ }
        }
        // new attempt of rescan stylesheets some seconds (e.g. for external css-files with longer initialisation)
        var me=this;
        if(this.parseCount<4)
        {
            setTimeout(function () 
		            	{
			                me.parseStyleSheet(editor);
		            	},this.parseCount*1000);
            
            this.parseCount=this.parseCount*2; 
        }                 
}

PEGOEditor.CSSHandler.prototype.collectCSSRule=function(cssRules,cssArray)
{
    for(var rule in cssRules){
        // StyleRule
        if(cssRules[rule].selectorText){
            if(cssRules[rule].selectorText.search(/:+/)==-1){
                
                // split equal Styles (Mozilla-specific) e.q. head, body {border:0px}
                // for ie not relevant. returns always one element
                var cssElements = cssRules[rule].selectorText.split(",");
                for(var k=0;k<cssElements.length;k++){                
                    var cssElement = cssElements[k].split(".");
                    
                    var tagName=cssElement[0].toLowerCase().trim();
                    var className=cssElement[1]; 
                    var cssName="";
                                                            
                    if(!tagName) tagName='all';
                    if(!cssArray[tagName]) cssArray[tagName]=new Array();
                    
                    if(className){
                        if(tagName=='all') cssName=className;
                        else cssName=className;
                    }
                    else{
                        className='none';
                        if(tagName=='all') cssName=this.translate("Default","msg");
                        else cssName=this.translate("Default","msg");
                    }
                    cssArray[tagName][className]=cssName;
                }
            }
        }
        // ImportRule (Mozilla)
        else if(cssRules[rule].styleSheet){
            cssArray=this.collectCSSRule(cssRules[rule].styleSheet.cssRules,cssArray);
        }
    }

    return cssArray;
}


    //----------------------------------------------//
    
PEGOEditor.CSSHandler.prototype.createList = function(tag)
{
  var cssArray=this.cssArray;
  var list = new Array(); 
  
  //1) ci aggiungiamo tutte le classi che valgono per tutti
  for(var i in cssArray['all']) list.push(cssArray['all'][i]);
  if(cssArray[tag]) 
  {
    list.push("-"+this.translate(tag,"statusbar_tags").toUpperCase()+"-");
    for(var i in cssArray[tag]) list.push(cssArray[tag][i]);
  }
  
  return list;        
}

PEGOEditor.CSSHandler.prototype.createFullList = function()
{
  var cssArray=this.cssArray;
  var list = new Array(); 
  
  //1) ci aggiungiamo tutte le classi che valgono per tutti
  for(var i in cssArray['all']) list.push(cssArray['all'][i]);
    
  for (var k=0;k<this._blockElements.length;k++)
  { 
    var tag = this._blockElements[k];
    if(cssArray[tag]) 
    {
      list.push("#"+tag);
      list.push("-"+this.translate(tag,"statusbar_tags").toUpperCase()+"-");
      for(var i in cssArray[tag]) list.push(cssArray[tag][i]);
    }
  }
  return list;        
}

/*
 * Used as action for a dropdown class selection...now useless
PEGOEditor.CSSHandler.prototype.applyClass = function(editor, obj) 
{
    var tbobj = editor._toolbarObjects[obj.id];
    var index = tbobj.element.selectedIndex;
    var className = tbobj.element.value;
     
    if(className=="tag" || className=="-") return;
        
    var parent = editor.getParentElement();
    
    className=className.split(" ");
    var tag=className[0];
    className=className[1];
    
    if(tag!="all")
    {
      while(parent.tagName.toLowerCase()!=tag) {if(parent.tagName.toLowerCase()=="body" || parent.tagName.toLowerCase()=="table") return; parent=parent.parentNode;  }
    }

    if(className!='none')
    {
      if(parent==editor._doc.body) 
      {
        var span = editor._doc.createElement("p");
        span.className=className;
        span.innerHTML=parent.innerHTML;
        parent.innerHTML="";
        parent.appendChild(span);
       }
       else parent.className=className;
    }
    else
    {
       // if(PEGOEditor.is_gecko) parent.removeAttribute('class');
       // else parent.removeAttribute('className');
       parent.className="";
    }
    editor.updateToolbar();
};
*/
    //----------------------------------------------//

PEGOEditor.CSSHandler.prototype.highlight = function()
{
  var editor = this.editor;
  
  //Turn off
  if(this.highlighted)
  {
    for (var i=0;i<this._blockElements.length;i++)
    {
      this.deleteRule(this._blockElements[i],"border: 1px dotted red; padding: 2px;",i)
    }
  if(this.lastElement) 
      { 
        this.lastElement.style.backgroundColor=this.lastElementBackground;
        //if(this.label) { this.lastElement.removeChild(this.label); this.label=null;}
        if  (this.bigLabel)
        {
          editor._doc.body.removeChild(this.bigLabel);
          this.bigLabel=null;
        } 
      }   
  this.highlighted=false;
  }
  //turn on
  else
  {   
  try
  { 
    for (var i=0;i<this._blockElements.length;i++)
    {
      this.addRule(this._blockElements[i],"border: 1px dotted red; padding: 2px;",i)
    }
    this.highlighted=true;
    } catch(e){alert(e)}
  }
}

PEGOEditor.CSSHandler.prototype.highlightOff = function()
{
  if(this.highlighted) this.highlight();
}

PEGOEditor.CSSHandler.prototype.addRule = function (selector,rules,index)
{ 
 if(PEGOEditor.is_ie)
  this.editor._doc.styleSheets[0].addRule(selector,rules,index);
 else
  this.editor._doc.styleSheets[0].insertRule(selector+" {"+rules+"}",null);
}

PEGOEditor.CSSHandler.prototype.deleteRule = function (selector,rules,index)
{
  try{
  if(PEGOEditor.is_ie)
 {
  this.editor._doc.styleSheets[0].removeRule();
 }
 else
 {
  this.editor._doc.styleSheets[0].deleteRule(selector+" {"+rules+"}",null);
 }  
  } catch(e) {alert(e)}
}
