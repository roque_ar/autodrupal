//Copyright (c) 2004-2008 Anastasis Societa' Cooperativa, Bologna, www.anastasis.it
//   
//  TableOperations was originally based on work by Mihai Bazon which is:
//	Copyright (c) 2003-2004 dynarch.com.
//	Copyright (c) 2002-2003 interactivetools.com, inc.
//	This copyright notice MUST stay intact for use.

PEGOEditor.TableOperations=function (editor) 
{
	this.name="TableOperations";
	
	this.editor = editor;
	this.I18N=PEGOEditor.TableOperations.I18N;	  
		
	this.buttonList= [
	["linebreak|tables",this.I18N["dialogs"]["Tables"]],
	// table properties button and deletion button
	["table-delete",     "table"],
	["table-prop",       "table"],
	null,			// separator

	// ROWS
	["row-insert-above", "tr"],
	["row-insert-under", "tr"],
	["row-delete",       "tr"],
	["row-split",        "td[rowSpan!=1]"],
	null,

	// COLS
	["col-insert-before", "td"],
	["col-insert-after",  "td"],
	["col-delete",        "td"],
	["col-split",         "td[colSpan!=1]"],
	null,

	// CELLS
	["cell-prop",          "tr"],
	["cell-insert-before", "td"],
	["cell-insert-after",  "td"],
	["cell-delete",        "td"],
	["cell-merge",         "tr"],
	["cell-split",         "td"]
	];
		
};

PEGOEditor.TableOperations.prototype=new PEGOEditor.Plugin();

//-------------------------------------------------------------//

PEGOEditor.TableOperations.prototype.onUpdateToolbar = function(ancestors)
{
  var nonce=true;
  for (var k in ancestors) 
      {
				if (!ancestors[k])  continue;// the impossible really happens.
				if (ancestors[k].tagName.toLowerCase() == "table") 
        {
					nonce=false;
					document.getElementById("tables").style.display="";
				}
			}
	if(nonce)document.getElementById("tables").style.display="none";
}

/************************
 * UTILITIES
 ************************/

// retrieves the closest element having the specified tagName in the list of
// ancestors of the current selection/caret.
PEGOEditor.TableOperations.prototype.getClosest = function(tagName) {
	var editor = this.editor;
	var ancestors = editor.getAllAncestors();
	var ret = null;
	tagName = ("" + tagName).toLowerCase();
	for (var i in ancestors) {
		var el = ancestors[i];		
		if (el.tagName.toLowerCase() == tagName) {
			ret = el;
			break;
		}
	}
	return ret;
};

PEGOEditor.TableOperations.prototype.dialogTableProperties = function() 
{
	var me=this;	
	// retrieve existing values
	var table = this.getClosest("table");
	var editor = this.editor;
	
	var action=function(params) {
		
		me.processStyle(params, table);	
		for (var i in params) {
			var val = params[i];
			switch (i) {
					case "f_summary":
				table.summary = val;
				break;
					case "f_width":			  
				if(val) table.style.width = ("" + val) + "%";
				else table.style.width="";
				break;
				  case "f_height":
				try {table.style.height = ("" + val) + "%"; if(!val) table.style.height=""; } catch(e){}     
				break;
					case "f_align":    //Per IE: si setta lo stile del DIV che contiene la tabella; in Mozilla si agir� sullo stile				
        if(table.parentNode.tagName.toLowerCase()=="div") table.parentNode.style.textAlign = val;
				else 
				{
          var div=editor._doc.createElement("div");
          table.parentNode.replaceChild(div,table);
          div.style.textAlign = val;
          div.appendChild(table);
        }
				break;
					case "f_spacing":
				table.cellSpacing = val;
				break;
					case "f_padding":
				table.cellPadding = val;
				break;
					case "f_frames":
				table.frame = val;
				break;
					case "f_rules":
				table.rules = val;				
				break;
			}
		}	
		editor.focusEditor();
		editor.updateToolbar();
		var save_collapse = table.style.borderCollapse;
		table.style.borderCollapse = "collapse";
		table.style.borderCollapse = "separate";
		table.style.borderCollapse = save_collapse;	
	};

  
  var width ="";
  var height = "";
  var borderColor="";
  var borderWidth="";
    
  if(table.style)
  {
	width = parseInt(table.style.width);
	if (isNaN(width)) width = "";
	height = parseInt(table.style.height);
	if(isNaN(height)) height = "";
	
	borderColor=table.style.borderColor;     //solito baco idiota di Mozilla, lo scrive 4 volte!
	if(!anastasis.is_ie){ borderColor=borderColor.split(") ")[0]; borderColor+=")";}
	
	borderWidth=table.style.borderWidth;     //solito baco idiota di Mozilla, lo scrive 4 volte!
	if(!anastasis.is_ie){ borderWidth=borderWidth.split(" ")[0];}
	if(borderWidth && borderWidth.match(/(\d+)px/)) borderWidth=borderWidth.match(/(\d+)px/)[1];
  }
	
	
	//alert(table.border+" "+table.frame);
	
	outparam = {
		f_summary           :   table.summary,
		f_width             :   width,
		f_height            :   height,
		f_align             :   table.parentNode.style ? table.parentNode.style.textAlign : table.align,
		f_textAlign         :   table.style ? table.style.textAlign : "",
		f_spacing           :   table.cellSpacing,
		f_padding           :   table.cellPadding,
		f_borders           :   borderWidth,
		f_frames            :   table.frame,
		f_rules             :   table.rules.trim(),
		f_st_backgroundColor:   table.style ? table.style.backgroundColor : "",
   		back_image_url      :   table.style ? table.style.backgroundImage : "",
    	f_st_borderColor    :   borderColor,
    	f_st_borderStyle    :   table.style ? table.style.borderStyle.split(" ")[0].trim() : "",
    	f_st_borderCollapse :   table.style ? table.style.borderCollapse : "",
		color_url			 : editor.popupURL("select_color.html"),
		scrollbars          : "yes"
	};
	
	editor.popupDialog("plugin://TableOperations/table_properties.html", action, outparam);
	
};

// this function requires the file PopupDiv/PopupWin to be loaded from browser
PEGOEditor.TableOperations.prototype.dialogCellProperties = function() 
{
	var me=this;
	var editor=this.editor;
	// retrieve existing values
	var element = null;
	var elementType=null;
  
  
  //Cerca il primo td o th verso l'esterno
  var ancestors = editor.getAllAncestors();
	for (var i = 0; i<ancestors.length;i++) 
  {
	element = ancestors[i];
	if(element.tagName && (element.tagName.toLowerCase()=="td" || element.tagName.toLowerCase()=="th")) break;
	}
  
  elementType=element.tagName.toLowerCase();
  
	var table = this.getClosest("table");
	// this.editor.selectNodeContents(element);
	// this.editor.updateToolbar();

  var action=function(params) 
  { 
  	me.processStyle(params, element);	
  	if(params["cell_type"]=="th" && params["scope"]) element.scope=params["scope"];
    if(elementType!=params["cell_type"])
		{
      var newEl=editor._doc.createElement(params["cell_type"]);
      for(var i=element.firstChild;i;i=i.nextSibling) newEl.appendChild(i);
      if(!anastasis.is_ie) //IE non puo' conservare gli attributi vecchi, altrimenti se ne inventa un sacco e poi da' errore! IE MERDA!!!!
      {
        var attrs=element.attributes;
        for(var i=0;i<attrs.length;i++) try {newEl.attributes[attrs[i].nodeName]=attrs[i].nodeValue;} catch(e){}
        attrs=element.style;
        for(var i in attrs) try {newEl.style[i]=attrs[i];} catch(e){} 
      }
      if(params["cell_type"]=="th" && params["scope"]) newEl.scope=params["scope"];
      element.parentNode.replaceChild(newEl,element);
      element=newEl;
    }

		editor.focusEditor();
		editor.updateToolbar();
		var save_collapse = table.style.borderCollapse;
		table.style.borderCollapse = "collapse";
		table.style.borderCollapse = "separate";
		table.style.borderCollapse = save_collapse;
	}
  
  var width = parseInt(element.style.width);
	isNaN(width) && (width = "");
	var height = parseInt(element.style.height);
	isNaN(height) && (height = "");
	
	var borderColor=element.style.borderColor;     //solito baco idiota di Mozilla, lo scrive 4 volte!
	if(!anastasis.is_ie){ borderColor=borderColor.split(") ")[0]; borderColor+=")";}
	
	outparam = 
	{
		f_width             :   width,
		f_height            :   height,
		f_textAlign         :   element.style.textAlign,
		f_st_verticalAlign  :   element.style.verticalAlign.toLowerCase(),
		f_st_backgroundColor:   element.style.backgroundColor,
		f_top_border_style  :   element.style.borderTopStyle,
		f_bottom_border_style   :    element.style.borderBottomStyle,
		f_right_border_style    :   element.style.borderRightStyle,
		f_left_border_style     :   element.style.borderLeftStyle,
    	f_top_border_width      :  element.style.borderTopWidth ? element.style.borderTopWidth.match(/(\d*)/)[0] : "-",
    	f_bottom_border_width   :  element.style.borderBottomWidth ? element.style.borderBottomWidth.match(/(\d*)/)[0] : "-",
    	f_right_border_width    :  element.style.borderRightWidth ? element.style.borderRightWidth.match(/(\d*)/)[0] : "-",
    	f_left_border_width     :  element.style.borderLeftWidth ? element.style.borderLeftWidth.match(/(\d*)/)[0] : "-",
    	back_image_url      :  element.style.backgroundImage,
    	f_st_borderColor    : borderColor,
    	cell_type           : elementType,
    	scope               : element.scope,          
		color_url			 : editor.popupURL("select_color.html"),
		scrollbars          : "yes"
	};
	
	editor.popupDialog("plugin://TableOperations/cell_and_line_properties.html", action, outparam);		
};

// this function gets called when some button from the TableOperations toolbar
// was pressed.
PEGOEditor.TableOperations.prototype.buttonPress = function(editor, button_id) {
	this.editor = editor;
	editor.focusEditor();
	var mozbr = anastasis.is_gecko ? "<br />" : "";

	// helper function that clears the content in a table row
	function clearRow(tr) {
		var tds = tr.getElementsByTagName("td");
		for (var i = tds.length; --i >= 0;) {
			var td = tds[i];
			td.rowSpan = 1;
			td.innerHTML = mozbr;
		}
	};

	function splitRow(td) {
		var n = parseInt("" + td.rowSpan);
		var nc = parseInt("" + td.colSpan);
		td.rowSpan = 1;
		tr = td.parentNode;
		var itr = tr.rowIndex;
		var trs = tr.parentNode.rows;
		var index = td.cellIndex;
		while (--n > 0) {
			tr = trs[++itr];
			var otd = editor._doc.createElement("td");
			otd.colSpan = td.colSpan;
			otd.innerHTML = mozbr;
			tr.insertBefore(otd, tr.cells[index]);
		}
		editor.updateToolbar();
	};

	function splitCol(td) {
		var nc = parseInt("" + td.colSpan);
		td.colSpan = 1;
		tr = td.parentNode;
		var ref = td.nextSibling;
		while (--nc > 0) {
			var otd = editor._doc.createElement("td");
			otd.rowSpan = td.rowSpan;
			otd.innerHTML = mozbr;
			tr.insertBefore(otd, ref);
		}
		editor.updateToolbar();
	};

	function splitCell(td) {
		var nc = parseInt("" + td.colSpan);
		splitCol(td);
		var items = td.parentNode.cells;
		var index = td.cellIndex;
		while (nc-- > 0) {
			splitRow(items[index++]);
		}
	};

	function selectNextNode(el) {
		var node = el.nextSibling;
		while (node && node.nodeType != 1) {
			node = node.nextSibling;
		}
		if (!node) {
			node = el.previousSibling;
			while (node && node.nodeType != 1) {
				node = node.previousSibling;
			}
		}
		if (!node) {
			node = el.parentNode;
		}
		editor.selectNodeContents(node);
	};

	switch (button_id) {
	  //DELETE TABLE
	  case "table-delete":
		var table = this.getClosest("table");
		if (!table) break;
		if(!confirm(this.translate("Delete the table and the entire content?","dialogs"))) break;
		var par = table.parentNode;
		selectNextNode(table);
		par.removeChild(table);
		editor.focusEditor();
		editor.updateToolbar();
		break;
	
		// ROWS

	    case "row-insert-above":
	    case "row-insert-under":
		var tr = this.getClosest("tr");
		if (!tr) { break; }
		var otr = tr.cloneNode(true);
		clearRow(otr);
		tr.parentNode.insertBefore(otr, /under/.test(button_id) ? tr.nextSibling : tr);
		editor.focusEditor();
		break;
	    case "row-delete":
		var tr = this.getClosest("tr");
		if (!tr) {
			break;
		}
		var par = tr.parentNode;
		if (par.rows.length == 1) {
			alert(this.translate("not-del-last-row","dialogs"));
			break;
		}
		// set the caret first to a position that doesn't
		// disappear.
		selectNextNode(tr);
		par.removeChild(tr);
		editor.focusEditor();
		editor.updateToolbar();
		break;
	    case "row-split":
		var td = this.getClosest("td");
		if (!td) {
			break;
		}
		splitRow(td);
		break;

		// COLUMNS

	    case "col-insert-before":
	    case "col-insert-after":
		var td = this.getClosest("td");
		if (!td) {
			break;
		}
		var rows = td.parentNode.parentNode.rows;
		var index = td.cellIndex;
		for (var i = rows.length; --i >= 0;) {
			var tr = rows[i];
			var ref = tr.cells[index + (/after/.test(button_id) ? 1 : 0)];
			var otd = editor._doc.createElement("td");
			otd.innerHTML = mozbr;
			tr.insertBefore(otd, ref);
		}
		editor.focusEditor();
		break;
	    case "col-split":
		var td = this.getClosest("td");
		if (!td) {
			break;
		}
		splitCol(td);
		break;
	    case "col-delete":
		var td = this.getClosest("td");
		if (!td) {
			break;
		}
		var index = td.cellIndex;
		if (td.parentNode.cells.length == 1) {
			alert(this.translate("not-del-last-col","dialogs"));
			break;
		}
		// set the caret first to a position that doesn't disappear
		selectNextNode(td);
		var rows = td.parentNode.parentNode.rows;
		for (var i = rows.length; --i >= 0;) {
			var tr = rows[i];
			tr.removeChild(tr.cells[index]);
		}
		editor.focusEditor();
		editor.updateToolbar();
		break;

		// CELLS

	    case "cell-split":
		var td = this.getClosest("td");
		if (!td) {
			break;
		}
		splitCell(td);
		break;
		
	    case "cell-insert-before":
	    case "cell-insert-after":
		var td = this.getClosest("td");
		if (!td) {
			break;
		}
		var tr = td.parentNode;
		var otd = editor._doc.createElement("td");
		otd.innerHTML = mozbr;
		tr.insertBefore(otd, /after/.test(button_id) ? td.nextSibling : td);
		editor.focusEditor();
		break;
	    case "cell-delete":
		var td = this.getClosest("td");
		if (!td) {
			break;
		}
		if (td.parentNode.cells.length == 1) {
			alert(this.translate("not-del-last-cell","dialogs"));
			break;
		}
		// set the caret first to a position that doesn't disappear
		selectNextNode(td);
		td.parentNode.removeChild(td);
		editor.updateToolbar();
		break;
	    case "cell-merge":
		
		var sel = editor.getSelection();
		var range, i = 0;
		var rows = [];
		var row = null;
		var cells = null;/*
		if (!anastasis.is_ie) {  // Mozilla 
			try {
				while (range = sel.getRangeAt(i++)) {
					var td = range.startContainer.childNodes[range.startOffset];
					if (td.parentNode != row) {
						row = td.parentNode;
						(cells) && rows.push(cells);
						cells = [];
					}
					cells.push(td);
				}
			} catch(e) { finished walking through selection }
			rows.push(cells);
		} else { */// Internet Explorer "browser"
			var td = this.getClosest("td");
			if (!td) {
				alert(this.translate("Please click into some normal cell","dialogs"));
				break;
			}
			var tr = td.parentNode;
			var no_cols = prompt(this.translate("How many columns would you like to merge?","dialogs"), 2);
			if (!no_cols) {
				// cancelled
				break;
			}
			var no_rows = prompt(this.translate("How many rows would you like to merge?","dialogs"), 2);
			if (!no_rows) {
				// cancelled
				break;
			}

		var cell_index = td.cellIndex;
		while (no_rows-- > 0) {
			td = tr.childNodes[cell_index];
			cells = [td];
			for (var i = 1; i < no_cols; ++i) {
				td = td.nextSibling;
				if (!td) {
					break;
				}
				cells.push(td);
			}
			rows.push(cells);
			tr = tr.nextSibling;
			if (!tr) {
				break;
			}
		}
			
		var HTML = "";
		for (i = 0; i < rows.length; ++i) {
			// i && (HTML += "<br />");
			var cells = rows[i];
			for (var j = 0; j < cells.length; ++j) {
				// j && (HTML += "&nbsp;");
				var cell = cells[j];
				HTML += cell.innerHTML;
				(i || j) && (cell.parentNode.removeChild(cell));
			}
		}
		var td = rows[0][0];
		td.innerHTML = HTML;
		td.rowSpan = rows.length;
		td.colSpan = rows[0].length;
		editor.selectNodeContents(td);
		editor.focusEditor();
		break;

		// PROPERTIES

	    case "table-prop":
		this.dialogTableProperties();
		break;

	    case "cell-prop":
		this.dialogCellProperties();
		break;

	    default:
		alert("Button [" + button_id + "] not yet implemented");
	}
};


// Applies the style found in "params" to the given element.
PEGOEditor.TableOperations.prototype.processStyle = function(params, element) {
	var style = element.style;	
	
	for (var i in params) {
		var val = params[i];		
		switch (i) {
		
		    case "f_align":     //Per Mozilla (ed in generale � il metodo corretto), IE setta invece il DIV esterno
	    switch(val)
      {
        case "left": style.marginLeft="0pt"; style.marginRight="auto"; break;
        case "center": style.marginLeft="auto"; style.marginRight="auto"; break;
        case "right": style.marginLeft="auto"; style.marginRight="0pt"; break;
      }
			break;
		
		    case "f_width":
		  if(val) style.width = val+"%";
			break;
			
			  case "f_height":
			if(val) style.height = val+"%";
			break;
			
		    case "f_st_backgroundColor":
			style.backgroundColor = val;
			break;
		    case "f_back_image_url":		    
			if (/\S/.test(val)) {          
				style.backgroundImage = 'url("' +_image_tmp+'?ID_NODO='+this.editor.document_id+'&NOME_FILE=file:///'+val+ '")';     				
			} else {
				style.backgroundImage = "none";
			}
			break;
		    case "f_st_borderStyle":
      style.borderStyle = val;
      if(val) { element.rules="all"; element.border="0"; }
      else { element.border="1"; if(params["f_borders"]) style.borderStyle="solid";}
			break;
		    case "f_st_borderColor":
			style.borderColor = val;
			break;
		    case "f_st_borderCollapse": 
			style.borderCollapse = val ? "collapse" : "";
			break;		    
		    case "f_textAlign": 
				style.textAlign = val;
			break;
		    case "f_st_verticalAlign":
			style.verticalAlign = val;
			break;
		    case "f_st_float":
			style.cssFloat = val;
			break;
			 	case "f_borders":	
				if(val) { style.borderWidth = val +"px"; element.border="0";}
				else { style.borderWidth = "";	style.borderStyle=""; element.border="1"; }
			break;
        case "f_top_border_style": 
      style.borderTopStyle=val;
      break;
        case "f_bottom_border_style":
      style.borderBottomStyle=val;
      break;
        case "f_right_border_style":
      style.borderRightStyle=val;
      break;
        case "f_left_border_style":
      style.borderLeftStyle=val;
      break;
        case "f_top_border_width":
      style.borderTopWidth= (val!="-") ? (val+"px") : "";
      break;
        case "f_bottom_border_width":
      style.borderBottomWidth= (val!="-") ? (val+"px") : "";
      break;
        case "f_right_border_width":
      style.borderRightWidth= (val!="-") ? (val+"px") : "";
      break;
        case "f_left_border_width":
      style.borderLeftWidth= (val!="-") ? (val+"px") : "";
      break;
// 		    case "f_st_margin":
// 			style.margin = val + "px";
// 			break;
// 		    case "f_st_padding":
// 			style.padding = val + "px";
// 			break;
		}
	}
};


