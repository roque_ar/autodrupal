// I18N constants
// LANG: "it", ENCODING: UTF-8 | ISO-8859-1
// Original Author: Fabio Rotondo <fabio@rotondo.it>

PEGOEditor.TableOperations.I18N = 
  {
  	lang: "en",
  	
  	tooltips:
  	{
  	  "table-delete"         : 	"Delete table",
	  "cell-delete"          :	"Delete cell",
	  "cell-insert-after"    :	"Insert cell after",
	  "cell-insert-before"   :	"Insert cell before",
	  "cell-merge"           :	"Merge cells",
	  "cell-prop"            :	"Cell properties",
	  "cell-split"           :	"Split cell",
	  "col-delete"           :	"Delete column",
	  "col-insert-after"     :	"Insert column after",
	  "col-insert-before"    :	"Insert column before",
	  "col-split"            :	"Split column",
	  "row-delete"           : 	"Delete row",
	  "row-insert-above"     :	"Insert row before",
	  "row-insert-under"     :	"Insert row after",
	  "row-prop"             : 	"Row properties",
	  "row-split"            :	"Split row",
	  "table-prop"           :	"Table properties"
  	},
  	dialogs:
  	{
	 
  	}
  };
  
  
