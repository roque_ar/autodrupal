// I18N constants

// LANG: "en", ENCODING: UTF-8 | ISO-8859-1
// Original Author: Mihai Bazon, http://dynarch.com/mishoo


PEGOEditor.I18N = {

	// the following should be the filename without .js extension
	// it will be used for automatically load plugin language.
	lang: "en",

	tooltips: {
		bold:           "Bold",
		italic:         "Italic",
		underline:      "Underline",
		strikethrough:  "Strikethrough",
		subscript:      "Subscript",
		superscript:    "Superscript",
		justifyleft:    "Justify Left",
		justifycenter:  "Justify Center",
		justifyright:   "Justify Right",
		justifyfull:    "Justify Full",
		insertorderedlist:    "Ordered List",
		insertunorderedlist:  "Bulleted List",
		outdent:        "Decrease Indent",
		indent:         "Increase Indent",
		forecolor:      "Font Color",
		hilitecolor:    "Background Color",
		horizontalrule: "Horizontal Rule",
		createlink:     "Insert Web Link",
		insertimage:    "Insert/Modify Image",
		inserttable:    "Insert Table",
		insertresource: "Insert external resource",
		htmlmode:       "Toggle HTML Source",
		popupeditor:    "Enlarge Editor",
		about:          "About this editor",
		showhelp:       "Help using editor",
		textindicator:  "Current style",
		undo:           "Undoes your last action",
		redo:           "Redoes your last action",
		cut:            "Cut selection",
		copy:           "Copy selection",
		paste:          "Paste from clipboard",
		lefttoright:    "Direction left to right",
		righttoleft:    "Direction right to left",
		changelang:     "Specify language",
		insertanchor:   "Insert anchor",
		preview:        "Preview",
		save:           "Save",
		cancel:         "Cancel",
		nodelink:       "Internal node link",
		findreplace:    "Find and replace",
		clearstyle:      "Clear style attributes"
	},
	
	formatblock:
	{
    Heading1: "Heading 1",
		Heading2: "Heading 2",
		Heading3: "Heading 3",
		Heading4: "Heading 4",
		Heading5: "Heading 5",
		Heading6: "Heading 6",
		Normal: "Normal"
  },
  
	buttons: {
		"ok":           "OK",
		"cancel":       "Cancel"
	},

	msg: {
		"Path":         "Path",
		"TEXT_MODE":    "You are in TEXT MODE.  Use the [<>] button to switch back to WYSIWYG."
    
	},
	
	templates:
	{
  
  },

	dialogs: {
		"Cancel"                                            : "Cancel",
		"Insert/Modify Link"                                : "Insert/Modify Link",
		"New window (_blank)"                               : "New window (_blank)",
		"None (use implicit)"                               : "None (use implicit)",
		"OK"                                                : "OK",
		"Other"                                             : "Other",
		"Same frame (_self)"                                : "Same frame (_self)",
		"Target:"                                           : "Target:",
		"Title (tooltip):"                                  : "Title (tooltip):",
		"Top frame (_top)"                                  : "Top frame (_top)",
		"URL:"                                              : "URL:",
		"You must enter the URL where this link points to"  : "You must enter the URL where this link points to"
	},
  
  statusbar_tags:
  {
  "body"    : "page",
  "p"       : "paragraph",
  "table"   : "table",
  "tr"      : "row",
  "td"      : "cell",
  "img"     : "image",
  "div"     : "block",
  "span"    : "block",
  "a"       : "link",
  "ol"      : "list",
  "ul"      : "list",
  "li"      : "item"
  }
  
};
