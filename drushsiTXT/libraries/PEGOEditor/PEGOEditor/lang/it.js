// I18N constants

// LANG: "it", ENCODING: UTF-8 | ISO-8859-1

PEGOEditor.I18N = {
	// the following should be the filename without .js extension
	// it will be used for automatically load plugin language.
	lang: "it",

	tooltips: {
		bold:           "Grassetto",
		italic:         "Corsivo",
		underline:      "Sottolineato",
		strikethrough:  "Barrato",
		subscript:      "Pedice",
		superscript:    "Apice",
		justifyleft:    "Allinea a sinistra",
		justifycenter:  "Centra",
		justifyright:   "Allinea a Destra",
		justifyfull:    "Giustifica",
		insertorderedlist:    "Lista Ordinata",
		insertunorderedlist:  "Lista Puntata",
		outdent:        "Decrementa Indentazione",
		indent:         "Incrementa Indentazione",
		forecolor:      "Colore del testo",
		hilitecolor:    "Colore di Sfondo",
		inserthorizontalrule: "Linea Orizzontale",
		createlink:     "Inserisci un Link",
		insertimage:    "Inserisci un'Immagine",
		inserttable:    "Inserisci una Tabella",
		insertresource: "Inserici una risorsa esterna",
		htmlmode:       "Attiva il codice HTML",
		popupeditor:    "Allarga l'editor",
		about:          "Info sull'editor",
		showhelp:       "Aiuto sull'editor",
		textindicator:  "Stile Attuale",
		undo:           "Elimina l'ultima modifica",
		redo:           "Ripristina l'ultima modifica",
		cut:            "Taglia l'area selezionata",
		copy:           "Copia l'area selezionata",
		paste:          "Incolla dalla memoria",
		changelang:     "Specifica la lingua",
		insertanchor:   "Inserisci un'ancora",
		preview:        "Anteprima",
		save:           "Salva",
		cancel:         "Annulla",
		nodelink:       "Collegamento a nodo interno",
		findreplace:    "Trova e sostituisci",
		clearstyle:     "Elimina attributi di stile",
		disable_key_shortcuts : "Disabilita scorciatoie tastiera",
		deleteall:      "Cancella tutto",
		pasteunformatted:	"Incolla senza formattazione"
	},
	
	formatblock:
	{
    Heading1: "Titolo 1",
		Heading2: "Titolo 2",
		Heading3: "Titolo 3",
		Heading4: "Titolo 4",
		Heading5: "Titolo 5",
		Heading6: "Titolo 6",
		Paragraph: "Paragrafo",
		None: "Nessuno"
  },

	buttons: {
		"Ok"      : "Ok",
		"Cancel"  : "Annulla"
	},
	
	labels: {
	 "HTML: "    : "HTML"
	},

	msg: {
		"Path"                                                    : "Posizione",
		"TEXT_MODE"                                               : "Sei in MODALITA' HTML. Usa il bottone [<>] per tornare alla modalita' EDITOR.",	
		"An error occured during saving:"                         : "Errore durante il salvataggio",
		"Accessibility warnig: there are two adiacent links in the page"      : "Problema di accessibilita': ci sono due link adiacenti nella pagina",
		"Accessibility warnig: object without alternative text"   : "Problema di accessibilita': oggetto senza testo alternativo",
		"Accessibility warnig: image without alternative text"    : "Problema di accessibilita': immagine senza testo alternativo",
		'Accessibility warnig: Table without summary \nAn automatic empty summary will be added ("")'  : 'Problema di accessibilita\': tabella senza sommario\nVerra\' inserito un sommario vuoto("")',
		"Accessibility warnig: forms are not handled by this editor.Accessibility is not assured": "Problema di accessibilita': i form non sono supportati dall'editor. L'accessibilita' non puo' essere assicurata",
		"Accessibility warnig: Table width and height must be specified in percentual values.": "Problema di accessibilita': altezza e larghezza di una tabella vanno specificate con valori percentuali.\nUsare il bottone 'Proprieta' tabella' per stabilirli. ",
		"Warning: some text is probably pasted form Microsoft Word and can be displayed with differences or errors.\nYou should paste the text from Word using the 'Paste unformatted' button in order to erase Word HTML tags": "Attenzione: parte del testo e' stata probabilmente incollata da Microsoft Word e potrebbe essere visualizzata diversamente o con errori.\nE' preferibile incollare il testo utilizzando il bottone 'Incolla senza formattazione', per eliminare i tag HTML inseriti da Word",		  				
		"was not found in this page"                              : "non e' stato trovato in questa pagina",
		"Reached end of document, start from the top of the page?": "Raggiunta la fine del documento, riprendere a sostituire dall'inizio?",
		"Reached end of document without findig any more occurrencies of"  : "Raggiunta la fine del documento senza trovare altre occorrenze di",
		"Anchor: "                                                :"Ancora: ",
		"Warning: there is an error in some HTML code, that must have been pasted or inserted by hand": "Attenzione: c'e' un errore in una porzione di codice HTML incollata o inserita a mano",
		"Exit without saving"                                    : "Uscire senza salvare le modifiche",
		"Opening a new window"                                    : "Apre una nuova finestra",
		"Delete all?"                                             : "Cancellare tutto?",
		"Warning: the page contains scripts. The Editor, due to some bugs of Internet Explorer, cannot save them. Please use Mozilla (download for free at mozilla.org)." : "Attenzione: la pagina contiene script. L'Editor, a causa di bug interni di Internet Explorer, non e' in grado di salvarli. Per farlo si consiglia di utilizzare il browser Mozilla (scaricabile gratuitamente su mozilla.org)",
		"Warning: the page contains scripts. The Editor can not manage script comments (//), if any delete them from the script" : "Attenzione: la paigna contiene script. Poiche' l'Editor non supporta i commenti degli script (//) e' necessario eliminarli",
		"Saving..." : "Salvataggio in corso...",
		"Each image must be inserted with the proper button. Please check the following images" : "Tutte le immagini vanno inserite con l'apposito bottone!Correggere le seguenti immagini",
		"Link to the"                                             : "Salta alla ",
		"nth line of buttons"                                     : "&#176; linea di pulsanti",
		"Select"                                                  : "Seleziona",
		"Movie"                                                   : "Filmato",
		"Wrong button. Use the proper one to edit the element"   : "Bottone errato. Usare quello opportuno per l'elemento selezionato.",
		"Errors during Editor loading"							  : "Errori durante il caricamento dell'Editor"
	},
		
	dialogs:
	{
    "Text"                      : "Testo",
    "Ok"                        : "Ok",
		"Cancel"                    : "Annulla",	
		"Title (tooltip)"           : "Titolo (tooltip)",	
    "URL"                       : "Indirizzo",	
    "Node number"               : "Numero del nodo",
    "Preview"		                : "Anteprima",
    "Percent"                   : "Percentuale",
    "Close"                     : "Chiudi",
    
          //Allineamento
		"Not set"                  : "Non specificato",
    "Left"                     : "Sinistra",
    "Right"                    : "Destra",
    "Bottom"                   : "In basso",
    "Middle"                   : "Al centro",
    "Top"                      : "In alto",
		
		//Titoli
		"Insert/Modify Link"        : "Inserisci/Modifica Link",
		"Insert/Modify Internal Node Link"   : "Inserisci/Modifica Collegamento Interno",
		"Insert/Modify Resource"    : "Inserisci/Modifica Risorsa",
		"Insert/Modify Anchor"      : "Inserisci/Modifica Ancora",
		"Insert/Modify Image"       : "Inserisci/Modifica Immagine",
		"Insert/Modify Movie"       : "Inserisci/Modifica Filmato",
		"Insert/Modify Audio Effect": "Inserisci/Modifica Effetto Audio",
		"Insert Table"              : "Inserisci Tabella",
		"Insert plain text or hml":	"Inserisci testo non formattato o codice html",
		"Specify Language"          : "Specifica la lingua",
		"Select color"              : "Scegli il colore",
		"Find and replace"          : "Trova e sostituisci",
		
		//target
		"Target"                    : "Target",
    "New window (_blank)"       : "Nuova finestra (_blank)",
	  "Same frame (_self)"        : "Stesso frame (_self)",
		"Top frame (_top)"          : "Top frame (_top)",
		"None (use implicit)"       : "Nessuno (usa default)",
		"Other"                     : "Altro",	
		
		//Immagini
		"Image URL"                 : "Indirizzo dell'immagine",
		"Alternate text"            : "Testo alternativo",
		"Do not insert alternate text" : "Non inserire testo alternativo",
		"Layout"                    : "Disposizione",
		"Alignment"                 : "Allineamento",
		"Border thickness"          : "Spessore del bordo",
		"Spacing"                   : "Spaziatura",
		"Horizontal"                : "Orizzontale",
		"Vertical"                  : "Verticale",
		"Image Preview"             : "Anteprima dell'immagine",
		
		//Tabelle
		"Summary"                   : "Sommario",
		"Do not insert summary"     : "Non inserire il sommario"	,
		"Rows"                      : "Righe",
		"Cols"                      : "Colonne",
		"Width"                     : "Larghezza" ,
		"Cell spacing"              : "Spaziatura tra le celle",
		"Cell padding"              : "Padding delle celle",
		
		//Lingua
		"Select another language(insert code)": "Scegli un'altra lingua",
		"Italian"                  : "Italiano",
    "English"                  : "Inglese",
    "French"                   : "Francese",
    "German"                   : "Tedesco",
    "Spanish"                  : "Spagnolo",
    
    //Trova e sostituisci
    "Find"                      : "Trova",
    "Replace"                   : "Sostituisci",
    "Replace all"               : "Sostituisci tutti",
    "Replace with"              : "Sostituisci con",
    "Match case"                : "Maiuscole/minuscole",     
        
    //Misto
    "Unlink"                    : "Rimuovi collegamento",
    "Remove anchor"             : "Rimuovi ancora",
    "Unlink resource"           : "Rimuovi collegamento",
    "Delete image"              : "Elimina immagine",
    "Use the button"            : "Usa il bottone",    
    "to edit this element"     : "per modificare questo elemento",
    "Try"                       : "Prova",
    "Colors accessible on a white background" : "Colori accessibili su sfondo bianco",
    
    //Messaggi
	  "You must enter the alternative text"                     : "E' obbligatorio l'inserimento del testo alternativo",
		"You must enter the URL"                                  : "E' obbligatorio l'inserimento dell'indirizzo",
		"You have to enter an URL first"                          : "Prima inserisci un indirizzo",
		"You must enter the URL where this link points to"        : "E' obbligatorio l'inserimento dell'indirzzo a cui punta il link",
		"You must enter the text"                                 : "E' obbligatorio l'inserimento del testo del link",
		"Now click in the text where you want to put the anchor"  : "Ora clicca nel testo nel punto dove vuoi agganciare l'ancora!",
		"You must compile the field"                              : "Devi compilare il campo",
		"You must enter a number of rows"                         : "E' obbligatorio l'inserimento del numero di righe",
		"You must enter a number of columns"                      : "E' obbligatorio l'inserimento del numero di colonne",
		"You must enter the summary"                              : "E' obbligatorio l'inserimento del sommario",
		"You must enter an ID"                                    : "E' obbligatorio l'inserimento del campo ID",
		"Invalid color code: "                                    : "Codice del colore non valido:  ",
		"There is the movie"                                      : "E' presente il filmato",
		
		//Titles
		  //Insert table
		"No summary"                    : "Nessun sommario",
		"Number of rows"                : "Numero di righe",
		"Number of columns"             : "Numero di colonne",
		"Width of the table"            : "Larghezza della tabella",
		"Positioning of the table"      : "Posizionamento dela tabella",
		"Leave empty for no border"     : "(Lasciare vuoto per una tabella senza bordi)",
		"Space between adjacent cells"  : "Spazio tra celle adiacenti",
		"Space between content and border in cell"  : "Spazio tra il bordo della cellla ed il suo contenuto",
		  //Insert image
		"Enter the image URL here"      : "Indirizzo dell'immagine",
		"Preview the image"             : "Anteprima dell'immagine",
		"For browsers that don't support images"  : "Testo sostitutivo dell'immagine",
		"No alterntive text"            : "Nessun testo alternativo",
		"Positioning of this image"     : "Posizionamento dell'immagine",
		"Horizontal padding"            : "Spaziatura orizzontale",
		"Vertical padding"              : "Spaziatura verticale",
		"Left, text flows on the right" : "Sx, testo scorre a dx",
		"Right, text flows on the left" : "Dx, testo scorre a sx",
		
		
		  //Change language
		"Language selection"            : "Scelta della lingua",
		"Select another language(insert code)"  : "Scegli un'altra lingua (inserire codice)",
		
		//Paste unformatted
		"Text or custom html"	:	"Testo o codice html",
		"Keep new lines"		:	"Mantieni capo riga"		
  },
  
  statusbar_tags:
  {
  "body"    : "pagina",
  "p"       : "paragrafo",
  "table"   : "tabella",
  "tr"      : "riga",
  "td"      : "cella",
  "th"      : "cella intestazione",
  "img"     : "immagine",
  "div"     : "blocco a capo",
  "span"    : "blocco",
  "a"       : "link",
  "ol"      : "elenco",
  "ul"      : "elenco",
  "li"      : "voce",
  "h1"      : "titolo 1",
  "h2"      : "titolo 2",
  "h3"      : "titolo 3",
  "h4"      : "titolo 4",
  "h5"      : "titolo 5",
  "h6"      : "titolo 6",
  "strong"  : "blocco",
  "em"      : "blocco",
  "Class"   : "Classe",
  "Style"   : "Stili"
  }
  
};
