// JavaScript Document
PEGOEditor._config_options=
{
  width     : "auto",
  height    : "auto",

	// enable creation of a status bar?
	statusBar : true,
	// enable background image in toolbar (/images/toolbar-back.jpg)
	toolbarBackroundImage: true,

	// URL-s
	imgURL : "images/",
	popupURL : "popups/",
	
	// Stop on acccessibility warnings
	stop_on_accessibility_warning: true,
	
	//libert� di assegnare stili (se false si possono usare solo le classi - se vengono contenmporaneamente disabilitati gli opportuni bottoni)
	freestyle: false,

	/**
	 * Altri elementi disponibili:
	 * fontname - fontsize - lefttoright - righttoleft   	
	 * Qualunque altra stringa viene visualizzata come etichetta	 
	 */
	 
	toolbar : [
		//Prima fila
    [ "ancora","undo", "redo","bold", "italic", "underline","justifyleft", 
      "justifycenter", "justifyright", "justifyfull","insertorderedlist", "insertunorderedlist"
    ],
		//Seconda fila  
		  [		  
		  "formatblock","inserthorizontalrule","createlink",
      "newblock","hiblocks","deleteall","htmlmode","showhelp"]
	]
}

//Simboli unicode che vanno convertiti nel corrispondente codice HTML
HTMLArea._unicode_symbols=
{
  0x20AC:  "&euro;",
  0x0060:  "'"
}
