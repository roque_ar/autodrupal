// JavaScript Document
PEGOEditor._config_options=
{
  width     : "auto",
  height    : "auto",

	// enable creation of a status bar?
	statusBar : true,
	// enable background image in toolbar (/images/toolbar-back.jpg)
	toolbarBackroundImage: true,

	// URL-s
	imgURL : "images/",
	popupURL : "popups/",
	
	// Stop on acccessibility warnings
	stop_on_accessibility_warning: true,

	/**
	 * Altri elementi disponibili:
	 * fontname - fontsize - lefttoright - righttoleft   	
	 * Qualunque altra stringa viene visualizzata come etichetta	 
	 */
	toolbar : [
		//Prima fila
    [ "ancora","save", "preview", "cancel","separator","space", 
      "undo", "redo","findreplace","separator",
		  "bold", "italic", "separator",
		  "insertorderedlist", "insertunorderedlist", "separator"
    ],
		//Seconda fila  
		  [
		  "HTML: ","space","ancora",
		  "formatblock","space","separator",      	  
		  "inserthorizontalrule", "separator",
      "createlink","insertanchor","nodelink","separator",
      "inserttable","separator",
      "insertimage", "insertresource", "insertmovie","insertaudio","separator",
      "newblock","hiblocks","space","separator",
		  "changelang","curlang","space","separator",
		  "deleteall","clearstyle","separator",
      "space","htmlmode","space","separator",
		  "space","showhelp", "about" ]
	]
}
