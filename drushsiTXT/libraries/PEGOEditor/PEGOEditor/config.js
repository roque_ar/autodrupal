// JavaScript Document
PEGOEditor._config_options=
{
  lang: "it",
  
  width     : "auto",
  height    : "auto",

	// enable creation of a status bar?
	statusBar : true,
	// enable background image in toolbar (/images/toolbar-back.jpg)
	toolbarBackroundImage: true,
	
	// Stop on acccessibility warnings
	stop_on_accessibility_warning: true,	

	/**
	 * Altri elementi disponibili:
	 * lefttoright - righttoleft - insertimage   	
	 * Qualunque altra stringa viene visualizzata come etichetta	 
	 */
	toolbar : [
		//Prima fila
    [ "Standard","save", "cancel","separator","space", 
      "undo", "redo","findreplace","pasteunformatted","separator",
		  "bold", "italic", "underline", "strikethrough", "separator",
		  "subscript", "superscript", "separator", 
		  "justifyleft", "justifycenter", "justifyright", "justifyfull","separator", 
		  "insertorderedlist", "insertunorderedlist", "outdent", "indent", "separator",
		  "forecolor", "hilitecolor"
    ],
		//Seconda fila  
		  ["linebreak",
		  "HTML","space",
		  "formatblock","space","separator",      	  
		  "inserthorizontalrule", "separator",
      "createlink","insertanchor","nodelink","separator",
      "inserttable","separator",      
      "space","separator",
		  "changelang","space","separator",
		  "deleteall","clearstyle","separator",
      "space","htmlmode","space","separator",
		  "space","showhelp", "about" ]
	],
	
	//----------------PLUGINS------------------------//
	
	//CSSHandler
	
	//true: user can assign only classes to the html elements
	//false: user can assign to each element various style propeerties, AND a class
	freestyle: true
}
