function PopupEditor()
{}

PopupEditor.openPopupEditor=function(popupeditor_url,textInputId,textOutputId)
{
	var textInput=document.getElementById(textInputId);
	if(!textInput) 
	{
		PopupEditor.errore("openPopupEditor","Campo testo di input assente.");
		return;
	}
	
	init=new Object();
	init["text"]=textInput.innerHTML;
	init["plugins"]=Drupal.settings.PEGOEditor.plugins;
	init["toolbar"]=[];
	for(var i in Drupal.settings.PEGOEditor.toolbar)
	{
		init["toolbar"][0]=Drupal.settings.PEGOEditor.toolbar[i];
	}
	init["formatblock"]=Drupal.settings.PEGOEditor.formatblock;
	init["lang"]=Drupal.settings.PEGOEditor.lang;
	init["drupal_base_url"]=Drupal.settings.PEGOEditor.base;
	
	var action=function(param) 
	{
		if(textInputId!=textOutputId) textInput.innerHTML=param["text"];		
    	if(anastasis.is_ie)
    		document.getElementById(textOutputId).value=param["text"];
    	else
    	{
    		document.getElementById(textOutputId).value=param["text"];
    		document.getElementById(textOutputId).innerHTML=param["text"];
    	}
	}
		

	anastasis.openDialog(popupeditor_url, action, init);
}

PopupEditor.errore=function(metodo,err)
{
	alert("[PopupEditor."+metodo+"]\n"+err);
}
