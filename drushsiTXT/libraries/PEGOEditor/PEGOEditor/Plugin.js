//Copyright 2008 (c) Anastasis Societa' Cooperativa, Bologna, www.anastasis.it

/**
 * "Abstract" class that every plugin must subclass.
 * Note that note all the abstract methods have to be implemented, but only the methods useful for the current plugin.
 */

PEGOEditor.Plugin=function(editor)
{
	this.editor = editor;	  	
	
	this.name="PluginName";
	this.buttonList=null;	
	this.i18n=null;	
	
	//Examples
	//this.buttons = 
	//{
	//	NAME: [ "TOOLTIP", editor.imgURL("IMGNAME.gif", this.name), TEXTMODE?, function(editor,id) { ...ACTION... },"CONTEXT",function(editor){return ...ACTIVATION...}],
	//	NAME: [ "TOOLTIP", editor.imgURL("IMGNAME.gif", this.name), TEXTMODE?, function(editor,id) { ...ACTION... },"CONTEXT",function(editor){return ...ACTIVATION...}]
	//}

	//this.selects = 
	//{
	//	NAME : ["TOOLTIP",
	//				TEXTMODE?,
	//				options={
	//						"NAME":   "VALUE",
	//					    "NAME": "VALUE"
	//						},
	//				function(editor,myself)
	//				{
	//					..ACTION...
	//				},
	//				"CONTEXT",
	//				function(editor,myself,ancestors)
	//				{
	//					...UPDATE...
	//				}
	//				]					
	//};
	
}

/**
 * Concrete method that MUST be called at the end of the constructor.
 * It is the same for every plugin; customizations must be inlucded in the onInit() function
 */
PEGOEditor.Plugin.prototype.init=function()
{
	this.registerLang();
	if(this.buttonList)
	{
		var me = this;
		var cfg = this.editor.config;		
		var toolbar=null;
		if(this.buttons)
		{
			cfg.registerButtons(this.buttons);			
		}
		else	//for compatibility issue
		{
			// register the toolbar buttons provided by this plugin
			toolbar=[];		
			for (var i in this.buttonList) 
			{
				var btn = this.buttonList[i];
				if (!btn) 
				{
					toolbar.push("separator");
				} 
				else 
				{
					var id = btn[0];
					cfg.registerButton(id, this.translate(id,"tooltips"), this.editor.imgURL(btn[0] + ".gif", this.name), false, function(editor,id) { me.buttonPress(editor,id); }, btn[1]);
					toolbar.push(id);
				}
			}	  	
			cfg.toolbar.push(toolbar);
		}
		
		if(this.selects)
		{
			cfg.registerSelects(this.selects);			
		}
		
		if(!toolbar) cfg.addToolbarObjects(this.buttonList);
	}
	if(typeof(this.onInit)=="function") this.onInit();
}

PEGOEditor.Plugin.prototype.registerLang=function()
{
	if(!this.I18N) return;
	for(var field in this.I18N)
	{
		if(!this.editor.I18NCollection[field])
			this.editor.I18NCollection[field]=new Object();
		
		for(var term in this.I18N[field])
		{
			this.editor.I18NCollection[field].add(term,this.I18N[field][term]);
		}
	}	
}

/**
 * Translation function         TODO: just call the editor translation funcion
 * @param term the term (or phrase) to translate
 * @param field the field where to search for the term
 */
PEGOEditor.Plugin.prototype.translate=function(term,field)
{
	return this.editor.translate(term,field);
}

//---------------------ABSTRACT-METHODS--------------------------------//

/**
 * Abstract method called at the end of init(). It takes no params.
 */
PEGOEditor.Plugin.onInit=null;	//=function()

/**
 * Abstract method call when one of the registered buttons is pressed.
 * @param id the id of the pressed button
 */
PEGOEditor.Plugin.prototype.buttonPress = null; //=function(editor,button_id) 

/**
 * Abstract method called at the end the generate() method of the editor. It takes no params.
 */
PEGOEditor.Plugin.prototype.onGenerate=null;//=function()

/**
 * Abstract method called at the beginning of the replaceOnGenerate() method of the editor. 
 * @param root HTMLElement; the original HTML element
 * @return HTMLElement; the modified new HTML element, or null if nothing is done  
 */
PEGOEditor.Plugin.prototype.onReplaceOnGenerate=null;//function(original)

/**
 * Abstract method called at the beginning of the replaceHTMLOnGenerate() method of the editor. 
 * @param html String; HTML code of the page
 * @return String; the modified new HTML code of the page, or simply html if nothing is done  
 */
PEGOEditor.Plugin.prototype.onReplaceHTMLOnGenerate=null;//function(html)
/**
 * Abstract method call when one of the toolbar is update
 * @param ancestors an array of the ancestor of the element currenlty focused in the editor
 */
PEGOEditor.Plugin.prototype.onUpdateToolbar = null;//function(ancestors)

/**
 * Abstract method called at the beginning of the replaceElements() method of the editor. 
 * @param root HTMLElement; the original HTML element
 * @return HTMLElement; the modified new HTML element, or null if nothing is done  
 */
PEGOEditor.Plugin.prototype.onReplace=null;//function(root)

/**
 * Abstract method called after getting the text from the editor, before putting it in the textarea and sending it
 * @param text String; the complete HTML of the page
 * @return String the same text edited (if the case) by the plugin 
 */
PEGOEditor.Plugin.prototype.onSave = null;//function(text)

/**
 * Abstract method called before sending the code of the page to the server 
 */
PEGOEditor.Plugin.prototype.onSend = null;//function()

/**
 * Abstract method called during the final check of the page before saving.
 * If it returns false, the saving process is stopped (should give an explanation)
 * @param body_clone HTMLElement; a clone of the 'body' of the editor content (after replace the 'temporary' elements)
 * @return boolean; whether or not to proceed saving
 */
PEGOEditor.Plugin.prototype.onCheckTree = null;//function(body_clone)

/**
 * Abstract method called during the final check of the page HTML code before saving
 * If it returns false, the saving process is stopped (should give an explanation)
 * @param text HTMLElement; a string representing the complete HTML code of the page (after replace the 'temporary' elements)
 * @return boolean; whether or not to proceed saving
 */
PEGOEditor.Plugin.prototype.onCheckHTML = null;//function(text)

/**
 * Abstract method called if a wrong button is pressed to edit an existing element.
 * Returns the suggestion of the correct button to use, or null if it does not belong to this plugin.
 * @param id String; the id of the currently selected element
 * @param className String; the class of the currently selected element 
 * @return String; the message to be prompted
 */
PEGOEditor.Plugin.prototype.onSuggestButton = null;//function(id,className)

/**
 * Abstract method called when a 'double-click' event is raised.
 * If the selected element matches something related to this plugin, do the proper calls and return true, else return false.
 * @param selectedElement HTMLElement;the currently selected element in the editor
 * @return boolean; true if the element belongs to this plugin (stops every further action), false otherwise.
 */
PEGOEditor.Plugin.prototype.onDoubleClick = null;//function(selectedElement)

