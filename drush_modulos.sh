#!/bin/bash
###########################################
#  instalador de módulos para drupal      #
#         -a través de drush -            #
#             Roque Perez                 #
#                 2013                    #
###########################################

# Bold
ENDCOLOR="\033[0m"	 
GRIS="\033[1;30m"	;	GRAY="\033[1;30m"
ROJO="\033[1;31m"	;	RED="\033[1;31m" 
VERDE="\033[1;32m"  	;	GREEN="\033[1;32m"
AMARILLO="\033[1;33m" 	;	YELLOW="\033[1;33m" 
AZUL="\033[1;34m"	;	BLUE="\033[1;35m" 
LILA="\033[1;35m"	;	PURPLE="\033[1;35m"
CELESTE="\033[1;36m"	;	CYAN="\033[1;36m"
BLANCO="\033[1;37m"	;	WHITE="\033[1;37m"
# Background
NEGRO2="\033[2;30m"	;	BLACK2="\033[2;30m" 
ROJO2="\033[2;31m"    	;	RED2="\033[2;31m" 
VERDE2="\033[2;32m"  	;	GREEN2="\033[2;31m" 	      
AMARILLO2="\033[2;33m" 	;	YELLOW2="\033[2;33m" 
AZUL2="\033[2;34m"	;	BLUE2="\033[2;34m"      
LILA2="\033[2;35m"	;	PURPLE2="\033[2;35m"
CELESTE2="\033[2;36m"	;	CYAN2="\033[2;36m"  
BLANCO2="\033[2;37m"	;	WHITE2="\033[2;37m"  
# Normal
NEGRO1="\033[0;[30m"	;	BLACK1="\033[0;30m" 
ROJO1="\033[0;[31m"    	;	RED1="\033[0;31m" 
VERDE1="\033[0;[32m"  	;	GREEN1="\033[0;32m" 	      
AMARILLO1="\033[0;[33m"	;	YELLOW1="\033[0;33m" 
AZUL1="\033[0;[34m"	;	BLUE1="\033[0;34m"      
LILA1="\033[0;[35m"	;	PURPLE1="\033[0;35m"
CELESTE1="\033[0;[36m"	;	CYAN1="\033[0;36m"  
BLANCO1="\033[0;[37m"	;	WHITE1="\033[0;37m"  

RUTA_scripts=$0
RUTA_SCRIPTS=$(dirname $0)      #DIRECTORIO donde se encuentra el script drush6siv2.sh
detectar_ruta=$(echo $RUTA_SCRIPTS | cut -c1)
    if [ "$detectar_ruta" != "/" ];then RUTA_SCRIPTS=$PWD/$RUTA_SCRIPTS;fi  #si se trata de ruta relativa, se llama al script desde una ruta relativa y se debe modificar la variable
RUTA_SCRIPTS_txt="$RUTA_SCRIPTS/drushsiTXT"     #donde los txt
BKPIFS=$IFS  #IFS=$'\x0A'$'\x0D'    #FINAL DE LINEA Y TABULADOR
FICH_MOD="$RUTA_SCRIPTS_txt/lista_modulos.txt"
#ficheros generados por el scripts
FICH_MOD_NEW="$RUTA_SCRIPTS_txt/lista.nueva.modulos.txt"
FICH_TMP="$RUTA_SCRIPTS_txt/temp.txt"
FICH_LMP_aux="$RUTA_SCRIPTS_txt/lista_modulos_principales_aux.txt"
FICH_BUSCA_TMP="$RUTA_SCRIPTS_txt/bbusca.txt"

CUENTA=$(cat $FICH_MOD | wc -l)
				       
###########################################################################################################################################
#############    PREPARACIÓN DEL ARCHIVO DE TEXTO QUE CONTIENE EL NOMBRE DE LOS MODULOS (/home/scripts/lista_modulos.txt)  ################
#############                 file:///home/roque/Downloads/SILICON/scripts/mod.listamodulos.txt.sh                         ################
###########################################################################################################################################
clear 

path=${FICH_MOD%/*} #obtiene /home/scripts  -sin la / -
file=${FICH_MOD##*/} #obtiene lista_modulos.txt
base=${file%%.*} #obtiene lista_modulos

if [ -f $FICH_MOD_NEW ];then rm -rf $FICH_MOD_NEW ;fi  #elimina el fichero auxiliar si existiese

   echo "$VERDE Deseas preparar el fichero con las lista de módulos, es conveniente hacerlo si se han agregado nuevos módulos: $AZUL \c "; read u
   
   if [ "$u" = s ] || [ "$u" = S ] || [ "$u" = y ] || [ "$u" = Y ] || [ "$u" = "" ]; then
      
     echo "$LILA   \n      >>>>>>>>>>>>>>> Preparando el fichero con la lista de módulos <<<<<<<<<<<<<<<<< \n"
    
      cp $FICH_MOD $RUTA_SCRIPTS_txt\_`date +%a%d_%b%Y_%H-%M`.txt  #backup del archivo
      
      sed -i 's/ /_/g' $FICH_MOD  #-i modifica el mismo fichero #elimina los espacios cambiándolos por _ -guien bajo-
      sed -i 's/^[ \t]*//;s/[ \t]*$//' $FICH_MOD  
	 #eliminar espacio al inicio y final de cada línea
      sed -i '/\:$/s/$/SIN_DESCRIPCIÓN/' $FICH_MOD 
	 #Agrega "SIN DESCRIPCIÓN" a la línea que termina en : o sea el módulo que no tiene descripción

      sort -o $FICH_MOD -u $FICH_MOD     #ordenar alfabéticamente	--la opción u elimina filas duplicadas--
      
	       for line in $(cat $FICH_MOD); do    #separa nombre del modulo y descripción utilando : como separador y trabaja con ambos 

		     modulo=$(echo  ${line%:*})  #obtener el nombre del módulo que está antes de :
		     modulo=$(echo $modulo | awk '{print tolower($0)}') #pasar a minúsculas

		     descrip="$(echo ${line#*:})" #obtener la descripción que esta después de :
	    #  if [ -z $descrip ];then descrip="SIN_DESCRIPCIÓN";fi  # agrega la nomenclatura SIN DESCRIPCIÓN a los módulos que no la tienen
	    # echo con sed en línea 22       # opción -z indica si descrip tiene length 0
		     descrip="$(echo $descrip | awk '{print tolower($0)}')" #pasar a minusculas (mayúsculas -toupper-) toda la descripción
		     descrip="$(echo $descrip | sed -r 's/\<./\u&/')"  #cambia a mayúsculas la primera letra de cada línea	 		    
		     echo "$modulo:$descrip" >> $FICH_MOD_NEW
		     
	       done
	       
	    mv $FICH_MOD_NEW $FICH_MOD    #reemplaza el archivo: lista_modulos.txt
	    if [ -f $FICH_MOD_NEW ];then rm -rf $FICH_MOD_NEW ;fi # eliminar el archivo auxiliar si existe

   fi
###############################################################################################################################################
####################################            MENÚ PARA LA INSTALACIÓN DE MÓDULOS            ################################################
###############################################################################################################################################


echo "\n $AZUL  Actualmente existen $ROJO $CUENTA $AZUL módulos en la base de datos $ENDCOLOR \n" ;sleep 2
PASO=0
#~~~~~~~~~~~ PREGUNTAR SI DESEA INSTALAR UN MODULO ESPECIFICO
#FILTRO_LETRA=(cat $FICH_MOD | cut -c 1 | grep $LETRA)
#hacer menu 1.quiere ver todos los modulos 2. quieres ver por letra 3.quiere buscar

while [ OPCION != "0" ]  
do 
clear 
echo " $AZUL Elija una opción $VERDE \n" 
echo "  0    Salir" 
echo "  1    Mostrar módulos filtrados por letra" 
echo "  2    Mostrar todos los módulos" 
echo "  3    Buscar módulo por criterio " 
echo "  4    Mostrar lista de módulos \n"
read -p " ?  " OPCION

PASO=0

      case $OPCION in
	0)
	    echo ""
	    if [ -f $FICH_TMP ] ; then rm -RIf $FICH_TMP; fi   #borrar este fichero que ya no hace falta
	    break 
	    ;;
	    
	    
	1) ################################ FILTRO POR LETRA INICIAL EN EL NOMBRE DEL MÓDULO ################################################
	
	      echo "\n $VERDE Ingrese una letra : $AZUL \c"; read LETRA
	      
	    until ! [ "$LETRA" = "" ] ;do  #si presiona enter vuelve a pedir letra
	      echo "\n $VERDE Ingrese una letra : $AZUL \c"; read LETRA
	    done  

	      LETRA=$(echo $LETRA | awk '{print tolower($0)}') #cambia letra a minusculas -toupper- para mayúsculas
	    
	    if [ -f $FICH_TMP ]; then rm -rf $FICH_TMP ; fi #vaciar el fichero o igual eliminarlo
	    
	    #### while en contraposición con for sí extrae la línea aunque hayan espacios
	    
	    while read line; do      # listo el fichero, filtro por primera letra y lo guardo en un nuevo fichero		
		if [ $(echo $line | cut -c 1) = $LETRA ] ;then echo $line >> $FICH_TMP ;else touch $FICH_TMP ;fi 
		#touch crea un fichero vacio
	    done < $FICH_MOD                                                        #$FICH_TMP se crea para contar líneas luego como '0'
	      
	      CUENTA_F=$(cat $FICH_TMP | wc -l)	      
	      
	     # if  (($CUENTA_F -ge 1)) ;then  #mayor o igual
	echo "$AZUL Actualmente existen $ROJO $CUENTA_F $AZUL módulos en la base de datos que comienzen por $ROJO $LETRA $ENDCOLOR "

		#while read line; do    #con while hay problemas al leer la línea 100  stty
		for line in $(cat $FICH_TMP); do     #CON FOR HAY PROBLEMAS CON LOS ESPACIOS y entonces utilizo _

			      PASO=$((PASO+1))   #CONTADOR=$(expr $CONTADOR + 1)    #let "CONTADOR += 1"
			      mod=$(echo  ${line%:*})  #quita el sufijo desde : 
			      descrip=$(echo ${line#*:}) #quita el prefijo desde :  nombre_modulo:descripción

			      bbusca=$(find . -type d -iname $mod) > $FICH_BUSCA_TMP
			      COUNT=$(cat $FICH_BUSCA_TMP | wc -l)
			      ###########~~~~~~~~~~~~~~~~~~~~~~~ con drush debería controlar si el modulo existe y que versión

				 #si count <> 0 FICH_BUSCA_TMP contiene al menos una busqueda encontrada, el modulo existe 
			   if ! [ $COUNT=0 ] ; then echo "$RED Ya existe un directorio con el nombre --> $mod <-- " ; else echo "" ; fi
	       
				 echo " $ROJO ($PASO) $VERDE    ¿Desea descargar y habilitar $ROJO $mod $VERDE? (s/S/y/Y)(0 para salir)" 
				 #~~~averiguar como hacer cuando una descarga tiene más de un modulo
				    descrip=$(echo $descrip | sed 's/_/ /g') #quita los guiones en descrip y los cambia por espacios
				 echo "Descripción: $YELLOW $descrip"
				 stty -echo ; read A; stty echo; printf '\n'; echo "$RED" #no muestra la respuesta del usuario por pantalla
	       
			   if [ "$A" = s ] || [ "$A" = S ] || [ "$A" = y ] || [ "$A" = Y ] || [ "$A" = "" ]; then 
			      
			      drush dl -y $mod 
				    if [ -f "$FICH_LMP_aux" ];then rm -fI "$FICH_LMP_aux" ; fi # si el fichero existe borrarlo
			      drush dl -p $mod >> $FICH_LMP_aux  # la cant de modulos que se descargaría se agrega a un fichero

					  for line in $(cat $FICH_LMP_aux); do  # se habilitan cada uno de los ficheros descargados
						   echo "\n $GRIS"
						   drush en -y $line
						   
					  done
			      
			   fi
		     echo "$GRIS ________________________________________________________________________________________________________\n"	
			   
			   salir=$A
			   if [ "$salir" = "0" ];then salir=null; break;fi
			   
		done   # < $FICH_TMP
		;;  
		
	    
	2) ####################################### LISTAR TODOS LOS MÓDULOS de 1 por 1  ######################################################

		  echo "\n $AZUL  Actualmente existen $ROJO $CUENTA $AZUL módulos en la base de datos $ENDCOLOR \n"

	    for line in $(cat $FICH_MOD); do      # este archivo contiene la lista de modulos 

		    PASO=$((PASO+1))   #CONTADOR=$(expr $CONTADOR + 1)    #let "CONTADOR += 1"
		    mod=$(echo  ${line%:*}) 
		    descrip=$(echo ${line#*:})
		    bbusca=$(find . -type d -iname $mod) > $FICH_BUSCA_TMP
		    COUNT=$(cat $FICH_BUSCA_TMP | wc -l)
		    ###########~~~~~~~~~~~~~~~~~~~~~~~ con drush debería controlar si el modulo existe y que versión

		      #si count <> 0 FICH_BUSCA_TMP contiene al menos una busqueda encontrada, el modulo existe 
		if ! [ $COUNT=0 ] ; then echo "$RED Ya existe un directorio con el nombre --> $mod <-- " ; else echo "" ; fi
    
			echo " $ROJO ($PASO) $VERDE    ¿Desea descargar y habilitar $ROJO $mod $VERDE? (s/S/y/Y) (0 para salir)" 
			#~~~averiguar como hacer cuando una descarga tiene más de un modulo
			  descrip=$(echo $descrip | sed 's/_/ /g') #quita los guiones en descrip y los cambia por espacios
			echo "Descripción: $YELLOW $descrip"
			stty -echo ; read A; stty echo; printf '\n'; echo "$RED" #no muestra la respuesta del usuario por pantalla
 
		if [ "$A" = s ] || [ "$A" = S ] || [ "$A" = y ] || [ "$A" = Y ] || [ "$A" = "" ]; then 
		
		  drush dl -y $mod 
			if [ -f "$FICH_LMP_aux" ];then rm -fI "$FICH_LMP_aux" ; fi # si el fichero existe borrarlo
		  drush dl -p $mod >> $FICH_LMP_aux  # la cant de modulos que se descargaría se agrega a un fichero

				for line in $(cat $FICH_LMP_aux); do  # se habilitan cada uno de los ficheros descargados
					echo "\n $GRIS"
					drush en -y $line
				done
				
		fi
	echo "$GRIS ________________________________________________________________________________________________________\n"	
			  
			  salir=$A
			  if [ "$salir" = "0" ];then salir=null; break;fi
	    done ;;
	    
	
	3) ########################################### FILTRO POR NOMBRE DEL MÓDULO ############################################################
	
	1      tot=0	      
	    until [ $tot -ge 4 ] ;do  #debe escribir por lo menos 4 caracteres y además no presionar enter
		    echo "\n $VERDE Ingrese nombre del módulo (+3ch) : $AZUL \c"; read NOM_MOD		    
	      	    tot=$(echo $NOM_MOD | wc -c) #wc cuenta un caracter mas por el salto de línea (entonces resto 1)
	      	    tot=$(($tot-1)) 	      	    
	    done  

		    NOM_MOD=$(echo $NOM_MOD | awk '{print tolower($0)}') #cambia NOM_MOD a minusculas
		  if [ -f $FICH_TMP ]; then rm -rf $FICH_TMP ; fi #vaciar el fichero o igual eliminarlo
	    
		    CUENTA_F=$(grep "$NOM_MOD" $FICH_MOD | wc -l)	       
		    echo "$AZUL Actualmente existen $ROJO $CUENTA_F $AZUL módulos en la base de datos que contengan la palabra $ROJO $NOM_MOD $ENDCOLOR "
		
	    for line in $(grep "$NOM_MOD" $FICH_MOD); do    ##debe leer solo el nombre del modulo y no la descripción

		    PASO=$((PASO+1))   #CONTADOR=$(expr $CONTADOR + 1)    #let "CONTADOR += 1"
		    mod=$(echo  ${line%:*}) 
		    descrip=$(echo ${line#*:})
		    bbusca=$(find . -type d -iname $mod) > $FICH_BUSCA_TMP
		    COUNT=$(cat $FICH_BUSCA_TMP | wc -l)
		    ###########~~~~~~~~~~~~~~~~~~~~~~~ con drush debería controlar si el modulo existe y que versión

		      #si count <> 0 FICH_BUSCA_TMP contiene al menos una busqueda encontrada, el modulo existe 
		  if ! [ $COUNT=0 ] ; then echo "$RED Ya existe un directorio con el nombre --> $mod <-- " ; else echo "" ; fi
    
			echo " $ROJO ($PASO) $VERDE    ¿Desea descargar y habilitar $ROJO $mod $VERDE? (s/S/y/Y) (0 volver al menu)" 
			
			  descrip=$(echo $descrip | sed 's/_/ /g') #quita los guiones en descrip y los cambia por espacios
			echo "Descripción: $YELLOW $descrip"
			stty -echo ; read A; stty echo; printf '\n'; echo "$RED" #no muestra la respuesta del usuario por pantalla
 
		  if [ "$A" = s ] || [ "$A" = S ] || [ "$A" = y ] || [ "$A" = Y ] || [ "$A" = "" ]; then 
		
		  drush dl -y $mod 
			if [ -f "$FICH_LMP_aux" ];then rm -fI "$FICH_LMP_aux" ; fi # si el fichero existe borrarlo
		  drush dl -p $mod >> $FICH_LMP_aux  # la cant de modulos que se descargaría se agrega a un fichero

				for line in $(cat $FICH_LMP_aux); do  # se habilitan cada uno de los ficheros descargados
					echo "\n $GRIS"
					drush en -y $line
				done
				
		  fi
		echo "$GRIS ________________________________________________________________________________________________________\n"	
	
		  salir=$A
		  if [ "$salir" = "0" ];then salir=null; break;fi
	
	    done ;;
	    
	    
	4) #################################### MOSTRAR LISTA de TODOS LOS MÓDULOS ##########################################################

		  echo "\n $AZUL  >>>>>  Actualmente existen $ROJO $CUENTA $AZUL módulos en la base de datos <<<<< $ENDCOLOR \n"
		numline=1
		  echo "$GRIS Para salir en cualquier momento presionar 0 (cero)\n "
				
		for line in $(cat $FICH_MOD); do    ##separa nombre del modulo y descripción utilando : como separador 

		    modulo=$(echo  ${line%:*})  #obtener el nombre del módulo que está antes de :
		    modulo=$(echo $modulo | awk '{print tolower($0)}') #pasar a minúsculas

		    descrip=$(echo ${line#*:}) #obtener la descripción que esta después de :
		      if [ -z $descrip ];then descrip="SIN_DESCRIPCIÓN";fi 
		      # agrega la nomenclatura SIN DESCRIPCIÓN a los módulos que no la tienen
		    descrip=$(echo $descrip | awk '{print tolower($0)}') #pasar a minusculas (mayúsculas -toupper-) toda la descripción
		    descrip=$(echo $descrip | sed -r 's/\<./\u&/')  #cambia a mayúsculas la primera letra de cada línea	
		      descrip=$(echo $descrip | sed 's/_/ /g') #quita los guiones en descrip y los cambia por espacios
		    echo "$RED $numline   $AZUL$modulo:  $YELLOW$descrip " 
		    stty -echo ; read p; stty echo; printf '\n' 
		    numline=$(($numline+1))
		    
		    if [ "$p" = "0" ];then break; fi
		    
		  
		done
	    ;;
	    
	
	*)
	    echo "$RED      >>>>>>>>>>>     Obción no válida    <<<<<<<<<<<  \n" ;;
   
      esac  #fin case
	echo "\n $LILA Presiona 'ENTER' para regresar al menú" 
	read key
	
done     #fin WHILE
 
