#!/bin/bash
###########################################
#  instalador de temas para drupal        #
#         -a través de drush -            #
#             Roque Perez                 #
#                 2013                    #
###########################################

# Bold
ENDCOLOR="\033[0m"	 
GRIS="\033[1;30m"	;	GRAY="\033[1;30m"
ROJO="\033[1;31m"	;	RED="\033[1;31m" 
VERDE="\033[1;32m"  	;	GREEN="\033[1;32m"
AMARILLO="\033[1;33m" 	;	YELLOW="\033[1;33m" 
AZUL="\033[1;34m"	;	BLUE="\033[1;35m" 
LILA="\033[1;35m"	;	PURPLE="\033[1;35m"
CELESTE="\033[1;36m"	;	CYAN="\033[1;36m"
BLANCO="\033[1;37m"	;	WHITE="\033[1;37m"
# Background
NEGRO2="\033[2;30m"	;	BLACK2="\033[2;30m" 
ROJO2="\033[2;31m"    	;	RED2="\033[2;31m" 
VERDE2="\033[2;32m"  	;	GREEN2="\033[2;31m" 	      
AMARILLO2="\033[2;33m" 	;	YELLOW2="\033[2;33m" 
AZUL2="\033[2;34m"	;	BLUE2="\033[2;34m"      
LILA2="\033[2;35m"	;	PURPLE2="\033[2;35m"
CELESTE2="\033[2;36m"	;	CYAN2="\033[2;36m"  
BLANCO2="\033[2;37m"	;	WHITE2="\033[2;37m"  
# Normal
NEGRO1="\033[0;[30m"	;	BLACK1="\033[0;30m" 
ROJO1="\033[0;[31m"    	;	RED1="\033[0;31m" 
VERDE1="\033[0;[32m"  	;	GREEN1="\033[0;32m" 	      
AMARILLO1="\033[0;[33m"	;	YELLOW1="\033[0;33m" 
AZUL1="\033[0;[34m"	;	BLUE1="\033[0;34m"      
LILA1="\033[0;[35m"	;	PURPLE1="\033[0;35m"
CELESTE1="\033[0;[36m"	;	CYAN1="\033[0;36m"  
BLANCO1="\033[0;[37m"	;	WHITE1="\033[0;37m"  

nom_scripts=$0
RUTA_SCRIPTS=$(dirname $0)
detectar_ruta=$(echo $RUTA_SCRIPTS | cut -c1)
    if [ "$detectar_ruta" != "/" ];then RUTA_SCRIPTS=$PWD/$RUTA_SCRIPTS;fi  #si se trata de ruta relativa, se llama al script desde una ruta relativa y se debe modificar la variable
RUTA_SCRIPTS_txt="$RUTA_SCRIPTS/drushsiTXT"     #donde los txt
BKPIFS=$IFS  #IFS=$'\x0A'$'\x0D'    #FINAL DE LINEA Y TABULADOR
FICH_LT="$RUTA_SCRIPTS_txt/lista_temas_drupal.txt"
#ficheros generados por el scripts
#FICH_LT_NEW="/$RUTA_SCRIPTS_txt/lista.nueva.temas.txt"
FICH_TMP="$RUTA_SCRIPTS_txt/temp.txt"
#FICH_LMP_aux="$RUTA_SCRIPTS_txt/lista_temas_principales_aux.txt"
FICH_BUSCA_TMP="$RUTA_SCRIPTS_txt/bbusca.txt"

CUENTA=$(cat $FICH_LT | wc -l)

func_InstTema() {   # usar FICHERO=$FICH_LMP_aux
   if [ "$A" = s ] || [ "$A" = S ] || [ "$A" = y ] || [ "$A" = Y ] || [ "$A" = "" ]; then 
      
      drush dl -y $tema 
	    if [ -f "$FICHERO" ];then rm -fI "$FICHERO" ; fi # si el fichero existe borrarlo
      drush en -y $tema
	 
	 echo "$VERDE ¿Deseas que $ROJO $tema $VERDE sea el tema por defecto? $AZUL \c ";read tf 
	 if [ "$tf" = s ] || [ "$tf" = S ] || [ "$tf" = y ] || [ "$tf" = Y ] || [ "$tf" = "" ]; then
	    drush vset -y theme_default $tema
	 fi
   fi
}
		

sort -o $FICH_LT -u $FICH_LT     #ordenar alfabéticamente la lista de temas --la opción u elimina filas duplicadas--

###############################################################################################################################################
####################################            MENÚ PARA LA INSTALACIÓN DE TEMAS              ################################################
###############################################################################################################################################


echo "\n $AZUL  Actualmente existen $ROJO $CUENTA $AZUL temas en la base de datos $ENDCOLOR \n" ;sleep 2
PASO=0
#~~~~~~~~~~~ PREGUNTAR SI DESEA INSTALAR UN tema ESPECIFICO
#FILTRO_LETRA=(cat $FICH_LT | cut -c 1 | grep $LETRA)
#hacer menu 1.quiere ver todos los temas 2. quieres ver por letra 3.quiere buscar

while [ OPCION != "0" ]  
do 
clear 
echo " $AZUL Elija una opción $VERDE \n" 
echo "  0    Salir" 
echo "  1    Mostrar temas filtrados por letra" 
echo "  2    Mostrar todos los temas" 
echo "  3    Buscar temas por criterio " 
echo "  4    Mostrar lista de temas \n"
read -p " ?  " OPCION

PASO=0

      case $OPCION in
	0)
	    echo ""
	    if [ -f $FICH_TMP ] ; then rm -RIf $FICH_TMP; fi   #borrar este fichero que ya no hace falta
	    break 
	    ;;
	    
	    
	1) ################################ FILTRO POR LETRA INICIAL EN EL NOMBRE DEL MÓDULO ################################################
	
	    echo "\n $VERDE Ingrese una letra : $AZUL \c"; read LETRA
	      
	    until ! [ "$LETRA" = "" ] ;do  #si presiona enter vuelve a pedir letra
	      echo "\n $VERDE Ingrese una letra : $AZUL \c"; read LETRA
	    done  

	    LETRA=$(echo $LETRA | awk '{print tolower($0)}') #cambia letra a minusculas -toupper- para mayúsculas
	    if [ -f $FICH_TMP ]; then rm -rf $FICH_TMP ; fi #vaciar el fichero o igual eliminarlo
	    	    
	    while read line; do      # listo el fichero, filtro por primera letra y lo guardo en un nuevo fichero		
		if [ $(echo $line | cut -c 1) = $LETRA ] ;then echo $line >> $FICH_TMP ;else touch $FICH_TMP ;fi 
		#touch crea un fichero vacio
	    done < $FICH_LT                                   #$FICH_TMP se crea para contar líneas luego como '0'
	      
	    CUENTA_F=$(cat $FICH_TMP | wc -l)	      
	    # if  (($CUENTA_F -ge 1)) ;then  #mayor o igual
	    echo "$AZUL Actualmente existen $ROJO $CUENTA_F $AZUL temas en la base de datos que comienzen por $ROJO $LETRA $ENDCOLOR "

		#while read line; do    #con while hay problemas al leer la línea 100  stty
		for tema in $(cat $FICH_TMP); do     #CON FOR HAY PROBLEMAS CON LOS ESPACIOS y entonces utilizo 

		     PASO=$((PASO+1))   #CONTADOR=$(expr $CONTADOR + 1)    #let "CONTADOR += 1"
		     bbusca=$(find . -type d -iname $tema) > $FICH_BUSCA_TMP
		     COUNT=$(cat $FICH_BUSCA_TMP | wc -l)

			   #si count <> 0 FICH_BUSCA_TMP contiene al menos una busqueda encontrada, el tema existe 
		     if ! [ $COUNT=0 ] ; then echo "$RED Ya existe un directorio con el nombre --> $tema <-- " ; else echo "" ; fi
	 
			   echo " $ROJO ($PASO) $VERDE    ¿Deseas descargar y habilitar $ROJO $tema $VERDE? (s/S/y/Y)(0 para salir)" 
			   stty -echo ; read A; stty echo; printf '\n'; echo "$RED" #no muestra la respuesta del usuario por pantalla
		  
		     FICHERO=$FICH_LMP_aux
		     func_InstTema
		     echo "$GRIS ________________________________________________________________________________________________________\n"	
			   
		     salir=$A
		     if [ "$salir" = "0" ];then salir=null; break;fi
			   
		done  
	;;  
		
	2) ####################################### LISTAR TODOS LOS temas de 1 por 1  ######################################################

		  echo "\n $AZUL  Actualmente existen $ROJO $CUENTA $AZUL temas en la base de datos $ENDCOLOR \n"

	    for tema in $(cat $FICH_LT); do      # este archivo contiene la lista de temas 

	       PASO=$((PASO+1))   #CONTADOR=$(expr $CONTADOR + 1)    #let "CONTADOR += 1
	       bbusca=$(find . -type d -iname $tema) > $FICH_BUSCA_TMP
	       COUNT=$(cat $FICH_BUSCA_TMP | wc -l) 
	       #si count <> 0 FICH_BUSCA_TMP contiene al menos una busqueda encontrada, el tema existe 
		if ! [ $COUNT=0 ] ; then echo "$RED Ya existe un directorio con el nombre --> $tema <-- " ; else echo "" ; fi
    
			echo " $ROJO ($PASO) $VERDE    ¿Desea descargar y habilitar $ROJO $tema $VERDE? (s/S/y/Y) (0 para salir)" 
			#~~~averiguar como hacer cuando una descarga tiene más de un tema
			  descrip=$(echo $descrip | sed 's/_/ /g') #quita los guiones en descrip y los cambia por espacios
			echo "Descripción: $YELLOW $descrip"
			stty -echo ; read A; stty echo; printf '\n'; echo "$RED" #no muestra la respuesta del usuario por pantalla
			FICHERO=$FICH_LMP_aux
			func_InstTema 
		  echo "$GRIS ________________________________________________________________________________________________________\n"	
			  
			  salir=$A
			  if [ "$salir" = "0" ];then salir=null; break;fi
	    done 
	 ;;
	    
	3) ########################################### FILTRO POR NOMBRE DEL MÓDULO ############################################################
	
	    tot=0	      
	    until [ $tot -ge 4 ] ;do  #debe escribir por lo menos 4 caracteres y además no presionar enter
		  echo "\n $VERDE Ingrese nombre del módulo (+3ch) : $AZUL \c"; read NOM_tema		    
		  tot=$(echo $NOM_tema | wc -c) # wc cuenta un caracter mas por el salto de línea (entonces resto 1)
		  tot=$(($tot-1)) 	      	    
	    done  

		  NOM_tema=$(echo $NOM_tema | awk '{print tolower($0)}') #cambia NOM_tema a minusculas
		  if [ -f $FICH_TMP ]; then rm -rf $FICH_TMP ; fi #vaciar el fichero o igual eliminarlo
		  CUENTA_F=$(grep "$NOM_tema" $FICH_LT | wc -l)	       
		  echo "$AZUL Actualmente existen $ROJO $CUENTA_F $AZUL temas en la base de datos que contengan la palabra $ROJO $NOM_tema $ENDCOLOR "
		
	    for tema in $(grep "$NOM_tema" $FICH_LT); do    ##debe leer solo el nombre del tema y no la descripción

		  PASO=$((PASO+1))   #CONTADOR=$(expr $CONTADOR + 1)    #let "CONTADOR += 1"
		  bbusca=$(find . -type d -iname $tema) > $FICH_BUSCA_TMP
		  COUNT=$(cat $FICH_BUSCA_TMP | wc -l)
		  #si count <> 0 FICH_BUSCA_TMP contiene al menos una busqueda encontrada, el tema existe 
		  if ! [ $COUNT=0 ] ; then echo "$RED Ya existe un directorio con el nombre --> $tema <-- " ; else echo "" ; fi
		  echo " $ROJO ($PASO) $VERDE    ¿Desea descargar y habilitar $ROJO $tema $VERDE? (s/S/y/Y) (0 volver al menu)" 
		  stty -echo ; read A; stty echo; printf '\n'; echo "$RED" #no muestra la respuesta del usuario por pantalla
 
		  FICHERO=$FICH_LMP_aux
		  func_InstTema
		echo "$GRIS ________________________________________________________________________________________________________\n"	
	
		  salir=$A
		  if [ "$salir" = "0" ];then salir=null; break;fi
	
	    done 
	;;
	    
	4) #################################### MOSTRAR LISTA de TODOS LOS temas ##########################################################

		echo "\n $AZUL  >>>>>  Actualmente existen $ROJO $CUENTA $AZUL temas en la base de datos <<<<< $ENDCOLOR \n"
		numline=1
		echo "$GRIS Para salir en cualquier momento presionar 0 (cero)\n "
				
		for tema in $(cat $FICH_LT); do    ##separa nombre del tema y descripción utilando : como separador 

		    tema=$(echo $tema | awk '{print tolower($0)}') #pasar a minúscula
		    echo "$RED $numline   $AZUL$tema" 
		    stty -echo ; read p; stty echo; printf '\n' 
		    numline=$(($numline+1))
		    
		    if [ "$p" = "0" ];then break; fi
		    
		done
	 ;;
	    
	*)
	    echo "$RED      >>>>>>>>>>>     Obción no válida    <<<<<<<<<<<  \n" ;;
   
      esac  #fin case
	echo "\n $LILA Presiona 'ENTER' para regresar al menú" 
	read key
	
done     #fin WHILE
 