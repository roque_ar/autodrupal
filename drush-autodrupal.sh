#! /bin/bash
VERS_SCRIPTS="versión 0.2.1 oct 2013"
# Bold
ENDCOLOR="\033[0m"	 
GRIS="\033[1;30m"	;	GRAY="\033[1;30m"
ROJO="\033[1;31m"	;	RED="\033[1;31m" 
VERDE="\033[1;32m"  	;	GREEN="\033[1;32m"
AMARILLO="\033[1;33m" 	;	YELLOW="\033[1;33m" 
AZUL="\033[1;34m"	;	BLUE="\033[1;35m" 
LILA="\033[1;35m"	;	PURPLE="\033[1;35m"
CELESTE="\033[1;36m"	;	CYAN="\033[1;36m"
BLANCO="\033[1;37m"	;	WHITE="\033[1;37m"
# Background
NEGRO2="\033[2;30m"	;	BLACK2="\033[2;30m" 
ROJO2="\033[2;31m"    	;	RED2="\033[2;31m" 
VERDE2="\033[2;32m"  	;	GREEN2="\033[2;31m" 	      
AMARILLO2="\033[2;33m" 	;	YELLOW2="\033[2;33m" 
AZUL2="\033[2;34m"	;	BLUE2="\033[2;34m"      
LILA2="\033[2;35m"	;	PURPLE2="\033[2;35m"
CELESTE2="\033[2;36m"	;	CYAN2="\033[2;36m"  
BLANCO2="\033[2;37m"	;	WHITE2="\033[2;37m"  
# Normal
NEGRO1="\033[0;30m"	;	BLACK1="\033[0;30m" 
ROJO1="\033[0;31m"    	;	RED1="\033[0;31m" 
VERDE1="\033[0;32m"  	;	GREEN1="\033[0;32m" 	      
AMARILLO1="\033[0;33m"	;	YELLOW1="\033[0;33m" 
AZUL1="\033[0;34m"	;	BLUE1="\033[0;34m"      
LILA1="\033[0;35m"	;	PURPLE1="\033[0;35m"
CELESTE1="\033[0;36m"	;	CYAN1="\033[0;36m"  
BLANCO1="\033[0;37m"	;	WHITE1="\033[0;37m"  



#echo "\n$GRIS Buscando Ruta web (DocumentRoot), se paciente ...\c "
#si encontrara mas una ruta habría un error
#RUTA_WEB=$(find /etc -type f -exec grep DocumentRoot {} \; | uniq |sed 's/[ \t]*//' | grep ^DocumentRoot | sed 's/DocumentRoot //' | uniq)

# QUIZÁS TENGAS QUE MODIFICAR
RUTA_scripts=$0
RUTA_SCRIPTS=$(dirname $0)      #DIRECTORIO donde se encuentra el script drush6siv2.sh
detectar_ruta=$(echo $RUTA_SCRIPTS | cut -c1)
    if [ "$detectar_ruta" != "/" ];then RUTA_SCRIPTS=$PWD/$RUTA_SCRIPTS;fi  #si se trata de ruta relativa, se llama al script desde una ruta relativa y se debe modificar la variable
RUTA_WEB="/var/www/vhosts"
locale=es_ES  # indica el idioma de instalación de drupal

RUTA_SCRIPTS_txt="$RUTA_SCRIPTS/drushsiTXT"   # directorio para guardar todos los archivos txt necesarios
SCRIPT_DRUSH_MODULOS="$RUTA_SCRIPTS/drush_modulos.sh"  #ruta al scripts drush_modulos.sh
SCRIPT_DRUSH_TEMAS="$RUTA_SCRIPTS/drush_temas.sh"
ARCH_VERS_DRUPAL="CHANGELOG.txt"
FICH_PUBL="sites/default/files"
DIR_PROFILES="profiles"
DIR_TRANSLATIONS="translations"
DIR_LIBRARY="$RUTA_SCRIPTS/drushsiTXT/libraries" # directorio que contiene las librerías
NOM_DISTRO=0   # si no se elije una distro se mantiene este valor

#ficheros txt creados previamente con datos para ser leídos por el script
FICH_LMP="$RUTA_SCRIPTS_txt/lista_modulos_principales.txt"
FICH_LMP_aux="$RUTA_SCRIPTS_txt/lista_modulos_principales_aux.txt"
FICH_DISTROS="$RUTA_SCRIPTS_txt/lista_distros_drupal.txt"
FICH_LT="$RUTA_SCRIPTS_txt/lista_temas_drupal.txt"
FICH_MOD="$RUTA_SCRIPTS_txt/lista_modulos.txt"
#ficheros generados en la ejecución del scripts
FICH_PROFIL="$RUTA_SCRIPTS_txt/lista_profiles.txt"
FICH_DATOS_INST="$RUTA_SCRIPTS_txt/datos-instación.txt" #~~~~~se guardarán todos los datos de la instalación (log)
FICH_FIND="$RUTA_SCRIPTS_txt/find.txt"
FICH_LIBRARIES="$RUTA_SCRIPTS_txt/lista_libraries.txt"

BD_PLESK="psa"   # base de datos que genera plesk al instalarse
COMPROBAR_DOM="RELAC_DOM" 		
DIR_EXCL_PLESK="plesk-stat"
#Variables de uso puntual
grupo="psacln"
admin_linux="root" 
distro_elegida=ko    #indica si se ha elegido una distro o no
PLESK_INST=ko  # se asume que plesk no está insladado

inicio_ns=`date +%s%N`   
inicio=`date +%s`
date=`date -I`
diaactual=`date +%a`

#echo "$GRIS\$RUTA_WEB:$RUTA_WEB, \n\$RUTA_scripts:$RUTA_scripts, \n\$RUTA_SCRIPTS:$RUTA_SCRIPTS, \n\$RUTA_SCRIPTS_txt:$RUTA_SCRIPTS_txt, \n\$SCRIPT_DRUSH_MODULOS:$SCRIPT_DRUSH_MODULOS, \n\$SCRIPT_DRUSH_TEMAS:$SCRIPT_DRUSH_TEMAS, \n\$DIR_LIBRARY:$DIR_LIBRARY, \n\$FICH_LMP:$FICH_LMP, \n\$FICH_LMP_aux:$FICH_LMP_aux, \n\$FICH_DISTROS:$FICH_DISTROS, \n\$FICH_LT:$FICH_LT, \n\$FICH_MOD:$FICH_MOD, \n\$FICH_DATOS_INST:$FICH_DATOS_INST, \n\$FICH_FIND:$FICH_FIND, \n\$FICH_LIBRARIES:$FICH_LIBRARIES"; read keys
echo "\n$GRIS Haciendo comprobaciones en el equipo, se paciente"

echo "$YELLOW"
echo "###############################################################################################################################"
echo "#####	autor. ROQUE PEREZ											  	#######"
echo "#####		INSTALACION DE DRUPAL UTILIZANDO DRUSH	para linux						  	#######"
echo "#####															#######"
echo "#####   *REQUISITOS (probado con ..)			<<< NO ESTÁ PROBADO CON OTRAS VERSIONES >>>		  	#######"
echo "#####		*PLESK 11 (no necesario)		<<< y seguramente funcione bien	en muchas >>>		  	#######"
echo "#####		*>=DRUSH 5.9 												#######"
echo "#####		*>=MYSQL 5			sirve para instalar drupal versión 7 y version 6		  	#######"
echo "#####		*BASH 4.1				para drupal 8 se requiere >= drush 6			  	#######"
echo "#####		*>=Ubuntu 10.04											  	#######"
echo "#####										Licencia GPLv3			  	#######"
echo "###############################################################################################################################"
echo "$YELLOW"

# Mejoras pendientes
#	al detectar requerimientos minimos, si estos no se cumplen no permitir la instalación de la versión de drupal en cuestion
#     	detectar versión de drush instalada y permitir o no drupal-8 o mostrar advertencia
#     	completar y revisar instalación de distribuciones drupal
#     	crear dominios y subdominios a través del script
#     	soporte para cpanel y virtualmin (webmin)
#     	funcionamiento sin ningún panel de administracion web
#     	deberá guardar la fecha, la hora, la ip de conexión por ssh, versión drupal, errores, etc (log)
#     	al cancelar una instalación los directorios deben quedar como estaban antes de comenzar el script
#     	al borrar una instalación existente debería preguntar si borrar la bd y el usuario de la bd
#     	integrar drush make
#	Comprobar otros servidores web no sólo apache2
#	Comprobar requisitos para el script e idicarlos si falta alguno
#	Detectar automáticamente web root porej. /var/www
#       capturar errores 2> mi_log.err 
#       soporte para postgresql
#       hacer una comprobación de que todos los archivos txt estén en su lugar
#	mejorar la instación de librerías
#	al ingresar manualmente NOM_DOM comprobar su existencia
#	solo se debería ejecutar como root
#	sin plesk group psacln no existe, chequear group para los permisos- se chequea por www-data pero en el caso de otro servidor web que grupo sería el correcto?


#errores
#	cuando es perfil personalizado no verifica la existencia de la bd
#	si no existe un dominio no se puede optener url, ni propietario, ni e-mail por defecto (ni dominio, claro)

#requerimientos: 
   # drupal 6 			# drupal 7				# drupal 8
   # web_service
   # mysql 4.1			mysql 5.0.15				mysql 5.0.15
   # 	postgresql 7.1		    postgresql 8.3			  postgresql 8.3
   #				    sqlite 3.3.7			  sqlite 3.3.7
   # php 4.4			php 5.2.5				php 5.3.10

#########################################################################################################################################
###########################################           COMPROBAR APP INSTALADAS EN EL SO       ###########################################
#########################################################################################################################################

 echo "\n$GRIS Comprobando los requisitos para ejecutar este script ... $VERDE\n"

      cancelar=ko
   for app in drush mysql psa php bash; do
      # echo "\n\$app vale $app"
      whereis $app | grep \/ >/dev/null;var=$? 
      if [ $app = psa ];then
	 if [ $var = 1 ];then
	    echo "$GRIS No se ha detectado un servicio psa (plesk) funcionando"
	    PLESK_INST=ko  #plesk no instalado = ko 
	 else
	    PLESK_INST=ok # plesk esta presente
	 fi
      else 
	 if [ $var = 1 ];then
	    echo "$RED No se ha detectado ninguna versión de $app, y $app es imprescindible para ejecutar este script"
	    if [ $app = drush ];then
	       echo "$VERDE Mas información en www.$app.org \c"
	    elif [ $app = bash ];then
	       echo "$VERDE Mas información en http://www.gnu.org/software/$app/ \c"
	    elif [ $app = php ];then
	       echo "$VERDE Mas información en www.$app.net \c"
	    else
	       echo "$VERDE Mas información en www.$app.com \c"
	    fi
	    echo "\n"
	    cancelar=ok   #se cancelará el script
	 fi
      fi
   done

   if [ $cancelar = ok ];then exit 0;fi

func_VersDrush() {
      cd /;a=$(drush --version | grep 5); if [ "$a" = "" ];then a=$(drush --version | grep '^drush'); b=$(echo ${a#*:});else a=$(drush --version | 	grep '^drush'); b=$(echo ${a##*\ });fi;echo $b
}

if [ $PLESK_INST = "ok" ];then
   updatedb 
   locate_vers_psa=$(locate core.version)
   echo "$VERDE versión de Plesk: 	$CELESTE	$(cat $locate_vers_psa)"
else  # si plesk no esta instalado, buscar ruta web generalmente (/var/www) 
   echo "\n$GRIS Buscando Ruta web (DocumentRoot), se paciente ...\n\n "
   #si encontrara mas una ruta habría un error
   RUTA_WEB=$(find /etc -type f -exec grep DocumentRoot {} \; | uniq |sed 's/[ \t]*//' | grep ^DocumentRoot | sed 's/DocumentRoot //' | uniq)
   echo "$VERDE ruta web: 	$CELESTE		$RUTA_WEB"
   
      existe_wwwdata=$(cat /etc/group | grep www-data | cut -d: -f1)
      if [ "$existe_wwwdata" = "" ];then
	 echo "\n$RED    no se encuentra el grupo de usuarios $AZUL www-data"
	 echo "$RED     Al instalar apache2, este grupo se crea automáticamente, verifica la instalación de apache2"
	 echo "\n$VERDE    Aún así deseas continuar? $AZUL \c ";read p
	 
	 if [ "$p" = y ] || [ "$p" = s ] || [ "$p" = S ] || [ "$p" = Y ];then
	    echo "\n\n"
	    grupo="www-data"
	 else
	    exit 0
	 fi
      else
	 grupo="www-data" 
      fi
fi

echo "$VERDE versión de MySQL: 	$CELESTE	$(mysql --version)"
echo "$VERDE versión de PHP:   	$CELESTE	$(a=$(php --version);b=$(echo ${a%%\-*});echo $b)"						
echo "$VERDE versión de Drush: 	$CELESTE	$(drush --version)"   # Drush $(func_VersDrush)""			
echo "$VERDE versión de Bash: 	$CELESTE	$(a=$(bash --version);b=$(echo ${a%%\-*});echo $b)"
echo "$VERDE versión de kernel:	$CELESTE	$(uname -ri)" 											
echo "$VERDE versión de Ubuntu: $CELESTE		$(lsb_release -d)"									
echo "$VERDE versión de Apache: $CELESTE		$(apache2 -v | head -1)"
echo "$VERDE nombre del scripts:$CELESTE		$RUTA_SCRIPTS  $VERS_SCRIPTS"


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@			FUNCIONES		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

func_Mostrar4Columnas() {   # llamar a la funcion:   lee_fich="ficherocualquiera.txt" ;func_Mostrar4Columnas
   #exec >> output.txt
   cant=$(cat $lee_fich | wc -l)
   count=0 # Initialize a counter for columns
   countx4=0

      while read line; do

	 printf "\t%-18s\t%s" "$line"
	 count=$(( $count + 1 ))
	 countx4=$(( $countx4 + 1 ))
	 if [ $countx4 = 4 ];then echo "\r";countx4=0 ;fi  #volver a contar nuevamente hasta 4
	 if [ $count = $cant ];then echo "\r";fi   #ultima pasada 	
	    
      done < $lee_fich
}

func_BorraInstPrevia() {			
	    if [ -d "$DIR_FIND"/sites ] ;then rm -RIf "$DIR_FIND"/sites ; fi
	    if [ -d "$DIR_FIND"/misc ] ;then rm -RIf "$DIR_FIND"/misc ; fi
	    if [ -d "$DIR_FIND"/scripts ] ;then rm -RIf "$DIR_FIND"/scripts ; fi	    
	    if [ -d "$DIR_FIND"/modules ] ;then rm -RIf "$DIR_FIND"/modules ; fi
	    if [ -d "$DIR_FIND"/includes ] ;then rm -RIf "$DIR_FIND"/includes ; fi
	    if [ -d "$DIR_FIND"/profiles ] ;then rm -RIf "$DIR_FIND"/profiles ; fi
	    if [ -d "$DIR_FIND"/themes ] ;then rm -RIf "$DIR_FIND"/themes ; fi
	    if [ -d "$DIR_FIND"/core ] ;then rm -RIf "$DIR_FIND"/core ; fi   #directorio de drupal8
	    if [ -f "$DIR_FIND"/web.config ] ;then rm -RIf "$DIR_FIND"/web.config ; fi
	   # find "$DIR_FIND" -maxdepth 1 -iname "drupal*" -exec rm -RIf {} \;
	    find "$DIR_FIND" -maxdepth 1 -type f -iname "*.txt" -exec rm -RIf {} \;
	    find "$DIR_FIND" -maxdepth 1 -type f -iname "*.html" -exec rm -RIf {} \;
	    find "$DIR_FIND" -maxdepth 1 -type f -iname "*.php" -exec rm -RIf {} \;
	    find "$DIR_FIND" -maxdepth 1 -type f -iname "composer*" -exec rm -RIf {} \;
	    find "$DIR_FIND" -maxdepth 1 -type f -iname ".gitignore" -exec rm -RIf {} \;
	    find "$DIR_FIND" -maxdepth 1 -type f -iname ".htaccess" -exec rm -RIf {} \;
	    find "$DIR_FIND" -maxdepth 1 -type f -iname ".jshintignore" -exec rm -RIf {} \;
	    find "$DIR_FIND" -maxdepth 1 -type f -iname ".editorconfig" -exec rm -RIf {} \;
	    find "$DIR_FIND" -maxdepth 1 -type f -iname ".gitattributes" -exec rm -RIf {} \;
	    find "$DIR_FIND" -maxdepth 1 -type f -iname ".jshintrc" -exec rm -RIf {} \;
	    #rm \.*[^.] #borrar todos los archivos ocultos pero según cada config. quizá algunos no se deberían borrar
	    dir_ok=0 # salir del bucle
}

func_PerfilPorVersion() {
      case $vers_princ_drupal in
	 6)
	    perfil_por_defecto="default"   ;;
	 7)
	    perfil_por_defecto="standard"  ;;
	 8)                                      # no se sabe cual será el perfil por defecto de drupal8
	    perfil_por_defecto="standard"  ;;
      esac  
}

func_DatosAuto() { 
	 # El directorio de instalación puede estar en mayúsculas, convertir a minúsculas y quitar espacios
	 echo "$VERDE Ingrese el nombre del sitio web : $AZUL \c "; read SITE_NAME

	    if [ "$SITE_NAME" = "" ];then 
	       SITE_NAME=$(echo $DIR_INSTALL | tr "[:upper:]" "[:lower:]" | sed "s/[ ][ ]*/_/g")
	    else
	       SITE_NAME=$(echo $SITE_NAME | tr "[:upper:]" "[:lower:]" | sed "s/[ ][ ]*/_/g")
	    fi    
	    
	 NAME_DB_aux=$(expr substr "$SITE_NAME" 1 15)     #trunca a 15 caracteres porque mysql soporta hasta 16 en el nombre de la db y el usuario 
	 NAME_DB=$(echo "$NAME_DB_aux" | sed "s/[ ][ ]*/_/g" | sed "s/\./_/g" ) #reemplazo punto por _ si los hubieran
	 USER_DB=$NAME_DB # al ser instalación automática poner usuario password y nombre de la bd igual 
	 PASS_DB=$NAME_DB 
	 USER_DRUPAL="admin"
	 PASS_DRUPAL="admin" 
	 MAIL_SITE_NAME=$(echo "$SITE_NAME@$NOM_DOM")
	 MAIL_USER_DRUPAL=$MAIL_SITE_NAME
	 SITE_NAME=$(echo $SITE_NAME | tr "[:lower:]" "[:upper:]")
}	

func_DatosPerfil() {	 
     #si ya se ha elegido distro y además el perfil de la distro es incorrecto se debe preguntar por el perfil
     if [ "$distro_elegida" = ok ] && [ "$perfil_distro" = ok ];then  
	 PERFIL="$NOM_DISTRO"
     else
	       echo "\n$VERDE"
	       if [ -d "$FICH_PROFIL" ] ;then rm -RIf "$FICH_PROFIL" ; fi #si el fichero existe borrarlo
	       ls "$DIR_FIND"/profiles > $FICH_PROFIL # obtener los nombres de los perfiles

	       if [ "$distro_elegida" = ok ];then
		  echo "$VERDE elija el perfil de la distribución $AZUL$NOM_DISTRO $VERDE:$AZUL \c ";read PERFIL
	       else
		  echo "$VERDE perfil de instalación $AZUL 'enter=standard' $VERDE:$AZUL \c ";read PERFIL   
	       fi
	       
	       CUENTA=$(cat $FICH_PROFIL | wc -l)  #contar la cant de lineas en el fichero
	       sal=ko
	    while [ "$sal" = "ko" ]; do
	    
		  #por cada línea verifica el perfil optenido si existe en el perfil de instalación
	       for linea in $(cat $FICH_PROFIL) ; do
		     if [ "$PERFIL" = "" ];then PERFIL="$perfil_por_defecto";sal=ok;break;else sal=ko; fi #enter al elegir perfil sale de los bucles
		     if [ "$linea" = "$PERFIL" ];then sal=ok; break ;else sal=ko;fi 
	       done #fin for	      
		  
		  if [ "$sal" = "ko" ];then   # quiere decir que el nombre del perfil ingresado no es correcto
		  clear; echo "\n $RED Perfil incorrecto, elija uno de los siguientes perfiles \n $YELLOW"
		  #cat "$FICH_PROFIL"
		  lee_fich=$FICH_PROFIL
		  func_Mostrar4Columnas
		  echo "\n $VERDE          Ingrese perfil de instalación: $AZUL\c "; read PERFIL
		  fi

	    done #fin while
     fi
}

func_DatosDrupal() {
clear    
      echo "$LILA nombre de usuario administrador de drupal $AZUL (enter=admin)$LILA:$AZUL \c  "; read USER_DRUPAL
		  if [ "$USER_DRUPAL" = "" ];then USER_DRUPAL="admin"; fi
	    echo "${USER_DRUPAL}" |  grep '^[a-z][a-z0-9_]*[a-z]$'  >>/dev/null
	    var=$?
	    
	 until [ $var = 0 ] ; do #nombre incorrecto no corresponde con el patron   
	    echo "$RED Ingresar un nombre válido $LILA \n"
	    echo "$LILA nombre de usuario administrador de drupal $AZUL (enter=admin)$LILA:$AZUL \c  "; read USER_DRUPAL
		  if [ "$USER_DRUPAL" = "" ];then USER_DRUPAL="admin"; fi
	    echo "${USER_DRUPAL}" |  grep '^[a-z][a-z0-9_]*[a-z]$'  >>/dev/null
	    var=$?
	 done # si USER_DRUPAL tiene mas de 15 caracteres entonces acortar
###      
      echo "$LILA password de administrador de drupal :$AZUL \c  "; read PASS_DRUPAL
		  if [ "$PASS_DRUPAL" = "" ];then PASS_DRUPAL="admin"; fi
	    echo "${PASS_DRUPAL}" |  grep '^[a-zA-Z]\+[a-zA-Z0-9._-+]\+[a-zA-Z0-9._-+]*[a-zA-Z+-]$' >>/dev/null
	    var=$?
	    
	 until [ $var = 0 ] ; do #nombre incorrecto no corresponde con el patron   
	    echo "$RED Ingresar un password válido \n "
	    echo "$LILA password de administrador de drupal :$AZUL \c  "; read PASS_DRUPAL
		  if [ "$PASS_DRUPAL" = "" ];then PASS_DRUPAL="admin"; fi
	    echo "${PASS_DRUPAL}" |  grep '^[a-zA-Z]\+[a-zA-Z0-9._-+]\+[a-zA-Z0-9._-+]*[a-zA-Z+-]$' >>/dev/null
	    var=$?
	 done
	 
###
      echo "$LILA e-mail de administrador de drupal :$AZUL \c  "; read MAIL_USER_DRUPAL    
		  if [ "$MAIL_USER_DRUPAL" = "" ];then MAIL_USER_DRUPAL="sumasoluciones@gmail.com"; fi
		  
	    echo "${MAIL_USER_DRUPAL}" |  grep '^[a-z][a-z0-9._-]\+@[a-z][a-z0-9]\+\.[a-z][a-z0-9.]*[a-z]$' >>/dev/null
	    var=$?

	 until [ $var = 0 ] ; do #el comando grep fue incorrecto la dirección de email no corresponde con el patron   
	    echo "$RED Ingresar un email válido \n"
	    echo "$LILA e-mail de administrador de drupal :$AZUL \c  "; read MAIL_USER_DRUPAL	
	    echo "${MAIL_USER_DRUPAL}" |  grep '^[a-z][a-z0-9._-]\+@[a-z][a-z0-9]\+\.[a-z][a-z0-9.]*[a-z]$' >>/dev/null
	    var=$?	  
	 done				
}

func_DatosSitio() {
clear
	 echo "$BLANCO nombre del sitio web (max 30caract) :$AZUL \c  ";read SITE_NAME
		  # nom del sitio queda en mayúsculas
		  if [ "$SITE_NAME" = "" ];then 
		     SITE_NAME=$(echo $DIR_INSTALL | tr "[:lower:]" "[:upper:]")
		  else 
		     SITE_NAME=$(echo $SITE_NAME | tr "[:lower:]" "[:upper:]")
		  fi	
	 SITE_NAME=$(expr substr "$SITE_NAME" 1 30)	  #limitar el tamaño del nombre del sitio
###	 # Uso SITE_NAME_aux para no modificar SITE_NAME que ya lo tengo
	 SITE_NAME_aux=$(echo $SITE_NAME | sed "s/ /_/g" | tr "[:upper:]" "[:lower:]") #quitar espacios y pasar a minúsculas

	 echo "$BLANCO e-mail del sitio web :$AZUL \c  "; read MAIL_SITE_NAME      
		  if [ "$MAIL_SITE_NAME" = "" ];then  ##al presionar enter toma el nombre de sitio como email
		     MAIL_SITE_NAME="$SITE_NAME_aux@$NOM_DOM"
		  fi 
		  
	 echo "${MAIL_SITE_NAME}" | grep '^[a-z][a-z0-9._-]\+@[a-z][a-z0-9]\+\.[a-z][a-z0-9.]*[a-z]$' >>/dev/null
	    var=$?

	 until [ $var = 0 ] ; do #el comando grep fue incorrecto la dirección de email no corresponde con el patron
      
	    echo "$RED Ingresar un email válido $BLANCO"
	    echo "$BLANCO e-mail del sitio web :$AZUL \c  "; read MAIL_SITE_NAME	#creo que drupal no soporta email con _ . -
	    echo "${MAIL_SITE_NAME}" |  grep '^[a-z][a-z0-9._-]\+@[a-z][a-z0-9_]\+\.[a-z][a-z0-9.]*[a-z]$'  >>/dev/null 
	    var=$?
	    
	 done	
}

func_DatosBd() {
   clear
	 #optener un nombre por defecto para la bd basado en SITE_NAME_aux, quitar puntos y limitar a 15 caracteres  
	 echo "$CELESTE   nombre de base de datos $AZUL (enter=$SITE_NAME_aux) $CELESTE:$AZUL \c		  ";read NAME_DB
	       if [ "$NAME_DB" = "" ];then 	 # si presiona enter
		  NAME_DB_aux=$(expr substr "$SITE_NAME_aux" 1 15)
	       else
		  NAME_DB_aux=$(expr substr "$NAME_DB" 1 15)
	       fi
	 NAME_DB=$(echo "$NAME_DB_aux" | sed "s/[ ][ ]*/_/g" | sed "s/\./_/g" ) #reemplazo punto por _ si los hubieran
###
	 echo "$CELESTE   nombre de usuario de la base de datos $AZUL(enter=$NAME_DB)$CELESTE:$AZUL \c		  "; read USER_DB
	       if [ "$USER_DB" = "" ];then USER_DB="$NAME_DB"; fi
	 echo "${USER_DB}" |  grep '^[a-z][a-z0-9_]*[a-z0-9]$'  >>/dev/null
	 var=$?
	 
	 until [ $var = 0 ] ; do   #nombre incorrecto no corresponde con el patron   
	    echo "$RED Ingresar un nombre válido $CELESTE \n"
	    echo "$CELESTE   nombre de usuario de la base de datos $AZUL(enter=$NAME_DB)$CELESTE:$AZUL \c  "; read USER_DB
		  if [ "$USER_DB" = "" ];then USER_DB="$NAME_DB"; fi
	    echo "${USER_DB}" |  grep '^[a-z][a-z0-9_]*[a-z0-9]$'  >>/dev/null
	    var=$?
	 done # si USER_DRUPAL tiene mas de 15 caracteres entonces acortar
	 USER_DB=$(expr substr "$USER_DB" 1 15) 
###	
	    echo "$CELESTE   password de base de datos $AZUL(enter=$USER_DB)$CELESTE:$AZUL \c		  ";read PASS_DB
		  if [ "$PASS_DB" = "" ];then PASS_DB="$USER_DB"; fi
	    echo "${PASS_DB}" |  grep '^[a-zA-Z]\+[a-zA-Z0-9._-+]' >>/dev/null
	    var=$?
	 
	 until [ $var = 0 ] ; do #nombre incorrecto no corresponde con el patron   
	    echo "$RED Ingresar un password válido \n "
	    echo "$CELESTE   password de base de datos del sitio web :$AZUL \c  "; read PASS_DB
		  if [ "$PASS_DB" = "" ];then PASS_DB="$USER_DB"; fi
	    echo "${PASS_DB}" |  grep '^[a-zA-Z]\+[a-zA-Z0-9._-+]{15}' >>/dev/null
	    var=$?
	 done
				 
   echo "\n $GRIS"
}

func_userdb_exist() {     # comprueba si el usuario de la bd existe
      userdb=0  #userdb vale 0 si encuentra una coincidencia de usuario userdb vale 1 y salir del bucle 
   for userdb in $(mysql -u$U_MYSQL -p$P_MYSQL -sN -e "SELECT user FROM mysql.user" | grep -iw $USER_DB);do 
      userdb=1
      if [ $userdb=1 ];then break;fi
   done
      
   if [ "$userdb" = "1" ];then 
      echo "\n$RED El usuario $AZUL $USER_DB $RED ya existe ten en cuenta que se cambiará la contraseña ¿Quieres continuar? $AZUL \c ";read borraudb
      echo "\n$GRIS 		El usuario $AZUL $USER_DB $GRIS pertenece a la/s siguiente/s bd/s"
	    echo "$GRIS __________________________________________________________________________"#comprobar las bd relacionadas con el usuario
	    mysql -u$U_MYSQL -p$P_MYSQL -sN -e "SHOW GRANTS FOR $USER_DB"
	    echo "$GRIS __________________________________________________________________________\n"
      if [ "$borraudb" = y ] || [ "$borraudb" = s ] || [ "$borraudb" = S ] || [ "$borraudb" = Y ] || [ "$borraudb" = "" ];then
	 mysql -u$U_MYSQL -p$P_MYSQL -sN -e "GRANT ALL PRIVILEGES ON $NAME_DB.* TO $USER_DB@'%' IDENTIFIED BY '$PASS_DB'" #asinar usuario a la db
      else
	NAME_DB_aux=$NAME_DB
	USER_DB_aux=$USER_DB
	func_DatosBd #pedir nuevamente los datos para db, usuario y password
      fi
   else
      mysql -u$U_MYSQL -p$P_MYSQL -sN -e "GRANT ALL PRIVILEGES ON $NAME_DB.* TO $USER_DB@'%' IDENTIFIED BY '$PASS_DB'" #asinar usuario a la db
      bdaceptada=ok #ya se preguntará por la db
   fi
}

func_BdExist() {
      cont=0            
      for DB in $(mysql -Bse "show databases" -u$U_MYSQL -p$P_MYSQL) ; do   

	 if [ "$DB" = "$NAME_DB" ];then 
	    echo "\n $RED La base de datos $NAME_DB ya existe, puede optar por eliminarla o por renombrarla\n"
	    echo "$VERDE Para eliminar la base de datos $AZUL $NAME_DB $VERDE escriba $RED (ELIMINAR) $VERDE o presione 'cualquier otra tecla' para renombrarla : $AZUL \c" ; read r
   
	       if ! [ "$r" = "ELIMINAR" ] ; then    #si no elimina la bd se le asigna un nuevo nombre automáticamente
		  NAME_DB1=$(expr substr "$NAME_DB" 1 15)    #TRUNCO A 14 CARACTERES para agregar un caracter mas luego
		  NAME_DB="$NAME_DB1""$cont"
		  USER_DB=$NAME_DB         # al ser instalación automática poner usuario password y nombre de la bd igual 
		  PASS_DB=$NAME_DB
		  echo "$LILA \n Tanto el nombre de la base de datos como el usuario de dicha base de datos será: $CELESTE  $NAME_DB \n\n $GRIS"  
	       fi   
	 fi
      
      done
} 

func_CrearbdDrupal8() {
   clear
   bdaceptada="ko";borradb="n" 
   while [ "$bdaceptada" = ko ];do
   
      db=0;for db in $(mysql -Bse "show databases" -u$U_MYSQL -p$P_MYSQL | grep -iw $NAME_DB);do echo "";done #verificar si la bd existe
   
      if [ $db = "$NAME_DB" ];then #respecto a la bd
      
	 echo "$ROJO La base de datos $AZUL $NAME_DB $ROJO existe $GRIS  (0 cancela instalación)"
	 echo "Si continúas será eliminada, ¿Deseas continuar? (y/Y/s/S): $AZUL \c ";read borradb
   
	    if [ "$borradb" = y ] || [ "$borradb" = s ] || [ "$borradb" = S ] || [ "$borradb" = Y ];then
		  func_userdb_exist #verificar usuario db antes de borrar bd
		  #borrar y crear db si se ha optado por borrar también usuario
		  if [ "$borraudb" = y ] || [ "$borraudb" = s ] || [ "$borraudb" = S ] || [ "$borraudb" = Y ] || [ "$borraudb" = "" ];then
		     mysql -u$U_MYSQL -p$P_MYSQL -sN -e "DROP DATABASE $NAME_DB"
		     mysql -u$U_MYSQL -p$P_MYSQL -sN -e "CREATE DATABASE $NAME_DB"
		     bdaceptada=ok
		  else
		     echo "No se ha borrado db $NAME_DB_aux porque tampoco se ha borrado usuariodb $USER_DB_aux"
		  fi
		  
	    else
		  if [ "$r" = 0 ];then exit 0;fi 
		  # No borrar la bd entonces a traves de la funcion se vuelven a preguntar los datos para la bd
		  func_DatosBd   # bdaceptada sigue con valor ko, seguimos en el bucle
	    fi
      else
		  #crear bd
		  mysql -u$U_MYSQL -p$P_MYSQL -sN -e "CREATE DATABASE $NAME_DB"
		  func_userdb_exist
      fi
   done
}

func_Drush_si() {

	 echo "drush -y si $PERFIL_FINAL --account-name=$USER_DRUPAL --account-pass=$PASS_DRUPAL --db-url=mysql://$USER_DB:$PASS_DB@localhost/$NAME_DB --site-name="$SITE_NAME" --db-su=$U_MYSQL --db-su-pw=$P_MYSQL --locale=$locale --account-mail=$MAIL_USER_DRUPAL --site-mail=$MAIL_SITE_NAME \n"
	
	 drush -y si $PERFIL_FINAL --account-name=$USER_DRUPAL --account-pass=$PASS_DRUPAL --db-url=mysql://$USER_DB:$PASS_DB@localhost/$NAME_DB --site-name="$SITE_NAME" --db-su=$U_MYSQL --db-su-pw=$P_MYSQL --locale=$locale --account-mail=$MAIL_USER_DRUPAL --site-mail=$MAIL_SITE_NAME
	 val_drush=$?;if [ "$val_drush" = 0 ]; then echo "\n $VERDE Instalación correcta"; else "\n $RED Algo ha salido mal";exit 1 ; fi
}

func_PreparaIdioma() {
   case $vers_princ_drupal in #dependiendo de la versión de drupal es donde se va a colocar el idioma
	 6|7)      
	    if [ -d profiles/$PERFIL_FINAL ];then echo "";else mkdir profiles/$PERFIL_FINAL;fi
	    if [ -d profiles/$PERFIL_FINAL/translations ];then echo "";else mkdir profiles/$PERFIL_FINAL/translations;fi
	    
	    mv "$fich_idioma" profiles/$PERFIL_FINAL/translations/
	    ;; #mover el idioma a su sitio de instalación
	    
	 8)  # copio el idioma a varios directorios no se sabe cual será el correco hasta la version final de d8
	    if [ -d $FICH_PUBL ];then echo"";else mkdir $FICH_PUBL;fi
	    if [ -d $FICH_PUBL/translations ];then echo"";else mkdir $FICH_PUBL/translations;fi #drupal8 
	    if [ -d profiles/$PERFIL_FINAL ];then echo "";else mkdir profiles/$PERFIL_FINAL;fi
	    if [ -d profiles/$PERFIL_FINAL/translations ];then echo "";else mkdir profiles/$PERFIL_FINAL/translations;fi
	    if [ -d core/profiles/$PERFIL_FINAL ];then echo "";else mkdir core/profiles/$PERFIL_FINAL;fi
	    if [ -d core/profiles/$PERFIL_FINAL/translations ];then echo "";else mkdir core/profiles/$PERFIL_FINAL/translations;fi
	    cp "$fich_idioma" $FICH_PUBL/translations/
	    cp "$fich_idioma" profiles/$PERFIL_FINAL/translations/
	    mv "$fich_idioma" core/profiles/$PERFIL_FINAL/translations/
 
	    ;;	  #para drupal8 no importa el perfil, el idioma está en un directorio comun (creo)
	    
	 9)
	    echo "$RED esta versión aún no está disponible" ;;
   esac
   
      if [ $NOM_DISTRO = 0 ];then
	 echo "$LILA     >>>>>>>>>>>>>>>>   Proceso de instalación de$AZUL $version_drupal $LILA <<<<<<<<<<<<<<<<< \n $GRIS"
      else
	 echo "$LILA     >>>>>>>>>>>>>>>>   Proceso de instalación de$AZUL $NOM_DISTRO$LILA core $AZUL$version_drupal $LILA <<<<<<<<<<<<<<<<< \n $GRIS"	       
      fi
      echo "\n $GRIS << Esto puede tardar hasta 3 minutos, dependiendo de la cantidad a descargar >> \n"
}

func_HallarDirPadre() {
      DIR_ACTUAL=$(pwd)     
      DIR_PADRE=$(echo ${DIR_ACTUAL%/*})      #utilizo un variable para acortar la ruta 
}

func_MostrarDatos() {
   clear   
   
   if [ "$f" = s ] || [ "$f" = S ] || [ "$f" = Y ] || [ "$f" = y ] || [ "$f" = "" ]; then 
      echo ""      
      echo "\t$GRIS DATOS DE ADMINISTADOR DE DRUPAL"					>> $DIR_FIND/datos.txt
      echo "\t$LILA administrador:			\t\t $USER_DRUPAL"		>> $DIR_FIND/datos.txt
      echo "\t$LILA password:           		\t\t $PASS_DRUPAL"		>> $DIR_FIND/datos.txt		
      echo "\t$LILA e-mail:             		\t\t $MAIL_USER_DRUPAL"		>> $DIR_FIND/datos.txt
      echo ""										>> $DIR_FIND/datos.txt
      echo "\t$GRIS DATOS DEL SITIO WEB"						>> $DIR_FIND/datos.txt
      echo "\t$BLANCO nombre del sitio: 		\t\t $SITE_NAME"		>> $DIR_FIND/datos.txt
      echo "\t$BLANCO e-mail            		\t\t $MAIL_SITE_NAME"		>> $DIR_FIND/datos.txt
      echo "" 										>> $DIR_FIND/datos.txt
      echo "\t$GRIS DATOS DE LA BD DE DRUPAL"						>> $DIR_FIND/datos.txt
      echo "\t$CELESTE base de datos:   		\t\t $NAME_DB"			>> $DIR_FIND/datos.txt
      echo "\t$CELESTE usuario bd:      		\t\t $USER_DB"			>> $DIR_FIND/datos.txt
      echo "\t$CELESTE password bd:     		\t\t $PASS_DB" 			>> $DIR_FIND/datos.txt
      echo ""										>> $DIR_FIND/datos.txt         
      echo "\t$GRIS OTROS DATOS"							>> $DIR_FIND/datos.txt
      echo "\t$AZUL propietario:  			\t\t $propietario"		>> $DIR_FIND/datos.txt
      echo "\t$AZUL dominio/subdominio:		\t\t $NOM_DOM"				>> $DIR_FIND/datos.txt
      echo "\t$AZUL directorio inst.:		\t\t $DIR_FIND"				>> $DIR_FIND/datos.txt 
      echo "\t$AZUL URL del nuevo sitio.:		\t\t $URL_actual"		>> $DIR_FIND/datos.txt      
      echo ""										>> $DIR_FIND/datos.txt         
      echo "\t$GRIS DATOS SOBRE EL SCRIPT"						>> $DIR_FIND/datos.txt
      echo "\t$YELLOW tiempo en segundos:		\t\t $totalseg"			>> $DIR_FIND/datos.txt
      echo "\t$YELLOW tiempo en minutos:		\t\t $totalmin"			>> $DIR_FIND/datos.txt
   fi

      echo "\n\n$VERDE ---------------------------->    INSTALACIÓN COMPLETA, disfrute de Drupal  <------------------------------\n\n"	
}      

func_Permisos() {
   # Estamos situados en DIR_FIND que es el directorio de instalación #SUBIR Un directorio para poder ejecutar ls 
   BASENAME_DIR_NOM_DOM=$(basename $DIR_NOM_DOM)  
   DIR_NOM_DOM_PADRE=$(echo ${DIR_NOM_DOM%/*}) 
   a=$(ls -l $DIR_NOM_DOM_PADRE | grep $BASENAME_DIR_NOM_DOM); b=$(echo ${a#*\ }); c=$(echo ${b#*\ }) ; propietario=$(echo ${c%%\ *})
   # cambiando permisos
   chown -R $propietario:$grupo ./* 
   if [ -d $FICH_PUBL ];then 
      chown -R $propietario:$grupo $FICH_PUBL
      chmod -R 777 $FICH_PUBL
   fi
   
   if [ -f .htaccess ];then
      chmod 664 .htaccess   # quizá 644 pero con los permisos de plesk
   fi
   
      if [ "$vers_princ_drupal" = 8 ];then
	 if [ -f .htaccess ];then
	    chown $propietario:$grupo .htaccess .jshintignore .editorconfig .gitattributes .jshintrc
	 fi
      elif [ "$vers_princ_drupal" = 7 ];then
	 if [ -f .htaccess ];then
	    chown $propietario:$grupo .htaccess .gitignore
	 fi
      elif [ "$vers_princ_drupal" = 6 ];then
	 if [ -f .htaccess ];then
	    chown $propietario:$grupo .htaccess
	 fi
      fi
      
   if [ -d "$DIR_EXCL_PLESK" ];then chown -R $admin_linux:$admin_linux $DIR_EXCL_PLESK;fi # no encontrará el directorio si la inst. está en un subdirectorio del dominio
}

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@				FIN FUNCIONES			@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#########################################################################################################################################
###########################################                PREPARACIÓN DE DATOS MYSQL         ###########################################
#########################################################################################################################################

          ########################          verificar que psa esta en ejecución   ###############
if [ $PLESK_INST = ok ];then        #PLESK presente verificamos el servicio en ejecución 
   if ! (ps aux | grep -v grep | grep sw-cp-serverd) >> /dev/null; then   # si psa está en ejecución ...
   echo "\n $RED No se ha encontrado un servicio plesk en ejecución, la instalación podría fallar" 
   fi
fi
          ########################          verificar que mysql esta en ejecución   ###############
if  (ps aux | grep -v grep | grep mysqld)  >> /dev/null; then   # si mysql está en ejecución ...

      echo "\n $VERDE Ingresar usuario administrador de MySQL : $AZUL \c"; read U_MYSQL
      echo "\n $VERDE Ingresar password de $U_MYSQL : \c" ; stty -echo ; read P_MYSQL; stty echo; printf '\n'; echo "$RED" 
      #oculta el ingreso del usuario (contraseña)
      #read -p -s "ingrese password: "  $P_MYSQL 
      mysql -u$U_MYSQL -p$P_MYSQL -D mysql -s -e "SELECT User,Host,Grant_priv FROM user Where User='$U_MYSQL'"
      # a partir de aqui solo hago un select para que termine de ejectuar y libere el prompt sino no podría seguir ejecutándose el script
      salida=$?
                    
	  until [ "$salida" = 0 ]; do    #mientras el admin y el passwd de mysql sea incorrecto se vuelve a preguntar
		echo "$RED      Nombre de usuario o contraseña incorrecto \n"
		echo "\n $VERDE Ingresar usuario administrador de MySQL : $AZUL \c"; read U_MYSQL
		echo "\n $VERDE Ingresar password de $U_MYSQL : \c" ; stty -echo ; read P_MYSQL; stty echo; printf '\n'; echo "$RED" 
		#oculta el ingreso del usuario (contraseña)
		#read -p -s "ingrese password: "  $P_MYSQL 
		mysql -u$U_MYSQL -p$P_MYSQL -D mysql -s -e "SELECT User,Host,Grant_priv FROM user Where User='$U_MYSQL'" 
		# a partir de aqui solo hago un select para que termine de ejectuar y libere el prompt 
		salida=$?
		echo "\n $ENDCOLOR"
	  done
else
     echo "\n $RED No se ha encontrado algún servicio MySQL, no se puede continuar con la instalación \n" ; exit 0
fi     

#######################################################################################################################################
##########################           <<FASE EXPERIMENTAL>>     CREAR DOMINIOS Y SUBDOMINIOS PLESK            ##########################
#######################################################################################################################################

#    echo "\n $VERDE        Deseas crear un subdominio plesk? (s/S/y/Y/'ENTER'): \c ";  read j
#      if [ "$j" = s ] || [ "$j" = S ] || [ "$j" = y ] || [ "$j" = Y ] || [ "$j" = "" ]; then
#	
#	  sal_dom=1
#	  until [ "$sal_dom" = 0 ]; do    #mientras el admin y el passwd de mysql sea incorrecto se vuelve a preguntar
#	
#	    echo "\n $VERDE Ingrese nombre de subdominio : $AZUL \c"   ; read DO_SDOM
#	    echo "\n $VERDE Ingrese nombre de dominio para el subdominio '$DO_SDOM' : $AZUL \c"   ; read DO_DOM
#	    echo "\n $VERDE Ingrese nombre admin plesk : $AZUL \c"     ; read ADM_PLESK
#	    echo "\n $VERDE Ingrese password de $ADM_PLESK : $AZUL \c" ; stty -echo ; read P_ADM_PLESK ; stty echo; printf '\n' ; echo "$RED" 
#	    
#	    /usr/local/psa/bin/subdomain --create $DO_SDOM -domain $DO_DOM 
#	    sal_dom=$?
#	  
#	  done
#	
#      fi
    
#~~~~~~~~~~~~~~~~ comprobar si el dominio está dado de alta en el servidor de silicontower

########################################################################################################################################
#######################            CREAR LA VISTA (VIEWS de MySQL) PARA EXTRAER EL NOMBRE DEL DOMINIO         ##########################
########################################################################################################################################
   func_ViewsPsaComprobarDom() {
      # COMPROBAR SI LA VIEWS $COMPROBAR_DOM  o la base de datos psa EXISTE, caso contrario parar el scripts
      # comprueba la existencia de la db mysql psa (plesk), si existe comprobar si la views $COMPROBAR_DOM existe sino crearla
      EXIST_psa=$(mysql -u$U_MYSQL -p$P_MYSQL -D information_schema -s -e "SELECT count(*) FROM schemata WHERE schema_name = 'psa'")
		#  adminMySQL   passw        bd                                             tabla          columna      dato a buscar
	if [ "$EXIST_psa" = 0 ] ;then 
	  echo "\n $RED La base de datos 'psa' no existe \n ¿tienes plesk instalado?" 
	else
	    EXIST_COMPROBAR_DOM=$(mysql -u$U_MYSQL -p$P_MYSQL -D information_schema -s -e "SELECT count(*) FROM views WHERE table_schema = 'psa' and table_name = '$COMPROBAR_DOM'")
	      if [ "$EXIST_COMPROBAR_DOM" = 0 ] ;then 
		echo "\n $RED La base de datos 'psa' existe pero no la vista (VIEWS) '$COMPROBAR_DOM'" 
		# la views mysql no existe, se procede a crearla
		mysql -u$U_MYSQL -p$P_MYSQL -D psa -s -e "CREATE VIEW $COMPROBAR_DOM AS SELECT domains.id,domains.name,hosting.dom_id,hosting.sys_user_id,hosting.www_root FROM domains,hosting WHERE domains.id=hosting.dom_id"
	      fi                                                   
	fi
   }
   if [ $PLESK_INST = ok ];then func_ViewsPsaComprobarDom;fi	
########################################################################################################################################
###################################                ENCONTRANDO EL DIRECTORIO DE INSTALACIÓN               ##############################  
########################################################################################################################################

dir_ok=1 #si DIR_INSTALL falla dir_ok vale 1. Cuando el directorio de instalción sea aceptado dir_ok tomará el valor 0 y salimos del bucle

while [ $dir_ok = 1 ] ; do   #while_1

     echo "\n $VERDE  Escribe el nombre del directorio donde se hará la instalación: $AZUL \c "; read DIR_INSTALL
         
  if [ "$DIR_INSTALL" = "" ] ; then     #control si no introduce ningún dato
     echo "\n $RED no has ingresado nada"
     dir_ok=1  #al presionar enter nos saltamos find y forzamos a ingresar al bucle while, ya que al hacer un find de "" dir_ok=0

  else  #sino es nulo se comprueba si existe
 
      if [ "$(find $RUTA_WEB -maxdepth 3 -type d -iname "$DIR_INSTALL" | uniq -c1 | wc -l)" = 0 ] ;then  
      #si encuentra el directorio asignamos dir_ok=0 sino dir_ok=1 
	   #se fuerza con uniq a obtener un solo resultado en la búsqueda si hubieran más de uno
	   #con wc se cuenta la linea optenida si es 1 find ha encontrado por lo menos un resultado 
	 
	 dir_ok=1
	 echo  "\n $RED Directorio no encontrado $LILA \n"
	 
      else     #el directorio existe comprobar si existe solo uno o más de uno
	find $RUTA_WEB -maxdepth 3 -type d -iname "$DIR_INSTALL" > $FICH_FIND  
	#se genera un archivo de texto $FICH_FIND con el resultado de búsqueda, así se puede tratar más de un resultado
	echo "$LILA \n"

	CUENTA=$(cat $FICH_FIND | wc -l)   #cuenta la cantidad de lineas del archivo, la cantidad de directorios encontrados

	#~~~~ ESTA PARTE SE PUEDE OMITIR Y USAR igual que a partir del else
	  if [ "$CUENTA" = 1 ] ; then     #si ha encontrado un solo directorio 
		DIR_FIND="$(cat $FICH_FIND)" # Sé que el fichero tiene una sola línea, por eso uso cat
	  	echo "$VERDE ¿Está seguro de instalar drupal en $YELLOW --->>  $DIR_FIND  <<--- $VERDE ? (s/S/y/Y/'ENTER'): $AZUL \c "; read d   
		#pedir confirmación del directorio de instalación
		    if [ "$d" = s ] || [ "$d" = S ] || [ "$d" = y ] || [ "$d" = Y ] || [ "$d" = "" ]; then   
		    #Se debe usar siempre comillas dobles "$d"
	      		   
			   # si acepta el directorio comprabamos que no existe una instalación previa
			   if [ -d "$DIR_FIND"/includes ] || [ -d "$DIR_FIND"/core ] ;then      #core por drupal8 e includes por drupal7 y drupal6 

			      echo "\n $VERDE ya existe una instalación de drupal en $RED $DIR_FIND $VERDE"
			      read -p "¿Quieres continuar y borrar la instalación existente? (s/S/y/Y)? -> enter='no' <- : " c
			      if [ "$c" = s ] || [ "$c" = S ] || [ "$c" = y ] || [ "$c" = Y ]; then   
				 # si detectamos instalación previa de drupal preguntamos si sobreescribirla
				 func_BorraInstPrevia
			      else   #si no deseamos sobreescribir la instalación previa de drupal, 
				    #entonces volvemos a pregruntar por el directorio de instalación
				 dir_ok=1
			      fi
			   else
			      dir_ok=0
			   fi  
		    fi
		    
	   else   # se han encontrado 2 o más directorios
	   	echo "$BLANCO Se han encontrado $CUENTA directorios con el nombre $AZUL $DIR_INSTALL $ENDCOLOR"
	   	echo "$CELESTE"
	   	find $RUTA_WEB -maxdepth 3 -type d -iname "$DIR_INSTALL"
		    
		echo "\n $BLANCO"; read -p "     ¿Deseas usar algunos de estos directorios?  (s/S/y/Y/'ENTER') :  " f   
		#pedir confirmación del directorio de instalación

	      if [ "$f" = s ] || [ "$f" = S ] || [ "$f" = y ] || [ "$f" = Y ] || [ "$f" = "" ]; then   		   	

		for linea in `seq 1 $CUENTA` ; do  # for_1  #se recorre cada linea de $FICH_FIND pregunta por cada directorio

		    DIR_FIND=$(head -$linea $FICH_FIND | tail -1)       #se usan " para capturar nombres con espacios
		    echo "\n $YELLOW Deseas utilizar el $lineaº de los $CUENTA directorios encontrados? $ENDCOLOR "
		    read -p " --->>  $DIR_FIND  <<--- (s/S/y/Y/'ENTER'):  " d   #pedir confirmación del directorio de instalación

			   if [ "$d" = s ] || [ "$d" = S ] || [ "$d" = y ] || [ "$d" = Y ] || [ "$d" = "" ]; then   
			   #Se debe usar siempre comillas dobles "$d"
			   
				# si acepta el directorio comprabamos que no existe una instalación previa
				if [ -d "$DIR_FIND"/includes ] || [ -d "$DIR_FIND"core ];then      #core lo crea drupal8 e includes lo crea drupa7 y drupal6 
				    echo "\n $VERDE ya existe una instalación de drupal en $RED $DIR_FIND $VERDE"
				    read -p "¿Quieres continuar y borrar la instalación existente (s/S/y/Y)? -> enter es='no' <-- :  " c
					if [ "$c" = s ] || [ "$c" = S ] || [ "$c" = y ] || [ "$c" = Y ]; then   
					
					  func_BorraInstPrevia
					  break # salimos del bucle for_1
					else   #si no deseamos sobreescribir la instalación previa de drupal, entonces 
						#volvemos a pregruntar por el directorio de instalación
					  dir_ok=1
					  break  # salimos del bucle for_1 pero no del bucle while_1, 
					  #vuelva preguntar por el directorio de instación
					fi
				else
				  dir_ok=0
				fi  
			   fi  
		done # fin for_1 
	      fi
	  fi
      fi
  fi
done  #finalizamos while_1

#####################################################################################################################################
##################                   A PARTIR DE AQUI TRABAJAMOS SOBRE EL DIRECTORIO DE INSTALACIÓN             #####################  
#####################################################################################################################################

    cd "$DIR_FIND" #una vez acepado el directorio de instalación nos situamos en él
    
    ############ COMPROBAR MEMORIA DE PHP       ERROR     ############
    # se comprueba la variable memory_limit una vez que estamos en el directorio de instalación, caso contrario drush dará error
############      a=$(drush st | grep php)
############      b=$(echo ${a#*:})
############      c=$(grep memory_limit $b)
############      d=$(echo ${c#*=})
############      e=$(echo ${d%M*})
############      if [ "$e" -lt 256 ];then 
############	 echo "\n $GRIS La variable $RED1 memory_limit $GRIS en el fichero $RED1 $b $GRIS vale $RED1 $d $GRIS se ha cambiado a $RED1 256M ############$GRIS"
############	 c=$(grep -n memory_limit $b)
############	 d=$(echo ${c%:*})
############	 # borrar linea 'memory_limit' y agregar una nueva con 256M 
############	 sed -i '407d' $b;sed -i "407i memory_limit = 256M" $b
############	 service apache2 restart  # resetear apache, es distinto para otros SO y otros servidores webs
############      fi

    ##################                   ELEGIR DRUPAL O DISTRO DRUPAL                   #####################
    ##########################################################################################################

    NUM=2
    REST_PADRE=Ko #  si modificamos la carpeta includes o core  la variable pasará a ok para luego restablecer el directorio includes
until [ $NUM -le 1 ]; do

    # find . -maxdepth 1 -type d -iname "drupal*" -exec rm -r {} \;  
    # find . -maxdepth 1 -type d -iname "themes" -exec rm -r {} \;
    # find . -maxdepth 1 -type d -iname "scripts" -exec rm -r {} \;

    # Se permite elegir entre una instalación drupal o una distribución drupal     
    NOM_DRUPAL=0 
    while [ "$NOM_DRUPAL" = 0 ];do   #mientras no obtengamos la vesion drupal a descragar
    
           echo "\n $VERDE ¿Deseas instalar una distribución drupal en lugar de sólo drupal? (s/S/y/Y/enter) 
            $GRIS (0\"cero\") cancela la instalación $AZUL                                              \c  "; read i
           if [ "$i" = 0 ];then exit 0;fi
                  
      if [ "$i" = s ] || [ "$i" = S ] || [ "$i" = y ] || [ "$i" = Y ] || [ "$i" = "" ];then  # elección de una distro drupal
      
	    contar_distros=$(cat $FICH_DISTROS | wc -l) #cuenta uno de mas por el salto de línea
	    contar_distros=$(expr $contar_distros - 1)
	    echo "\n$VERDE 	Existen en la base de datos $AZUL$contar_distros$VERDE distros drupal $ENDCOLOR"
	       
	       lee_fich=$FICH_DISTROS
	       func_Mostrar4Columnas
	       
	       echo "\n\n $VERDE Escribe el nombre de la distribución drupal que deseas instalar : $AZUL\c  ";read distro
	       distro=$(echo $distro | awk '{print tolower($0)}')  #pasar a minúsculas
	       
	    for line in $(grep "$distro" $FICH_DISTROS); do  #busca coincidencia con grep y muesta la linea completa del archivo si encontró algo
	       A=0
	       echo " $VERDE ¿Deseas descargar $AZUL $line $VERDE? (s/S/y/Y/enter) : \c $AZUL ";read A
	       echo "$GRIS"
	       
	       if [ "$A" = s ] || [ "$A" = S ] || [ "$A" = y ] || [ "$A" = Y ] || [ "$A" = "" ]; then 
		  echo "$GRIS  << Se paciente esto puede tardar un par de minutos >> \n" 

		  distro_elegida=ok #indicamos que se ha elegido una distro
		  NOM_DISTRO=$line   # almacenar nombre de la distro para trabajar con ella mas tarde
		  NOM_DRUPAL=$line #guardar el nombre de la distro
		  break 
	       else
		  if [ "$A" = 0 ];then break;fi
	       fi			 	
	       
	    done
      else				# elección de instalación drupal normal
	 s="nodrupal"
	 while [ $s = "nodrupal" ]; do  # while_2 # mientras no ingrese drupal-xxxx seguirá preguntado por un nombre correcto
	       echo "\n $VERDE Escribe la versión a instalar, $AZUL ENTER $VERDE instalará la versión más actual y estable $GRIS (0\"cero\" cancelar) "
	       echo "$VERDE  Escribe (por ej. drupal-7.20 o drupal-6.19 o drupal-8.2 o drupal-6.23 o drupal-7.16 ... ... )"
	       echo "  Escribe (por ej. drupal-7 o drupal-6 o drupal-8 para la última versión estable de cada rama ) : $AZUL\c  ";read v
	       
	       if [ "$v" = 0 ];then break;fi #obliga a salir del bucle
	       
	       t=$(echo "$v"  | cut -d "-" -f 1)    
	       
		  if [ "$t" = "drupal" ];then 
		     NOM_DRUPAL="$v" 
		     distro_elegida=ko # al no elegir una distro guardamos el valor ko
		     break   #s=$v ; echo "s vale $v  ahora salimos del bucle"   #borrar echo
		  else 
		     if [ "$v" = "" ];then   
		     # Al presionar enter se elegirá la ultima versión de drupal (~linea 162) por lo tanto paramos el bucle
			NOM_DRUPAL="drupal"
			distro_elegida=ko # al no elegir una distro guardamos el valor ko
			break
		     else
			echo "\n $RED Nombre no válido, 'presionando $AZUL ENTER $RED descarga la última versión estable' \n $ENDCOLOR"
		     fi
		  fi   
	 done  #fin while_2 optenemos el nombre de la versión de drupal y lo guardamos en $NOM_DRUPAL 
      fi
   done  

      if [ "$NOM_DRUPAL" = "" ]; then NOM_DRUPAL="drupal"; fi  # Si solo presiona enter ha elegido drupal, y se descarga la última versión
      #eliminar todos los espacios en caso de que $NOM_DRUPAL contenga mas de una palabra
      if [ $distro_elegida = ok ];then 
	 echo "$VERDE Has elegido la distro: $YELLOW $NOM_DISTRO $VERDE debes tener en cuenta que cada distribución puede tener distintos requerimientos para la instalación"
	 echo " Algunas distribuciones drupal permiten una instalación:\n"
	 echo "$VERDE1 SEMIAUTOMATICA: $VERDE el scripts configurará varios datos y luego deberá completar la instalación a través del navegador web"
	 echo "$VERDE1 AUTOMÁTICA: $VERDE el scripts intentará hacer toda la instalación como si de drupal se tratara. (Esto está soportado sólo para algunas distros)"
	 echo "$GRIS"
      fi
      
      case $NOM_DISTRO in 
	 
	 commerce_kickstart)
	    echo "\n $YELLOW $NOM_DISTRO permite una instalación automática a excepción del idioma, el cual deberás configurar luego \n $GRIS"	    #verficar si es posible instalar el idioma modificando la bd
	 ;;
	 
	 civicrm_starterkit)
	    echo "\n $YELLOW $NOM_DISTRO instalación semiautomática \n $GRIS" #intentar crear las dos bd que necesita
	 ;;
	 
	 erpal)
	    echo "\n $YELLOW $NOM_DISTRO instalación semiautomática \n $GRIS" #necesita una config. adicional sobre datos de la empresa
	    DISTRO_TipoInst=SA  #semiautomática
	 ;;
	 
	 openpublish | openpublic )
	    echo "\n $YELLOW $NOM_DISTRO instalación automática" 
	    echo "$VERDE Al ingresar por primera vez a $NOM_DISTRO se creará un directorio en $AZUL sites/default/files/languages $VERDE es necesario modificar los permisos de dicho directorio \n $GRIS El script te indicará el proceso en su momento \n"
	 ;;
	 
	 openchurch | openenterprise | opendeals)
	    echo "\n $YELLOW $NOM_DISTRO instalación automática" 
	 ;;
	 
	 openatrium)
	    echo "\n $YELLOW $NOM_DISTRO No es posible realizar la instalación"
	    exit 0
	 ;;
	 openenterprise | openfolio)
	    echo "\n $YELLOW $NOM_DISTRO instalación automática" 
	 ;;
	 
	 deims)
	    echo "\n $YELLOW $NOM_DISTRO instalación automática" 
	    echo "Luego de la instalación deberás arreglar varios errores relacionados con \"modules/system/page.tpl.php\" y \"includes/theme.inc)\""
	 ;;
	 
	 #tb_sirate_starter   buscar una carpeta profile en el dir padre para la descarga
##spark
#openatrium
#recruite
#julio
#corporative_site
#tb_events_starter
#openaid
#agov
#openoutrearch
 
      esac
      
      drush dl -y "$NOM_DRUPAL" # proceder con la descarga tanto distro como drupal
      
      DIR_OK=$(find "$DIR_FIND" -maxdepth 1 -type d -iname "$NOM_DRUPAL*")  ## buscar el directorio descargado
      if [ -d "$DIR_OK" ] ;then     #si el comando anterior se ejectutó bien, si encontró el directorio (la descarga) entonces todo ok
	echo "\n $VERDE  >>>>>>  Descarga completa continuando con la instalación ... <<<<<<< " 
	NUM=0   #parar el bucle
      else
	    echo "\n   $RED Ha habido un problema con la descarga de drupal. Quieres intentar de nuevo ('Enter'/s/S/y/Y) : \c $AZUL";read h
	    
	    func_BorraInstPrevia
	    
	    if [ "$h" = s ] || [ "$h" = S ] || [ "$h" = Y ] || [ "$h" = y ] || [ "$h" = "" ]; then  # arreglar dir padre con drupal instalado
	       #cambia el nombre includes y profiles del directorio padre al de instalación y luego lo restaura
	       NUM=2	#continuar con el bucle
	       
	       func_HallarDirPadre 	 
	       # verificar si el error se produce porque el directorio padre ya tiene el directorio includes y profiles
	       if [ -d "$DIR_PADRE"/includes ] || [ -d "$DIR_PADRE"/core ];then
		  # en este caso se indica que esta instación en el dir padre impide la instalación actual
		  echo "\n $YELLOW Ya existe una instalación de drupal en $AZUL $DIR_PADRE $YELLOW"
		  echo "   y esto impide la descarga de drupal en $AZUL $DIR_ACTUAL $ENDCOLOR"
		  echo "\n $LILA Solucionando el problema ... $ENDCOLOR "
		  # ~~~ el sitio tanto (DIR_PADRE) estará caido durante la instalación comprobar el nombre del sitio con NOM_DOM
	    
		  if [ -d "$DIR_PADRE"/includes ];then 
		     MODIF_DIR_includes=includes 
		     MODIF_DIR_profiles=profiles
		  else      #según sea d6-7 o d8
		     MODIF_DIR_profiles=core
		  fi 
		  mv "$DIR_PADRE"/"$MODIF_DIR_includes" "$DIR_PADRE"/"$MODIF_DIR_includes"_bkp_BKP_bkp
		  mv "$DIR_PADRE"/"$MODIF_DIR_profiles" "$DIR_PADRE"/"$MODIF_DIR_profiles"_bkp_BKP_bkp
		  #modificamos directorio includes o core (drupal8) para que permita la instalación
		  REST_PADRE=ok     #creamos una variable para luego restablecer includes a su estado natural
	       fi
	    fi     
      fi    
done
      #si fue necesario modificar el directorio includes/core/profiles lo restablecemos a su nombre original inmediato de la descarga
      func_HallarDirPadre
      if [ -d "$DIR_PADRE"/"includes_bkp_BKP_bkp" ];then mv "$DIR_PADRE"/"includes_bkp_BKP_bkp" "$DIR_PADRE"/"includes";fi 
      if [ -d "$DIR_PADRE"/"profiles_bkp_BKP_bkp" ];then mv "$DIR_PADRE"/"profiles_bkp_BKP_bkp" "$DIR_PADRE"/"profiles";fi
      if [ -d "$DIR_PADRE"/"core_bkp_BKP_bkp" ];then mv "$DIR_PADRE"/"core_bkp_BKP_bkp" "$DIR_PADRE"/"includes";fi      
#####################################################################################################################################
#####################################             EXTRAER NOMBRE DEL DOMINIO      ###################################################
#####################################################################################################################################
   func_ExtraerNomDomPsa() {
      NOM_DOM=$(mysql -u$U_MYSQL -p$P_MYSQL -D psa -s -e "SELECT d.name FROM $COMPROBAR_DOM AS d WHERE www_root='$(pwd)'") 
      DIR_NOM_DOM=$(pwd)        #guardo la ruta del nombre del directorio que coincide con el nombre del dominio
      URL_add="" #solo existira si estamos en un subdirectorio del del que corresponde al domino actual
      #comprueba si el directorio actual corresponde a un nombre de dominio
      #echo "NOM_DOM provisorio es:  -------   $NOM_DOM   ----------- "   #~~~~~~~~~~ controlar si se trata de un dominio o un subdominio
	 if [ -z "$NOM_DOM" ] ;then    #$NOM_DOM no tiene valor
	 #si el directorio actual no corresponde a un nombre de domino comprueba si el directorio superior al actual concuerda con el nombre de un dominio
	 AUX=$(pwd)
	 AUX_NOM_DOM=$(echo ${AUX%/*})      #utilizo un variable para acortar la ruta 
	 NOM_DOM1=$(mysql -u$U_MYSQL -p$P_MYSQL -D psa -s -e "SELECT name FROM $COMPROBAR_DOM WHERE www_root='$AUX_NOM_DOM'") 
	 ## optener la URL del sitio
	 URL_add=$(basename $AUX)

	       if [ -z "$NOM_DOM1" ] ;then   # si el nuevo directorio comprobado (el padre) corresponde al nombre de un dominio
		  echo "\n" 
	       else
		  NOM_DOM=$NOM_DOM1             #ajusto el nombre de la variable NOM_DOM
		  DIR_NOM_DOM=$AUX_NOM_DOM      #guardo la ruta del nombre del directorio que coincide con el nombre del dominio 
	       fi
	 else
	 echo "$ENDCOLOR" #si todo va bien con el domino pasa aqui
	 fi    #comprobar si NOM_DOM0 es nulo
	 URL_actual=$(echo "http://$NOM_DOM/$URL_add")
   }
   if [ $PLESK_INST = ok ];then 
      func_ExtraerNomDomPsa
   else
      #~~~ esto se debe revisar, muchas cosas podrían fallar
      echo "\n$VERDE Ingresar el nombre de dominio o subdominio existente para realizar la instalación $AZUL \c ";read NOM_DOM
      URL_actual=$(echo "http://$NOM_DOM")
   fi

######################################################################################################################################
####################################    COPIAR LA DESCARGA DRUPAL EN EL DIR DE INSTALACIÓN    ########################################
######################################################################################################################################

   cd $NOM_DRUPAL*   #ingresar al directorio de la descarga, por ej. drupal-7.22
   ## extraer la ruta del directorio de descarga de drupal 
   DIR_DRUPAL=$(pwd)   #extraer la ruta del directorio de descarga de drupal
   BASENAME_DIR_DRUPAL=$(basename "$DIR_DRUPAL")
   cd ..  		#  <<<<<<<<<<<<<<<  cambiar a directorio anterior  >>>>>>>>>>>>>>>>>>>
   cp -rpf "$BASENAME_DIR_DRUPAL"/* ./
   cp $RUTA_SCRIPTS_txt/drushrc.php ./sites/default/  #el fichero drushrc.php contiene instrucciones para que drush no de error de límite de memoria
   
   ##### obtengo la versión de drupal descargada ####
   #el fichero CHANGELOG.txt contiene la versión de drupal, el problema es que a veces está en la 1º linea y otras en la 2º además en d8 esta "core"
   #con grep busco que la linea del fichero coincida con Drupal y con head extraigo la 1º encontrada
   # el archivo CHANGELOG que indica la versión de drupal descargada en drupal8 se encuentra en un directorio distinto
   if [ -f $ARCH_VERS_DRUPAL ];then   
      vers_idioma=$(cat "$DIR_FIND"/$ARCH_VERS_DRUPAL | grep Drupal | head -1 | cut -d" " -f2 | sed 's/,//g') 									#por ej. Drupal 7.22, 2012-04-03 	7.22, o 7.18        7.18 o 7.22
   else										
      if [ -f core/$ARCH_VERS_DRUPAL ];then 
	 vers_idioma=$(cat "$DIR_FIND"/core/$ARCH_VERS_DRUPAL | grep Drupal | head -1 | cut -d" " -f2 | sed 's/,//g') 	#por ej. 8.2    #para drupal8	 
      else
	 echo "$RED No se ha encontrado el archivo $ARCH_VERS_DRUPAL no se puede continuar con la instalación"	
	 #se podría pedir que ingrese manualmente la version de drupal y continuar
      fi
   fi
   
   vers_princ_drupal=$(echo  $vers_idioma | cut -c 1)               		#ej. 7 8 6
   version_drupal=$(echo "drupal-$vers_idioma")                      		#ej. drupal-7.22 drupal-8.0

   #mover los achivos ocultos
   #no funciona esta opción      <<< cp -rpf $  DIR_DRUPAL/.*[^.] >>>   aunque directo por consola sí que funciona
   cd $BASENAME_DIR_DRUPAL  # ingreso al directorio donde están los archivos ocultos
   if [ "$vers_princ_drupal" = 8 ];then
      cp .htaccess .jshintignore .editorconfig .gitattributes .jshintrc ../
      chmod 777 ../.htaccess   # agunas distros necesitan hacer modificaciones, al final del script se corrijen los permisos
   elif [ "$vers_princ_drupal" = 7 ];then
      cp .gitignore .htaccess ../
      chmod 777 ../.htaccess
   elif [ "$vers_princ_drupal" = 6 ];then
      cp .htaccess ../
      chmod 777 ../.htaccess
   fi

   cd ..  ; rm -RIf $BASENAME_DIR_DRUPAL		#una vez movidos los archivos eliminamos el directorio
   
   func_Permisos #necesario cambiar permisos porque durante la instalación se crean nuevos directorios y archivos
   
   if [ $distro_elegida = ok ];then  # se ha elegido una distribución drupal
	 conf_prof_distro=$(echo "$DIR_FIND"/$DIR_PROFILES/$NOM_DISTRO)

	 FIND_DIR_PERFIL=$(find "$DIR_FIND"/$DIR_PROFILES -maxdepth 1 -type d -iname "$NOM_DISTRO*")		     
		  
	 if [ -d "$FIND_DIR_PERFIL" ];then  #si existe un nombre de perfil igual al nombre de la distro descargada
	       perfil_distro=ok
	 else 
	       echo "\n $ROJO No se pudo detectar el perfil de instalación para $AZUL$NOM_DRUPAL $ROJO podrá seleccionarlo más adelante \n"
	       perfil_distro=ko # este valor obliga a elegir el nombre de perfil manualmente
	       sleep 2
	 fi	       
   fi  

######################################################################################################################################
#################################################     Descarga del idioma      #######################################################
######################################################################################################################################
      
func_DescagaIdioma() {

	 find "$DIR_FIND" -maxdepth 1 -type f -iname "*.es.po*" -exec rm -RIf {} \; #si existe el idioma anteriormente lo borramos
	 
	 if [ "$vers_princ_drupal" = 8 ];then # solo para drupal-8 dev, cuando salga la vers final habrá que revisar
	       vers_idioma=7.22 #cambiar la versión del idioma a descargar
	       wget http://ftp.drupal.org/files/translations/7.x/drupal/drupal-$vers_idioma.es.po
	 else
	       wget http://ftp.drupal.org/files/translations/$vers_princ_drupal.x/drupal/drupal-$vers_idioma.es.po    # descargar el idioma en drupal

	 fi
	 
	       fich_idioma="drupal-$vers_idioma.es.po"
	       echo "$LILA \n Se procede con la descarga del idioma español versión $AZUL $fich_idioma $GRIS \n  "
	       
	 if [ -f "$fich_idioma" ] ;then  #checar que el idioma correspondiente se ha descargado 
	       echo "$VERDE >>>>>>>>>>>   Idioma descargado correctamente <<<<<<<<<<<<< \n"
	 else
	       echo "$ROJO \n ha habido un problema con la descarga del idioma"
	       echo "$VERDE Desea continuar sin el idioma? ('Enter'/s/S/y/Y) o cancelar la instalación $AZUL \c "; read e ; echo "\n"
	    if [ "$e" = s ] || [ "$e" = S ] || [ "$e" = y ] || [ "$e" = Y ] || [ "$e" = "" ]  ; then   #sino desea continuar sin el idioma
	       echo ""
	    else
	       echo "\n $RED Se cancela la instalación"
			if [ -d drupal* ] ;then rm -R ./drupal* ; fi    #~~~~~ Antes de cancelar debería restaurarse el directorio a como estaba antes de comenzar la instalación
	       exit 0
	    fi
	 fi
}
      func_DescagaIdioma
      func_PerfilPorVersion
###########################################################################################################################################
#############################################       COMIENZA LA INSTALACIÓN      ##########################################################
###########################################################################################################################################

   if [ "$perfil_distro" = ko ];then  #Ha elegido una distro pero ha fallado el perfil (no es igual el nombre del perfil al nombre de la distro)
      a="p"				#salta a instalación personalizada para poder elegir el perfil en forma manual
   else
      echo "$VERDE ¿Prefieres una instalación $AZUL automática $VERDE ('Enter'/s/S/y/Y) o $AZUL personalizada $VERDE ('cualquier otra tecla') : $AZUL \c "; read a
   fi
	 ###########################      Instalación automática       ###############################
         #############################################################################################

   if  [ "$a" = s ] || [ "$a" = S ]  || [ "$a" = "" ] || [ "$a" = y ] || [ "$a" = Y ]; then     

	 echo "\n\n $LILA <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    COMENZANDO LA INSTALACION AUTOMÁTICA    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> \n\n $YELLOW" 
    
	 func_DatosAuto
	 func_BdExist
	 ####    Mov_idioma   #antes de instalar mover el idioma a su directorio de instalación   #####
	 if [ $distro_elegida = ok ];then perfil_por_defecto=$NOM_DISTRO;fi  #ha elegido una distro entonces el perfil deberá ser el de la distro
	 PERFIL_FINAL=$perfil_por_defecto
	 func_PreparaIdioma
	 func_Permisos
	 func_Drush_si
	 func_Permisos
	 
   else     ###########################      Instalación personalizada     ###############################
         ##############################################################################################
	 echo "\n\n $LILA <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    COMENZANDO LA INSTALACION PERSONALIZADA    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> \n\n $YELLOW" 
	 echo "\n $CELESTE Ingrese los siguiente datos para completar la instalación \n $GRIS Al presionar 'enter' se tomarán datos predefinidos "
	 func_DatosPerfil	
	 func_DatosDrupal	
	 func_DatosSitio	
	 func_DatosBd		
	 PERFIL_FINAL=$PERFIL
	 func_PreparaIdioma	
	 func_Drush_si		
   fi  

   echo "$GRIS"

######################################################################################################################################
################################################         INSTALANDO MÓDULOS    #######################################################
######################################################################################################################################
   clear
   for DB in $(mysql -Bse "show databases" -u$U_MYSQL -p$P_MYSQL) ; do  #for_3 
   #comprobar que se haya creado la base de datos para proceder con la instalación de los módulos 

      if [ $DB = "$NAME_DB" ];then    #sólo se ejecuta si la base de datos existe, esto es porque drush la ha creado correctamente
	       
	 ## Al descargar un módulo éste puede contener algunos más, y solo se habilita el principal, a través del siguiente procedimiento
	 ## Se logra habilitar cada uno de los módulos descargados 
   
	 echo "\n $LILA   >>>>     Instalando los siguientes módulos  <<<<<   $AZUL \n\n"
	 if [ -f "$FICH_LMP_aux" ];then rm -fI "$FICH_LMP_aux" ; fi # si el fichero existe borrarlo
			
	 lee_fich=$FICH_LMP #la func_Mostrar4Columnas leerá $FICH_LMP
	 func_Mostrar4Columnas 
#	       for line in $(cat $FICH_LMP); do
#		     echo -n "$line \t"   
#	       done
	 y=0
	 echo "\n $VERDE Quieres instalar estos módulos contenidos en el fichero $AZUL1 $FICH_LMP $VERDE"
	 echo "$GRIS Si estás instalando una distribución drupal no es aconsejable $AZUL \c : "; read y	 
	 if [ "$y" = s ] || [ "$y" = S ] || [ "$y" = Y ] || [ "$y" = y ] || [ "$y" = "" ]; then 
	 echo "\n $GRIS"	 
	       # leer cada linea del fichero y descargar el correspondiente modulo y además checar la cantidad de módules que descargará
	       # cada uno y generar un nuevo fichero para luego habilitarlos a todos 
	    if [ -f $FICH_LMP ];then 
	       for line in $(cat $FICH_LMP); do 
		     echo "\n $GRIS"
		     drush dl -y $line
		     drush dl -p $line >> $FICH_LMP_aux  # la cant de modulos que se descargaría se agrega a un fichero
	       done

	       for line in $(cat $FICH_LMP_aux); do  # se habilitan cada uno de los ficheros descargados
		     echo "\n $GRIS"
		     drush en -y $line
	       done
	    else
	       echo "\n $RED Fichero $FICH_LMP no encontrado, no se puede instalar ninún módulo \"principal\"" 	       
	    fi
	 fi
	 
	 if [ -f "$FICH_LMP_aux" ];then rm -fI "$FICH_LMP_aux" ; fi # borrar el fichero que ya no necesitamos
	 #si desea instalar más modulos llamar a sh /home/scripts/drush6_SEO.sh
	 clear
	 y=0
	 echo "\n $VERDE ¿Deseas instalar algún otro módulo? : $AZUL \c "; read y 
	 
	 if [ -f $FICH_MOD ];then 
	    if [ "$y" = s ] || [ "$y" = S ] || [ "$y" = Y ] || [ "$y" = y ] || [ "$y" = "" ]; then 
		  sudo sh $SCRIPT_DRUSH_MODULOS     # quiza source drush_modulos sea mejor opción
	    fi 
	 else
	    echo "\n $RED Fichero $FICH_MOD no encontrado, no se puede instalar ninún módulo" 
	 fi
      fi
   done		## final del bucle for_3      
   
######################################################################################################################################
################################################        INSTALANDO LIBRERIAS   #######################################################
######################################################################################################################################   
   ls $DIR_LIBRARY > $RUTA_SCRIPTS_txt/lista_libraries.txt
   CUENTA_L=$(cat $FICH_LIBRARIES | wc -l)
   clear
   A=0
   echo "\n $VERDE ¿Deseas copiar alguna librería $VERDE? (s/S/y/Y) \c $AZUL ";read A 
   echo "\n $AZUL Actualmente existen $ROJO $CUENTA_L $AZUL librerías en la base de datos"

   if [ "$A" = s ] || [ "$A" = S ] || [ "$A" = y ] || [ "$A" = Y ] || [ "$A" = "" ]; then 
      contar=0
      if [ -d $DIR_LIBRARY ];then
	 if [ -d "sites/all/libraries/" ];then 
	    echo ""
	 else
	    if [ -d "sites/all" ];then 
	       mkdir sites/all/libraries
	    else
	       mkdir sites/all
	       mkdir sites/all/libraries
	    fi
	 fi
	 for line in $(cat $FICH_LIBRARIES); do
	    contar=$((contar+1))
	    echo "$ROJO $contar) $VERDE ¿Deseas copiar la librería $ROJO $line $VERDE? (s/S/y/Y) $AZUL \c";read A2 
	    if [ "$A2" = s ] || [ "$A2" = S ] || [ "$A2" = y ] || [ "$A2" = Y ] || [ "$A2" = "" ]; then 
	       cp -ar $DIR_LIBRARY/$line sites/all/libraries
	       if [ sites/all/libraries/$line ];then echo "$VERDE2 instalado \n";fi
	    else
	       if [ "$A2" = 0 ];then break;fi  
	    fi
	 done 	 
      else
	 echo "\n $GRIS Directorio $RED2 $DIR_LIBRARY $GRIS no encontrado, no se han copiado las librerías" 
      fi  
   fi   
         
   if [ -f $FICH_LIBRARIES ];then rm $FICH_LIBRARIES;fi
   
######################################################################################################################################
################################################        INSTALANDO TEMAS       #######################################################
######################################################################################################################################   
   clear
   A=0
   echo "\n $VERDE ¿Deseas instalar algún tema $VERDE? (s/S/y/Y) \c $AZUL ";read A
   
   if [ "$A" = s ] || [ "$A" = S ] || [ "$A" = y ] || [ "$A" = Y ] || [ "$A" = "" ]; then 
   
      if [ -f $FICH_LT ];then
	 sudo sh $SCRIPT_DRUSH_TEMAS
	 #source sh $SCRIPT_DRUSH_TEMAS not found
      else
	 echo "\n $RED Fichero $FICH_LT no encontrado, no se puede instalar ninún tema" 
      fi  
   fi   
  
######################################################################################################################################
################################################          FINALIZAR DISTROS    #######################################################
######################################################################################################################################
  
   if [ $NOM_DISTRO != 0 ];then  #si se ha instalado una distro se actualiza todo el sitio e ingresamos al prox bucle
      echo "$GRIS Actualizando ..."
      drush -y up
   fi 
   
   # Al ingresar al sitio por 1º vez se crear directorios y archivos adicional que harán falta modificarles los permisos
   C=0
   until [ $C = C ]; do
      # if [ $NOM_DISTRO = "openpublic" ] || [ $NOM_DISTRO = "openpublish" ];then
	 echo "$VERDE Para completar la instalación de $BLANCO $NOM_DRUPAL $VERDE debes ingresar al sitio web $BLANCO 'http://$URL_actual' $VERDE y luego regresar y presionar la tecla C $AZUL \c ";read C;echo "$GRIS"
	 
	 if [ $C = "" ];then C="0";fi
	 if [ $C = "C" ];then	
	       echo "\n $GRIS << Refresca la web $BLANCO 'http://$URL_actual' $GRIS (F5) un par de veces para que todo funcione bien \n >> " 
	       if [ $NOM_DISTRO = "openpublic" ];then  #~~~ esto puede variar en versiones futuras de openpublic OJO
		  patch -p1 < profiles/openpublic/modules/contrib/captcha/captcha.inc
		  echo "$GRIS"; drush dl -y captcha; echo "$GRIS"; drush en -y captcha
		  cd modules/system
		  sed -i 's/$data = getimagesize($image->source);/\/\/&/' image.gd.inc   #comenta la linea 349
		  # agrega dos líneas al fichero
		  sed -i "350i\ \ \ \ \ \ \$img = drupal_realpath(\$image->source);\n      if (filesize(\$img) > 10 ) { \$data = getimagesize(\$img);} " image.gd.inc
		  cd ..;cd ..
	       fi
	 fi   
      # fi
   done
   
######################################################################################################################################
###########################         REVISANDO LOS ULTIMOS DETALLES AL FINALIZAR LA INSTALACION    ####################################
######################################################################################################################################

   if [ -d "$FICH_PUBL" ];then chmod -R 777 $FICH_PUBL/* ;else mkdir $FICH_PUBL; touch $FICH_PUBL/borrar.txt;fi     #necesario para drupal8dev
   find . -maxdepth 1 -type d -iname "$NOM_DRUPAL*" -exec rm -RIf {} \; #buscamos cualquier carpeta que hayamos descargado en el direct de inst y borramos
   if [ -f index.html ];then mv ./index.html ./_index.html ; fi   
   if [ -f "$FICH_FIND" ] ;then rm -RIf $FICH_FIND;fi   #borrar este fichero que ya no hace falta
   if [ -f "$FICH_LMP_aux" ] ;then rm -RIf $FICH_LMP_aux;fi   #borrar este fichero que ya no hace falta
   if [ -f "$FICH_PROFIL" ] ;then rm -RIf "$FICH_PROFIL" ; fi #si el fichero existe borrarlo

   func_Permisos # Al finalizar volvemos a colocar los permisos correctamente
   
   # tiempo que ha tardado el script
   fin_ns=`date +%s%N`
   fin=`date +%s`
   totalseg=`expr $fin - $inicio`
   a=60
   totalmin=`expr $totalseg / $a`
   
   if [ -f "$DIR_FIND"/datos.txt ];then rm -RIf "$DIR_FIND"/datos.txt; touch "$DIR_FIND"/datos.txt; chmod 400 "$DIR_FIND"/datos.txt;fi
   func_MostrarDatos
   cat "$DIR_FIND"/datos.txt
   
         
   # fichero ./sites/default/settings.php En la mayoría de los casos esto no es necesario. Pero por precaución ...
   chmod 644 sites/default/settings.php ; chmod 644 sites/default/default.settings.php 
   # copiar un .htaccess modificado
   cp .htaccess .htaccess.original
   if [ -f "$RUTA_SCRIPTS_txt/.htaccess" ];then cp $RUTA_SCRIPTS_txt/.htaccess ./;echo "$GRIS No se ha podido copiar el fichero .htaccess modificado";fi
   
   echo "\n$ROJO2";sleep 1; echo "8\c";sleep 1; echo ".7\c";sleep 1; echo ".6\c";sleep 1; echo ".5\c";sleep 1; echo ".4\c";sleep 1; echo ".3\c"; sleep 1;echo ".2\c";sleep 1;echo ".1\c";sleep 1;echo ".0\c";clear 
   
   echo "\n $VERDE Los datos de configuración están guardados en $DIR_FIND/datos.txt\n\n"

   drush vset clean_url 1 --yes  2> /dev/null #urls limpias     
